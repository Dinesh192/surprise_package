import { Component } from '@angular/core';
import { HttpClient, HttpRequest, HttpEventType } from '@angular/common/http';
import { Global } from '../../app.global';
import { Router } from '@angular/router';
import { ToastrModule } from 'ngx-toastr';
import { AuthenticationService } from '../../services/authentication.service';
import { PatientService } from '../../services/patient/patient.service';
import { AlertService } from '../../services/alert.service';
import { DoctorService } from '../../services/doctor/doctor.service';
import * as moment from 'moment';
import { Doctor } from 'src/app/app.model';
import { PatientFilesUploadModel } from 'src/app/models/patient/pat-fileupload.model';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  templateUrl: './app.dailyvisit.html'
})
export class DailyVisitComponent  {
  config: any;
  token: any;
  public filterdate = moment().format('YYYY-MM-DD');
  dailyVisitList: any;
  filteredDailyVisitList: any;
  public dailyvisit: boolean;
  collection = { count: 60, data: [] };
  progress: number;
  message: string;
  p:number=1;
  public docInfo: Doctor = new Doctor();
  public prescriptionFile: PatientFilesUploadModel = new PatientFilesUploadModel ();
  constructor(public routing: Router, public docService: DoctorService, public authService: AuthenticationService, public patService: PatientService, public alertService: AlertService,
  public global: Global , public notify: NotificationService, public http: HttpClient,) {
    this.token = this.authService.currentUserValue;
    this.ShowDailyVisitList();
    this.DailyVisit();
   
  }
  pageChanged(event) {
    this.config.currentPage = event;
  }
  public DailyVisit() {
    for (var i = 0; i < this.collection.count; i++) {
      this.collection.data.push(
        {
          id: i + 1,
          value: "items number " + (i + 1)
        }
      );
    }

    this.config = {
      itemsPerPage: 5,
      currentPage: 1,
      totalItems: this.collection.count
    };
  }

  public ShowDailyVisitList() {
    this.dailyvisit = true;
    //var id = this.token.PatientIdentifier;
    this.docService.getPatientListByProviderId(this.token.DoctorIdentifier).subscribe(res => {
      this.dailyVisitList = res;
     
      for (var i = 0; i < this.dailyVisitList.length; i++) {
        this.dailyVisitList[i].VisitDate = moment(this.dailyVisitList[i].VisitDate).format("YYYY-MM-DD");
       // this.patBookList[i].VisitStartTime = moment(this.patBookList[i].VisitStartTime).format('LT');
      }
      this.collection = {count: this.dailyVisitList.length,data:[]}
      
      for (var i = 0; i < this.collection.count; i++) {
          this.collection.data.push(
           {
              id: i + 1,
              VisitDate: moment(this.dailyVisitList[i].VisitEndTime).format('YYYY-MM-DD'),
              PatientName:this.dailyVisitList[i].PatientName,
              Gender:this.dailyVisitList[i].Gender,
              ContactNumber:this.dailyVisitList[i].ContactNumber,
              TreatmentAdvice:this.dailyVisitList[i].TreatmentAdvice,
              Medication:this.dailyVisitList[i].Medication,
              FollowUp:this.dailyVisitList[i].FollowUp,
              VisitId: this.dailyVisitList[i].VisitId,
              PatientId: this.dailyVisitList[i].PatientId
            }
          );
        }
      
      this.ChangeDate();
    },
      error => {
        this.alertService.error(error);
        // this.loading = false;
      });

  }
  ChangeDate() {
   // moment().format("DD/MM/YYYY");
    this.filteredDailyVisitList = new Array<any>();
    this.filteredDailyVisitList = JSON.parse(JSON.stringify(this.collection.data));//deepcopy
    this.filteredDailyVisitList = this.filteredDailyVisitList.filter(a => a.VisitDate == this.filterdate);
    //console.log(this.filterdate);
  }
  function(visitId,patientId){
    this.prescriptionFile.VisitId = visitId;
    this.prescriptionFile.PatientId = patientId;

  }

  upload(files) {

    if (files.length === 0)
      return;

    const formData = new FormData();

    for (let file of files)
      formData.append(file.name, file);



    const uploadReq = new HttpRequest('POST', `api/Doctor/UploadFile/`, formData, {
      reportProgress: true,
    });

    this.http.request(uploadReq).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress)
        this.progress = Math.round(100 * event.loaded / event.total);
      else if (event.type === HttpEventType.Response)
        this.message = event.body.toString();
        if(this.message =="Upload Successful."){
          this.docService.uploadPrescription(this.prescriptionFile).subscribe(res => this.Success(res),
          res => this.Error(res));
        }
    });
  }
  Success(res){
    this.notify.showSuccess("Sucess","Upload Sucessfully!")

  }
  Error(res){

  }


}
