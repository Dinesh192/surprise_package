import { Component, OnInit, Inject } from '@angular/core'
import { Visit } from 'src/app/models/visit/visit.model'
import { Global } from 'src/app/app.global';
import { DoctorService } from 'src/app/services/doctor/doctor.service';
import { PatientService } from 'src/app/services/patient/patient.service';
import { MAT_DIALOG_DATA } from '@angular/material';
import { AlertService } from 'src/app/services/alert.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { DatePipe } from '@angular/common';
import { FormBuilder } from '@angular/forms';
import { NotificationService } from 'src/app/services/notification.service';
import * as moment from 'moment';
import { SchedulingService } from 'src/app/services/scheduling/scheduling.service';
@Component({
  templateUrl: './app.doctor_reschedule.html'
})

  export class RescheduleComponent {
      public visitObj : Visit = new Visit();
      public appointmentTime: Array<any> = new Array<any> () ;
      public showRescheduleBtn: boolean = false;
      public todayDate = moment().format('YYYY-MM-DD');
      public showIntervalButton: boolean = false ;
      public showIntervalSlot: boolean = false ;
      scheduleIntervalList: Array<any> = new Array<any>();
    constructor(public global: Global, public docService: DoctorService, public patservice: PatientService,  @Inject(MAT_DIALOG_DATA) public data: any, public alertService: AlertService, public route: ActivatedRoute,
    public router: Router, public schedulingService: SchedulingService, public httpClient: HttpClient, private authenticationService: AuthenticationService,
    public http: HttpClient, private datepipe: DatePipe, private formBuilder: FormBuilder, private notifyService: NotificationService,
    ){
    }

  GetVisitTime(event) {
    this.showIntervalButton = true;
    var vDate = moment(this.visitObj.VisitDate);
    var visitDate = vDate.format('YYYY-MM-DD');
    if (visitDate != null) {
      this.schedulingService.getVisitTime(visitDate, this.data.docId, this.data.deptId, this.data.hosId).subscribe(res => this.SuccessAppointmentTime(res),
        res => this.ErrorAppointmentTime(res));
    }
  }
  SuccessAppointmentTime(res) {
    this.appointmentTime = Object.assign(this.appointmentTime, res);

    if (this.appointmentTime.length == 0) {
      this.notifyService.showInfo("Sorry", "There is no schedule for" + " " + "Dr." + " " + `` + " " + "on" + " " + `${moment(this.visitObj.VisitDate).format('YYYY-MM-DD')}`);
    }
    // if (this.data.docId != null && this.appointmentTime.length > 0) {
    //   this.appointmentTime = this.appointmentTime.filter(x => x.DoctorId == this.data.docId && x.DepartmentId == this.data.deptId && x.HospitalId == this.data.hosId);

    // }

    else {
      this.appointmentTime = this.appointmentTime;
    }
  }

  ErrorAppointmentTime(res) {
    // this.notifyService.showInfo("Sorry", "There is no schedule for" + " " + "Dr." + " " + `${this.doctor.DoctorName}` + " " + "on" + " " + `${moment(this.visitObj.VisitDate).format('YYYY-MM-DD')}`);
  }
  checkTimeSlot(SchId) {
    this.visitObj.BookingTime = null;
    //this.showBookingAppointBtn = false;
    if (this.showIntervalSlot == true) {
      this.scheduleIntervalList = [];
      this.showIntervalSlot = false;
      this.showRescheduleBtn = false;
    } else {
    this.schedulingService.getScheduleIntervalBySchedulingId(SchId).subscribe(res => this.SuccessScheduleInterval(res),
      res => this.Error(res));
    }
  }
  SuccessScheduleInterval(res) {
    this.scheduleIntervalList = Object.assign(this.scheduleIntervalList, res);
    this.showIntervalSlot = true;
    console.log(this.scheduleIntervalList);
  }
  setAppointmentTime(time) {
    if (time.IsBooked == true) {
      var bookingtime = `${time.StartTime} - ${time.EndTime}`;
      this.notifyService.showWarning("The time slot " + bookingtime + " was the previous booked time", "Sorry");
    }
    else {
      this.visitObj.BookingTime = `${time.StartTime} - ${time.EndTime}`;
      this.global.BookingTime = this.visitObj.BookingTime;
      this.global.VisitDate = this.visitObj.VisitDate;
      this.docService.ScheduleIntervalId = time.ScheduleIntervalId;
      this.docService.SchedulingId = time.SchedulingId;
      this.showRescheduleBtn = true;
    }

  }
  Reschedule() {
    this.schedulingService.postReschedule(this.data.visitId, this.docService.ScheduleIntervalId, this.docService.SchedulingId, this.visitObj, this.data.schedullingId, this.data.bookingTime).subscribe(res => this.Success(res),
      res => this.Error(res));


      }
      Success(res){
        this.appointmentTime = [];
        this.showRescheduleBtn = false;
        //this.visitObj.VisitDate = moment().format('YYYY-MM-DD');;
        this.notifyService.showSuccess('Updated successfully', 'Success');

  }
  Error(res) {
    this.notifyService.showError("Error", " Error in Re-Scheduling")

  }





}
