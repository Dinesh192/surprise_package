import { Component, OnDestroy, HostListener, Inject, OnInit, ViewChild } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { DOCUMENT } from "@angular/common";
import { Router, ActivatedRoute } from '@angular/router';
import { Global } from "../../app.global";
//import { Patient, Patient } from '../../app.model';
import { timer, Subscription } from "rxjs";
import { map, timestamp, takeWhile } from "rxjs/operators";
import { DomSanitizer } from "@angular/platform-browser";
import { PatientService } from "../../services/patient/patient.service";
import { AuthenticationService } from "../../services/authentication.service";
import { PatientDoc, Patient } from "../../models/patient/patient.model";
import { Visit } from "../../models/visit/visit.model";
import { AlertService } from "../../services/alert.service";
import { NotificationService } from "../../services/notification.service";
import { FinalReportComponent } from "../../app.finalreportcomponent";
//import * as moment from 'moment/moment'
//import * as fs from  'file-system'
import { DoctorService } from '../../services/doctor/doctor.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ChatService } from 'src/app/services/ChatNotification/chat-notification.service';
import { relativeTimeThreshold } from 'moment';
import { Doctor } from '../../models/doctor/doctor.model';
import { PopUpDialogComponent } from '../../components/common/popup/pop-up.component';
import { MatDialog } from '@angular/material';
//import 'src/vendor/jitsi/external_api.js';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
//declare var JitsiMeetExternalAPI: any;
//import * as $ from 'jquery';
@Component({
  //selector: 'app-root',
  templateUrl: "./app.doctorroom.html",
})
export class DoctorRoomComponent implements OnInit {
  
  public showPatDetail: boolean = false;
  //timerpat: any;
  //timerdoc: any;
  istimer="";
  apiDestroy: boolean = false;
  IsDoctor: boolean;
  showDocument: boolean = false;
  clickDocument: boolean = false;
  public isCollapsed = true;
  token: any;
  //status: any;
  public path: any;
  public domain :any;
  public options: any;
  public api: any;
  public Title: any;
  public Filetype: string;
  public docName: string;
  public Ext: boolean;
  public showChat: boolean;
  isCancelled: boolean;
  public showCallButton: boolean = true ;
  visitObj: Visit = new Visit();
  visitdata: Visit = new Visit();
  public visitId: string;
  public chatPatName: string;
  public chatPatId: string;
  public patId: string;
  public fileExtension: any;
  public docInfo: Doctor = new Doctor();
  patients: Array<PatientDoc> = new Array<PatientDoc>();
  docList: Array<any> = new Array<any>();
  public patInfo: Patient = new Patient();
  patList: Array<PatientDoc> = new Array<PatientDoc>();
  filteredpat: Array<PatientDoc> = new Array<PatientDoc>();
  fileData: File = null;
  filedoc: any;
  timerpat = timer(4000, 10000);
  timerdoc = timer(4000, 60000);
  timerlogout = timer(2000, 6000);
  loginVal: any;
  previewUrl: any = null;
  public documents: any;
  public showView: boolean = false;
  ChatForm: FormGroup;
  ChatMessages: Array<any> = new Array<any>();
  ChatReceivedMessages: Array<any> = new Array<any>();
  docId: string;
  doctorRoomName: string;
  public AllUserChats: any;
  apiStatusDestroy = false;
  closeResult = '';
  VisitId: any;
  DoctorId: any;
  isVideoFullscreen = false;
  isDoctor = false;
  vid: any;
  loginP: any;
  logout = false;
  @ViewChild('fullScreen', { static: true }) divRef;
  constructor(public httpClient: HttpClient,
    private modalService: NgbModal,
    public routing: Router, private authenticationService: AuthenticationService,
    private route: ActivatedRoute,
    public global: Global, public patService: PatientService, public dialog: MatDialog,
    private alertService: AlertService, public docService: DoctorService,
    private notifyService: NotificationService, public notificationService: ChatService, private formBuilder: FormBuilder) {
    this.token = this.authenticationService.currentUserValue;
    this.global.apiLogout = false;
    this.initForm();
    if (this.route.snapshot.queryParams['id']) {
      this.VisitId = this.route.snapshot.queryParamMap.get('id');
    }
    if (this.route.snapshot.queryParams['did']) {
      this.DoctorId = this.route.snapshot.queryParamMap.get('did');
    }
    if (this.token.UserType == "Doctor") {
      this.GetDoctorName();
      this.IsDoctor = true;
    } else {
      this.getPatientName();
      this.GetDoctorName();
      this.IsDoctor = false;
    }

    this.notificationService.Connect();
    this.notificationService.EventChatMessage.subscribe((chatData) => {
      if (this.ChatForm.controls["Id"].value != chatData.Name) {
        //  this.ChatForm.controls['Id'].setValue(chatData.Name);
        //  console.log(chatData);
        this.OnChatUserChange();
      }

      const chatMsg = {
        Name: chatData.Name,
        Message: chatData.Message,
        Class: "receiver-msg",
      };
      this.ChatMessages.push(chatMsg);
      // this.showChat = true;
      this.pushChatMsgUserwise(chatData.Name, chatMsg);

      //this.cdr.detectChanges();
      //this.scrollBottom.nativeElement.lastElementChild.scrollIntoView(); // scroll to bottom
    });
    // this.RefreshPatients();
    this.TimerDocandPat();
  }
  TimerDocandPat() {
    this.loginVal = this.authenticationService.loginValue;
    this.loginP = localStorage.getItem('loginPat');
    localStorage.setItem("istimer", "true");
    this.istimer = localStorage.getItem("istimer");
    if (this.token.UserType === "Patient" && this.istimer === "true" && this.apiDestroy === false && this.loginP === "patient") {

        this.timerpat.pipe(
          takeWhile(x => this.istimer === "true" && this.apiStatusDestroy === false && this.loginP === "patient")
        ).subscribe(
          () => {

            this.patService.getStatus(this.VisitId).subscribe(res =>
              this.StatusSuccess(res),
              res => this.Error(res));

          }
        );

    }
    else {

        this.timerdoc.pipe(
          takeWhile(x => this.istimer === "true")
        ).subscribe(
          () => {
              this.RefreshPatients();
            
          });
      }
  }

  ngOnInit() {
    // 
  }
  private initForm() {
    this.ChatForm = this.formBuilder.group({
      Id: [null, Validators.required],
      chatMessage: ['', Validators.required]
    });
  }
  hasError(typeofvalidator: string, controlname: string): boolean {
    const control = this.ChatForm.controls[controlname];
    return control.hasError(typeofvalidator) && control.dirty;
  }

  public GetDoctorName() {
    this.docService.getDoctor(this.token.DoctorIdentifier ? this.token.DoctorIdentifier : this.DoctorId).subscribe(res => this.SuccessDoctorNamme(res),
      res => this.Error(res));
  }
  SuccessDoctorNamme(res) {
    this.docInfo = res;
    this.docName = `${res.FirstName} ${res.MiddleName ? res.MiddleName : ""} ${res.LastName}`;
    this.docId = res.IdentityUserId;
    this.doctorRoomName = res.DoctorRoomName;
    this.docList.push({
      DoctorName: this.docName,
      IdentityUserId: ""
    });
    if (this.token.UserType == "Doctor") {
      this.vid = this.authenticationService.visitid;
      if (this.vid !== null) {
        this.patService.getPatientByVisitId(this.vid).subscribe(
          (res) => this.DoctorRoomBrowserRefreshPatient(res),
          (res) => this.ErrorPatAtt(res)
        );
      }
    } 
    const url = this.global.config.videourl;
    const urlParams = new URL(`${url}`);
    //const roomName = urlParams.set('room', `${this.doctorRoomName}`);
    const roomName = urlParams.searchParams.set('room',`${this.doctorRoomName}`);
    this.global.config.videourl = urlParams.toString();

    //if (this.token.UserType === "Doctor" || this.token.UserType === "Patient") {
    //this.domain = "meet.jit.si";
    //this.options = {
    //  roomName: `${this.doctorRoomName}_${this.docName}`,
    //  //width: 850,
    //  //height: 570,
    //  parentNode: document.querySelector('#meet'),
    //  configOverwrite: {
    //    doNotStoreRoom: true,
    //    disableInviteFunctions: true,
    //    startWithVideoMuted: true,
    //    startWithAudioMuted: true,
    //    enableWelcomePage: false,
    //    disableRemoteMute: true,
    //    prejoinPageEnabled: false,
    //    remoteVideoMenu: {
    //      // If set to true the 'Kick out' button will be disabled.
    //      disableKick: true
    //    },
    //  },
    //  interfaceConfigOverwrite: {
    //    SHOW_BRAND_WATERMARK: false,
    //    SHOW_CHROME_EXTENSION_BANNER: false,
    //    SHOW_JITSI_WATERMARK: false,
    //    SHOW_POWERED_BY: false,
    //    SHOW_WATERMARK_FOR_GUESTS: false,
    //    TOOLBAR_ALWAYS_VISIBLE: false,
    //    GENERATE_ROOMNAMES_ON_WELCOME_PAGE: false,
    //    DISPLAY_WELCOME_PAGE_CONTENT: false,
    //    DISPLAY_WELCOME_PAGE_TOOLBAR_ADDITIONAL_CONTENT: false,
    //    DEFAULT_REMOTE_DISPLAY_NAME: `${this.patInfo.PatientId}`,
    //    filmStripOnly: false,
    //    TOOLBAR_BUTTONS: ['microphone', 'camera', 'fullscreen', 'videoquality']
    //  }
    //}
    //this.api = new JitsiMeetExternalAPI(this.domain, this.options);

  }
  

  public getPatientName() {
    //var id = this.token.PatientIdentifier;
    this.patService.getPatient(this.token.PatientIdentifier).subscribe(
      (res) => {
        this.patInfo = res;
      },
      (error) => {
        this.alertService.error(error);
      }
    );
  }

  StatusSuccess(res) {
    var status = new Array<any>();
    status = res;
    this.loginP = localStorage.getItem('loginPat');
    if (status[0].Status == "completed") {
      this.apiDestroy = true;
      this.patService.VisitId = status[0].VisitId;
      this.apiStatusDestroy = true;
      this.routing.navigate(['/FinalReport']);
    }
    if (status[0].Status == "initiated" && status[0].IsActive == true) {
      this.apiDestroy = true;
      this.apiStatusDestroy = true;
      this.routing.navigate(['/WaitingRoom'], { queryParams: { redirectUrl: 'PatientUrl', did: this.DoctorId, vid: this.VisitId } });
    }
    if (status[0].Status == "initiated" && status[0].IsActive == false && this.loginP === "patient") {
      this.apiDestroy = true;
      this.apiStatusDestroy = true;
      this.routing.navigate(['/PatDashboard']);
    }
  }

  RefreshPatients() {
    var DoctorId = this.token.DoctorIdentifier;
 
    this.patService.getPatientDocList(DoctorId).subscribe(
      (res) => this.SuccessRefreshPatients(res),
      (res) => this.Error(res)
    );
  }
  SuccessRefreshPatients(res) {
    if (res.length > 0) {
      this.patients = res;
      if (this.patList.length == 0) {
        this.patList = res;
      }
      if (this.patList.length !== this.patients.length) {
          this.showCallButton = true;
        if (this.patients.length === 1) {
          this.chatPatId = this.patients[0].IdentityUserId;
          this.chatPatName = this.patients[0].PatientName;
        }
        this.patList = res;
      }
    } else if (res.length === 0) {
      localStorage.removeItem("istimer");
      localStorage.setItem('istimer', "false");
      this.istimer = localStorage.getItem('istimer');

      this.patients = [];
      //this.routing.navigate(['/DocDashboard']);
      this.routing.navigate(['/DocDashboard'], { queryParams: { redirectUrl: 'PatientUrl' } });
      this.notifyService.showInfo("Info", "There is no any Patient in Doctor Room ");
    }

    else {
      this.patients = [];
    }

  }
  DoctorRoomBrowserRefreshPatient(res) {
    this.visitObj = res;
    //this.visitObj.Status = "ongoing";
    //this.visitObj.IsActive = true;
    if (this.visitObj.Status === 'ongoing') {
      const DoctorId = this.token.DoctorIdentifier;

      this.patService.getPatientDocList(DoctorId).subscribe(
        (res) => this.SuccessBrowserRefreshPatients(res),
        (res) => this.Error(res)
      );
    }
  }
  SuccessBrowserRefreshPatients(res) {
    if (res.length > 0) {
      this.patients = res;
      if (this.patList.length === 0) {
        this.patList = res;
      }
      if (this.patList.length !== this.patients.length) {
        this.showCallButton = true;
        if (this.patients.length === 1) {
          this.chatPatId = this.patients[0].IdentityUserId;
          this.chatPatName = this.patients[0].PatientName;
        }
        this.patList = res;
      }
    } else if (res.length === 0) {
      localStorage.removeItem("istimer");
      localStorage.setItem('istimer', "false");
      this.istimer = localStorage.getItem('istimer');

      this.patients = [];
      //this.routing.navigate(['/DocDashboard']);
      this.routing.navigate(['/DocDashboard'], { queryParams: { redirectUrl: 'PatientUrl' } });
      this.notifyService.showInfo("Info", "There is no any Patient in Doctor Room ");
    }

    else {
      this.patients = [];
    }
    this.filteredpat = new Array<any>();
    this.filteredpat = JSON.parse(JSON.stringify(this.patients)); //deepcopy
    this.filteredpat = this.filteredpat.filter(
      (s) => s.VisitId == this.vid
    );
    this.showPatDetail = true;
  }

  CallPatient(VisitId: string, PatientId: string) {
    this.visitId = VisitId;
    this.vid = this.authenticationService.visitIdToken(VisitId);
    this.showCallButton = false;
    //this.showPatDetail = true;
    this.patService.getPatientByVisitId(VisitId).subscribe(
      (res) => this.SuccessCallPatient(res),
      (res) => this.ErrorPatAtt(res)
    );
  } 

  SuccessCallPatient(res) {
    this.visitObj = res;
    this.visitObj.Status = "ongoing";
    this.visitObj.IsActive = true;
    this.visitObj.VisitStartTime = new Date();
    //this.patService.updateStatus(this.visitObj, this.visitId)
    //  .subscribe(res =>
    //    this.alertService.success('Patient Called', true));
    this.filteredpat = new Array<any>();
    this.filteredpat = JSON.parse(JSON.stringify(this.patients)); //deepcopy
    this.filteredpat = this.filteredpat.filter(
      (s) => s.VisitId == this.visitId
    );
    this.showPatDetail = true;
    this.UpdateStatus();
  }



  UpdateStatus() {
    this.patService.updateStatus(this.visitObj, this.visitId)
      .subscribe(res => this.SuccessUpdateStaus(res),
        res => this.ErrorPatAtt(res));
  }
  SuccessUpdateStaus(res) {
    this.alertService.success('Patient Called', true);
    this.patService.PatientByVisit = false;
    this.RefreshPatients();
    this.logout = false;
    this.timerlogout.pipe(
      takeWhile(x => this.logout === false)
    ).subscribe(
      () => {

        this.patService.getStatus(this.visitId).subscribe(res =>
          this.StatusLogoutSuccess(res),
          res => this.Error(res));

      }
    );
  }
  StatusLogoutSuccess(res) {
    let status = new Array<any>();
    status = res;
    if (status[0].Status === "initiated" && status[0].IsActive === false) {
      
      this.logout = true;
      this.RefreshPatients();
      this.showCallButton = false;
      this.apiStatusDestroy = true;

    }
  }
  GetDocuments() {
    //this.visitId = this.visitId;
    this.clickDocument = true;
    this.patService.getPatientDocuments(this.visitId).subscribe(
      (res) => {
        this.documents = res;
        if (this.documents.length > 0) {
          this.showDocument = true;
        }
        //this.filedoc = res;
        //for (var i = 0; i < this.documents.length; i++) {
        //  //this.documents[i].VisitDate = moment(this.documents[i].VisitDate ).format('lll');
        //}
      },
      (error) => {
        this.alertService.error(error);
        //this.loading = false;
      });
  }
  openDialog(VisitId) {
    const dialogRef = this.dialog.open(PopUpDialogComponent, {
      data: { msg: "fromDocRoom" }, disableClose: true ,
      width: '250px',
      height: '300',
    },);

    dialogRef.afterClosed().subscribe(result => {
      this.isCancelled = result;
      if (this.isCancelled === true) {
        this.PatientAttended(VisitId);
      }
      else
      {
        this.patService.changeStatus(VisitId)
          .subscribe(res => this.SuccesschangeStatus(res),
            res => this.Error(res));
      }
    });

  }
  SuccesschangeStatus(res) {
    //this.apiStatusDestroy = true;
    //this.apiDestroy = true;
    this.showCallButton = true;
  }

  PatientAttended(VisitId) {
    this.showPatDetail = false;
    this.showCallButton = true;
    this.patService.getPatientByVisitId(VisitId).subscribe(
      (res) => this.SuccessPatientAttend(res, VisitId),
      (res) => this.ErrorPatAtt(res)
    );
  }

  SuccessPatientAttend(res, VisitId) {
    //this.patService.VisitId = VisitId;
    this.visitdata = Object.assign(this.visitdata, res)
    this.visitdata.IsActive = false;
    this.visitdata.Status = "completed";
    this.visitdata.TreatmentAdvice = this.visitObj.TreatmentAdvice;
    this.visitdata.Medication = this.visitObj.Medication;
    this.visitdata.FollowUp = this.visitObj.FollowUp;
    localStorage.removeItem('vid');
    this.patService.updateTreatmentAdvice(this.visitdata, VisitId).subscribe(
      (res) =>
        this.notifyService.showInfo("Treatment Advice Added !", "Success"),
      (res) => this.Error(res)
    );
    const DoctorId = this.token.DoctorIdentifier;
    this.patService.getPatientDocList(DoctorId).subscribe(
      (res) => this.SuccessRefreshPatients(res),
      (res) => this.Error(res)
    );

    this.PatientCompleted();
  }
  PatientCompleted() {
    //this.patService.VisitId = null;
    this.filteredpat = new Array<any>();
    this.RefreshPatients();
  }
  ErrorPatAtt(res) {}

  Error(res) {
    alert(res.status);
  }

  preview(filePath, fileExt, Title) {
    this.showView = true;
    this.Title = Title;
    //window.open(filePath);
    this.path = filePath;
    this.fileExtension = fileExt;
    if (
      this.fileExtension == "jpeg" ||
      this.fileExtension == "JPEG" ||
      this.fileExtension == "png" ||
      this.fileExtension == "PNG" ||
      this.fileExtension == "JPG" ||
      this.fileExtension == "jpg"
    ) {
      this.Filetype = "image";
    }
    if (this.fileExtension == "pdf" || this.fileExtension == "PDF") {
      this.Filetype = "pdf";
    }
    if (
      this.fileExtension == "docx" ||
      this.fileExtension == "DOCX" ||
      this.fileExtension == "DOC" ||
      this.fileExtension == "doc"
    ) {
      this.Filetype = "docfile";
    }
  }
  hide() {
    this.showView = false;
  }
  SendChatMsg() {
    if (this.token.UserType == "Patient") {
      this.ChatForm.controls["Id"].setValue(this.docName);
    }
    try {
      for (const i in this.ChatForm.controls) {
        this.ChatForm.controls[i].markAsDirty();
        this.ChatForm.controls[i].updateValueAndValidity();
      }

      if (this.ChatForm.valid) {
        const chatMsg = {
          SenderId: this.IsDoctor
            ? this.docInfo.IdentityUserId
            : this.patInfo.IdentityUserId,
          ReceiverId: this.IsDoctor
            ? this.chatPatId
            : this.docInfo.IdentityUserId,
          Name: this.IsDoctor
            ? `${this.docInfo.FirstName} ${
                this.docInfo.MiddleName ? this.docInfo.MiddleName : ""
              } ${this.docInfo.LastName}`
            : `${this.patInfo.FirstName} ${
                this.patInfo.MiddleName ? this.patInfo.MiddleName : ""
              } ${this.patInfo.LastName}`,
          ReceiverName: this.IsDoctor ? this.chatPatName : this.docName,
          Message: this.ChatForm.controls["chatMessage"].value,
        };
        const chatmsgObj = {
          Name: "Me",
          Message: chatMsg.Message,
          Class: "sender-msg",
        };
        this.ChatMessages.push(chatmsgObj);
        this.pushChatMsgUserwise(
          this.ChatForm.controls["Id"].value.PatientName,
          chatmsgObj
        );

        // this.cdr.detectChanges();
        // this.scrollBottom.nativeElement.lastElementChild.scrollIntoView(); // scroll to bottom

        this.notificationService.SendChatMessage(chatMsg);

        // this.ChatForm.reset();
        this.ChatForm.controls["chatMessage"].reset();
        if (this.token.UserType == "Doctor") {
          this.ChatForm.controls["Id"].value.setValue(chatMsg.Name);
        } else {
          this.ChatForm.controls["Id"].setValue(this.chatPatName);
        }
      }
    } catch (e) {}
  }

  OnChatUserChange() {
    try {
      const id = this.ChatForm.controls["Id"].value.PatientName;
      // const id2 = this.ChatForm.controls['Id'].value.PatientName;
      this.chatPatId = this.ChatForm.controls["Id"].value.IdentityUserId;
      this.chatPatName = this.ChatForm.controls["Id"].value.PatientName;
      if (this.AllUserChats.hasOwnProperty(id)) {
        this.ChatMessages = this.AllUserChats[id].slice();
        this.ChatReceivedMessages = this.AllUserChats[id].slice();
      } else {
        this.ChatMessages = new Array<any>();
        this.ChatReceivedMessages = new Array<any>();
      }
    } catch (e) {}
  }

  onChatEnter(event) {
    if (event.keyCode === 13) {
      this.SendChatMsg();
    }
  }
  pushChatMsgUserwise(user, messageObj) {
    try {
      if (!this.AllUserChats.hasOwnProperty(user)) {
        this.AllUserChats[user] = new Array<any>();
      }
      this.AllUserChats[user].push(messageObj);
    } catch (e) {}
  }
  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', windowClass: 'modal-send-message'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  openVideo() {
    //this.isVideoFullscreen = true;
    //$(".btn-fullscreen").click(function () {
    //  $(".frame-area.video-frame").toggleClass("fullscreenactive");
    //});
    //$(document).ready(function () {
    //  $(".btn-fullscreen").click(function () {
    //    $(".video-frame").toggleClass("fullscreenactive");
    //  });
    //});
    //const url = this.global.config.videourl;
    //const urlParams = new URL(`${url}`);
    ////const roomName = urlParams.set('room', `${this.doctorRoomName}`);
    //const roomName = urlParams.searchParams.set('room', `${this.doctorRoomName}`);
    //this.global.config.videourl = urlParams.toString();
    //this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', windowClass: 'modal-fullscreen'}).result.then((result) => {
    //  this.closeResult = `Closed with: ${result}`;
    //}, (reason) => {
       
    //  this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    //});
  }
  private getDismissReason(reason: any): string {
   
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  exitFullscreen() {
    this.isVideoFullscreen = false;
  }
  openFullscreen() {
    this.isVideoFullscreen = true;
    // Use this.divRef.nativeElement here to request fullscreen
    const elem = this.divRef.nativeElement;

    if (elem.requestFullscreen) {
      elem.requestFullscreen();
    } else if (elem.msRequestFullscreen) {
      elem.msRequestFullscreen();
    } else if (elem.mozRequestFullScreen) {
      elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) {
      elem.webkitRequestFullscreen();
    }
  }
  closeFullscreen() {
    this.isVideoFullscreen = false;
    //const elem = this.divRef.nativeElement;
    if (document.exitFullscreen) {
      document.exitFullscreen();
    }
  }

  //openFullscreen() {
  //  this.isVideoFullscreen = true;
  //  // Trigger fullscreen
  //  const docElmWithBrowsersFullScreenFunctions = document.documentElement as HTMLElement & {
  //    mozRequestFullScreen(): Promise<void>;
  //    webkitRequestFullscreen(): Promise<void>;
  //    msRequestFullscreen(): Promise<void>;
  //  };

  //  if (docElmWithBrowsersFullScreenFunctions.requestFullscreen) {
  //    docElmWithBrowsersFullScreenFunctions.requestFullscreen();
  //  } else if (docElmWithBrowsersFullScreenFunctions.mozRequestFullScreen) { /* Firefox */
  //    docElmWithBrowsersFullScreenFunctions.mozRequestFullScreen();
  //  } else if (docElmWithBrowsersFullScreenFunctions.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
  //    docElmWithBrowsersFullScreenFunctions.webkitRequestFullscreen();
  //  } else if (docElmWithBrowsersFullScreenFunctions.msRequestFullscreen) { /* IE/Edge */
  //    docElmWithBrowsersFullScreenFunctions.msRequestFullscreen();
  //  }
  //}

  //closefullscreen() {
  //  this.isVideoFullscreen = false;;
  //  const docWithBrowsersExitFunctions = document as Document & {
  //    mozCancelFullScreen(): Promise<void>;
  //    webkitExitFullscreen(): Promise<void>;
  //    msExitFullscreen(): Promise<void>;
  //  };
  //  if (docWithBrowsersExitFunctions.exitFullscreen) {
  //    docWithBrowsersExitFunctions.exitFullscreen();
  //  } else if (docWithBrowsersExitFunctions.mozCancelFullScreen) { /* Firefox */
  //    docWithBrowsersExitFunctions.mozCancelFullScreen();
  //  } else if (docWithBrowsersExitFunctions.webkitExitFullscreen) { /* Chrome, Safari and Opera */
  //    docWithBrowsersExitFunctions.webkitExitFullscreen();
  //  } else if (docWithBrowsersExitFunctions.msExitFullscreen) { /* IE/Edge */
  //    docWithBrowsersExitFunctions.msExitFullscreen();
  //  }
  //}
}
