import { Component } from '@angular/core'
import { Patient } from '../../models/patient/patient.model';
import { PatientService } from '../../services/patient/patient.service';
import { AlertService } from '../../services/alert.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { DatePipe } from '@angular/common';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { NotificationService } from '../../services/notification.service';
import { HospitalService } from '../../services/hospital/hospital.service';
import * as moment from 'moment/moment';
import { Global } from '../../app.global';
@Component({
  selector: 'patient-update-profile',
  templateUrl: './app.patient_updateprofile.html'
})
export class PatientUpdateProfileComponent {
  public updatePat: boolean;
  public patInfo: Patient = new Patient();
  public today = new Date();
  public patList: Array<Patient> = new Array<Patient>();
  loading = false;
  public pat: any;
  public token: any;
  public hospitalId: any;
  patientUpdateForm: FormGroup;
  bsValue = new Date();
  isPatEditor = false;
  patId: any;
  patName: string;
  public hospitalIdentifier: string;
  public changesSavedCallback: () => void;
  public changesCancelledCallback: () => void;
  constructor(public patservice: PatientService, public alertService: AlertService, private route: ActivatedRoute,
    public hospitalService: HospitalService,
    public routing: Router, private authenticationService: AuthenticationService, private datePipe: DatePipe,
    private formBuilder: FormBuilder, private notifyService: NotificationService, public global: Global) {
    this.token = this.authenticationService.currentUserValue;
    if (this.token.UserType !== "Admin") {
      this.GetPatient();
      this.hospitalId = this.route.snapshot.queryParamMap.get('id');
      this.patInfo.DateOfBirth = moment().format('MM-DD-YYYY');
    }
  
  }


  GetPatientList() {
    this.patservice.getPatientList().subscribe(res => {
      this.patList = res;
    },
      error => {
        this.alertService.error(error);
        this.loading = false;
      });
  }
  GetPatient() {

    //var id = this.token.PatientIdentifier;
    if (this.token.UserType !== "Admin") {
      this.patId = this.token.PatientIdentifier;
    }
    
    this.patservice.getPatient(this.patId).subscribe(res => {
      this.patInfo = res;
     // this.patInfo.DateOfBirth = this.datePipe.transform(res.DateOfBirth, 'yyyy-MM-dd');
      //this.bsValue =this.patInfo.DateOfBirth;
      if (this.patInfo.DateOfBirth != null){
        this.patInfo.DateOfBirth = moment(res.DateOfBirth).format('MM-DD-YYYY');
      }
      else{
        this.patInfo.DateOfBirth =moment().format('MM-DD-YYYY');
      }
      
    },
      error => {
        this.alertService.error(error);
        this.loading = false;
      });
  }

  UpdatePatProfile(form: NgForm) {
    if (this.patInfo.ContactNumber == "") {
      this.notifyService.showError("", "Phone Number is required");
      return;
    }
    if (form.invalid) {
      this.notifyService.showError("", "Please fill up the required field");
      return;
    }
    const momentDate = new Date(this.patInfo.DateOfBirth);
    if (this.token.UserType === "Admin") {
      this.patInfo.AdminId = this.token.unique_name;
    }
    else {
      this.patInfo.AdminId = "";
    }

    this.patInfo.DateOfBirth = moment(momentDate).format("YYYY/MM/DD");// converted gmtdate  to moment date
    //this.patInfo.IdentityUserId =this.token.unique_name;
    this.patInfo.HospitalIdentifier = this.hospitalIdentifier;
    this.patservice.updatePatient(this.patInfo).subscribe(data => {
      this.alertService.success('Updated successful', true);
      this.patInfo.PatientId = this.token.PatientIdentifier;;
      this.updatePat = false;
      if (this.token.UserType === "Admin") {
        this.routing.navigate(['/adminpatlist']);
        if (this.changesSavedCallback) {
          this.changesSavedCallback();
        }
      }
      else {
        this.routing.navigate(['/PatDashboard'], { queryParams: { id: this.hospitalId } });
      }
    },
      error => {
        this.alertService.error(error);
        this.loading = false;
      });
  }
  updatepatient() {
    
    const momentDate = new Date(this.patInfo.DateOfBirth);
    if (this.token.UserType === "Admin") {
      this.patInfo.AdminId = this.token.unique_name;
    }
    

    this.patInfo.DateOfBirth = moment(momentDate).format("YYYY/MM/DD");// converted gmtdate  to moment date
    //this.patInfo.IdentityUserId =this.token.unique_name;
    this.patInfo.HospitalIdentifier = this.hospitalIdentifier;
    this.patservice.updatePatient(this.patInfo).subscribe(data => {
      this.alertService.success('Updated successful', true);
      if (this.token.UserType === "Admin") {
        this.routing.navigate(['/adminpatlist']);
        if (this.changesSavedCallback) {
          this.changesSavedCallback();
        }
      }
      else {
        this.routing.navigate(['/PatDashboard'], { queryParams: { id: this.hospitalId } });
      }
    },
      error => {
        this.alertService.error(error);
        this.loading = false;
      });
  }
  public BackDashBoard() {
    this.routing.navigate(['/PatDashboard'], { queryParams: { id: this.hospitalId } });
  }
  editAdminPatient(pat: Patient, isPat: boolean, hospital: string) {
    this.isPatEditor = isPat;
    this.patId = pat.PatientId;
    this.patName = pat.FullName;
    this.hospitalIdentifier = hospital;
    this.GetPatient();

    return this.patId;
  }
  cancel() {
    if (this.changesCancelledCallback) {
      this.changesCancelledCallback();
    }
  }
}
