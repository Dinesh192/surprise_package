import { Component } from '@angular/core'
import { Global } from '../../app.global';
import { Router, ActivatedRoute } from '@angular/router';
import { Patient } from '../../models/patient/patient.model';
import { PatientService } from '../../services/patient/patient.service';
import { AuthenticationService } from '../../services/authentication.service';
import { AlertService } from '../../services/alert.service';
import { HospitalService } from '../../services/hospital/hospital.service';
import { DoctorService } from '../../services/doctor/doctor.service';
import { NotificationService } from '../../services/notification.service';
import * as moment from 'moment/moment';
import { ConsentFormComponent } from '../../ConsentForm/app.consentformcomponent';
import { MatDialog } from '@angular/material';

@Component({
  templateUrl: "./app.patient_dashboard.html",
})
export class PatientDashboardComponent {
  public updatePat: boolean;
  public showViewNotes: boolean = false;
  public prob: boolean = false;
  filekind: any;
  fileExtension: any;
  Filetype: any;
  p: number = 1;
  q: number = 1;
  r: number = 1;
  nearestVisit: any;
  public showPrescription: boolean = false;
  public showNearestVisit: boolean = false;
  public visitId: string;
  public patInfo: Patient = new Patient();
  public token: any;
  nepaliDate = moment();
  public allpatBookList: any;
  public viewList: any;
  path: any;
  public noteList: any;
  filterPatList: any;
  patientFile: any;
  public patBookList: any;
  public patHistoryList: any;
  public allVisitsList: any;
  public hospitalId: any;
  public doctordId: any;
  allcollection = { count: 60, data: [] };
  collection = { count: 60, data: [] };
  historycollection = { count: 0, data: [] };
  redUrl: string;
  constructor(public global: Global, public routing: Router, public docService: DoctorService, public dialog: MatDialog, public patservice: PatientService,public hospitalService:HospitalService,
    public authservice: AuthenticationService, public alertService: AlertService, private route: ActivatedRoute, private notifyService: NotificationService) {
    this.token = this.authservice.currentUserValue;
    this.patservice.PatientId = this.token.PatientIdentifier;
    this.getPatientName();
    this.GetAllVisits();
    this.hospitalId = this.route.snapshot.queryParamMap.get('id');
    this.doctordId = this.route.snapshot.queryParamMap.get('id1');
    this.redUrl = this.route.snapshot.queryParamMap.get('redirectUrl');
    this.ShowAllBookList();
    this.ShowBookList();
    this.AppointmentHistory();
  }
  public getPatientName() {
    //var id = this.token.PatientIdentifier;
    this.patservice.getPatient(this.token.PatientIdentifier).subscribe(
      (res) => {
        this.patInfo = res;
      },
      (error) => {
        this.alertService.error(error);
      }
    );
  }
  public BookAppointment() {
    this.updatePat = true;
    //this.patDash = true;
    this.routing.navigate(["/HospitalList"]);
  }
  public History() {
    this.updatePat = true;
    this.routing.navigate(["/PatDashboard/PatHistory"], {
      queryParams: { id: this.hospitalId },
    });
  }

  public payment() {
    this.updatePat = true;
    this.routing.navigate(["/PatDashboard/PaymentSelection"], {
      queryParams: { id: this.hospitalId },
    });
  }
  public Update() {
    this.updatePat = true;
    this.routing.navigate(["/PatDashboard/PatUpdateProfile"], {
      queryParams: { id: this.hospitalId },
    });
  }
  public HospitalDashBoard() {
    this.global.DepartmentId = null;
    this.hospitalService.HospitalId = null;
    this.hospitalService.DepartmentId = null;
    this.global.DoctorId = null;
    this.docService.DoctorId = null;
    this.routing.navigate(["/HospitalList"]);
  }
  public UpdateProfile() {
    this.routing.navigate(["/PatUpdateProfile"], {
      queryParams: { id: this.hospitalId },
    });
  }

  public PatientHistory() {
    this.routing.navigate(["/PatHistory"], {
      queryParams: { id: this.hospitalId },
    });
  }
  public BookingList() {
    this.routing.navigate(["/PatBookList"]);
  }
  public PaidBookingList() {
    this.routing.navigate(["/PatPaidBookingList"]);
  }
  public GetAllVisits() {
    if (this.redUrl === null) {
      this.patservice.getPatientBookingList(this.token.PatientIdentifier).subscribe(res => {
        this.allVisitsList = res;
        for (var i = 0; i < this.allVisitsList.length; i++) {
          if ((this.allVisitsList[i].Status === 'ongoing' || this.allVisitsList[i].Status === 'initiated') && this.allVisitsList[i].IsActive === true) {
            this.patservice.changeStatus(this.allVisitsList[i].VisitId)
              .subscribe(res => this.SuccesschangeStatus(res),
                res => this.Error(res));
          }
        }
      },
        error => {
          this.alertService.error(error);
          // this.loading = false;
        });
    }
    else if (this.redUrl === "PatientUrl") {
      this.patservice.getPatientBookingList(this.token.PatientIdentifier).subscribe(res => {
        this.allVisitsList = res;
        for (var i = 0; i < this.allVisitsList.length; i++) {
          if ((this.allVisitsList[i].Status === 'ongoing' || this.allVisitsList[i].Status === 'initiated') && this.allVisitsList[i].IsActive === true) {
            this.patservice.changeStatus(this.allVisitsList[i].VisitId)
              .subscribe(res => this.SuccesschangeStatus(res),
                res => this.Error(res));
          }
        }
      },
        error => {
          this.alertService.error(error);
          // this.loading = false;
        });
    }
    else if (this.redUrl === undefined) {
      this.patservice.getPatientBookingList(this.token.PatientIdentifier).subscribe(res => {
        this.allVisitsList = res;
        for (var i = 0; i < this.allVisitsList.length; i++) {
          if ((this.allVisitsList[i].Status === 'ongoing' || this.allVisitsList[i].Status === 'initiated') && this.allVisitsList[i].IsActive === true) {
            this.patservice.changeStatus(this.allVisitsList[i].VisitId)
              .subscribe(res => this.SuccesschangeStatus(res),
                res => this.Error(res));
          }
        }
      },
        error => {
          this.alertService.error(error);
          // this.loading = false;
        });
    }
    else {
      return;
    }
    
  }
  SuccesschangeStatus(res) {
   // this.notifyService.showInfo("Status Changed To Initiated!", "Success")
  }
 
  // Slick Slider this is added new

  slides = [{ img: "./assets/img/Slider11.jpg" }];
  slideConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    fade: true,
    cssEase: "linear",
    draggable: false,
    autoplaySpeed: 5000,
    arrows: false,
    dots: false,
  };
  // End SLick slider
  public ShowAllBookList() {
    this.patservice.getPatientBookingList(this.token.PatientIdentifier).subscribe(res => {
      this.allpatBookList = res;

      for (var i = 0; i < this.allpatBookList.length; i++) {
        this.allpatBookList[i].VisitDate = moment(this.allpatBookList[i].VisitDate).format('ll');
        this.allpatBookList[i].VisitStartTime = moment(this.allpatBookList[i].VisitStartTime).format('LT');
      }
      this.collection = { count: this.allpatBookList.length, data: [] }
      this.allcollection={ count: this.allpatBookList.length, data: [] }
      for (var i = 0; i < this.allcollection.count; i++) {

        this.allcollection.data.push(
          {
            id: i + 1,
            VisitDate: this.allpatBookList[i].VisitDate,
            HospitalName: this.allpatBookList[i].HospitalName,
            DoctorName: this.allpatBookList[i].DoctorName,
            DepartmentName: this.allpatBookList[i].DepartmentName,
            VisitType: this.allpatBookList[i].VisitType,
            PaymentStatus: this.allpatBookList[i].PaymentStatus,
            VisitId: this.allpatBookList[i].VisitId,
          }
        );
      }


    },
      error => {
        this.alertService.error(error);
      });

  }
  public ShowBookList() {
    //this.patbook = true;
    this.patservice.getPatientBookingList(this.token.PatientIdentifier).subscribe(res => {
      this.patBookList = res;

      for (var i = 0; i < this.patBookList.length; i++) {
        var currentNepaliDate = this.nepaliDate.tz("Asia/Kathmandu").format('YYYY/MM/DD HH:mm z');
        var currentNepaliDateTime = this.nepaliDate.tz("Asia/Kathmandu").format('YYYY/MM/DD HH:mm z');
        var currentdate = moment(currentNepaliDate, 'YYYY/MM/DD').format('YYYY/MM/DD');
        var currentDateWithTime = moment(currentNepaliDateTime, 'YYYY/MM/DD hh:mm').format('YYYY/MM/DD hh:mm');
        var currentYear = moment(currentNepaliDate, 'YYYY/MM/DD').format('YYYY');
        var currentMonth = moment(currentNepaliDate, 'YYYY/MM/DD').format('M');
        var currentDay = moment(currentNepaliDate, 'YYYY/MM/DD').format('D');
        this.patBookList[i].VisitDate = moment(this.patBookList[i].VisitDate).format('ll');
        if (this.patBookList[i].VisitDate == moment(this.nepaliDate).format('ll')) {
          var todaydate = new Date();
          var currentdate1 = this.nepaliDate.format('YYYY/MM/DD');
          var index = this.patBookList[i].BookingTime.indexOf("-");
          var timeextract = this.patBookList[i].BookingTime.substring(0, index - 1);
          var bookedDateTime = moment(currentdate + ' ' + timeextract, 'YYYY/MM/DD HH:mm A');
          var formatedBookedDateTime = moment(bookedDateTime, 'YYYY-MM-DD HH:mm A');
          // var temp = moment(formatedBookedDateTime, 'YYYY-MM-DD HH:mm A').subtract(15, 'minutes').format('YYYY-MM-DD HH:mm A');
          // var formatedBookedDateTime = moment(temp, 'YYYY-MM-DDTHH:mm:s');
          var DocStartTime = moment(currentdate + ' ' + this.patBookList[i].DocStartTime, 'YYYY/MM/DD HH:mm A');
          var formatedreducedStartTime = moment(DocStartTime, 'YYYY-MM-DD HH:mm A');
          var decStartTime = moment(formatedreducedStartTime, 'YYYY-MM-DD HH:mm A').subtract(15, 'minutes').format('YYYY-MM-DD HH:mm A');
          var formatedDecStartTime = moment(decStartTime, 'YYYY-MM-DDTHH:mm:A');
          var DocEndtime = moment(currentdate + ' ' + this.patBookList[i].DocEndTime, 'YYYY/MM/DD HH:mm A');
          var formattedEndTime = moment(DocEndtime, 'YYYY-MM-DD HH:mm A');
          var currentTime1 = moment(todaydate).tz("Asia/Kathmandu").format('YYYY-MM-DD HH:mm z');
          var currentTime = moment(currentTime1, 'YYYY/MM/DD hh:mm').format('YYYY-MM-DD HH:mm A');
          var momentObj = moment(currentTime, 'YYYY-MM-DDLT');
          var currentdateTime = momentObj.format('YYYY-MM-DDTHH:mm:s');
          var formatedCurrentTime = moment(currentdateTime, 'YYYY-MM-DD HH:mm A');
          //  var formatedCurrentTime = moment(currentTime, 'YYYY-MM-DD HH:mm A');
          // var diffmin = formatedBookedDateTime.diff(formatedCurrentTime, 'minutes');
          if ((formatedDecStartTime <= formatedCurrentTime && formatedCurrentTime <= formattedEndTime) && this.patBookList[i].IsConversationCompleted == 0 && this.patBookList[i].PaymentStatus != 'unpaid' ) {
            //if (diffmin <= 15) {
            //  this.patBookList[i].GoToWaitingRoom = true;
            //  this.patBookList[i].AccessPermission = true;
            //  this.patBookList[i].EntryStatus = "go"
            //} else {
            //  this.patBookList[i].AccessPermission = false;
            //  this.patBookList[i].EntryStatus = "todayupcoming";
            this.patBookList[i].GoToWaitingRoom = true;
            this.patBookList[i].AccessPermission = true;
            this.patBookList[i].EntryStatus = "go";
            this.nearestVisit = new Array<any>();
            this.nearestVisit = JSON.parse(JSON.stringify(this.patBookList));//deepcopy
            this.nearestVisit = this.patBookList[i];
            this.showNearestVisit = true;

          }
          else {
            var diffminwithStarttime = formatedCurrentTime.diff(formatedDecStartTime, 'minutes');
            var diffminwithEndtime = formatedCurrentTime.diff(formattedEndTime, 'minutes');
            if (diffminwithStarttime < 0) {
              this.patBookList[i].AccessPermission = false;
              this.patBookList[i].EntryStatus = "todayupcoming";
            }
            if (diffminwithEndtime > 0) {
              this.patBookList[i].AccessPermission = false;
              this.patBookList[i].EntryStatus = "todaypast";
            }
          }


        }
        if (this.patBookList[i].VisitDate < moment(currentDateWithTime).format('ll')) {
          this.patBookList[i].AccessPermission = false;
          this.patBookList[i].GoToWaitingRoom = false;
          this.patBookList[i].EntryStatus = "missed";
        }
        if (this.patBookList[i].VisitDate > moment(currentDateWithTime).format('ll')) {
          this.patBookList[i].AccessPermission = false;
          this.patBookList[i].GoToWaitingRoom = false;
          this.patBookList[i].EntryStatus = "upcoming";
        }
        this.patBookList[i].VisitDate = moment(this.patBookList[i].VisitDate).format('ll');

        this.patBookList[i].VisitStartTime = moment(this.patBookList[i].VisitStartTime).format('LT');
      }
      this.collection = { count: this.patBookList.length, data: [] }

      for (var i = 0; i < this.collection.count; i++) {
        this.collection.data.push(
          {
            id: i + 1,
            VisitDate: this.patBookList[i].VisitDate,
            HospitalName: this.patBookList[i].HospitalName,
            DoctorName: this.patBookList[i].DoctorName,
            DepartmentName: this.patBookList[i].DepartmentName,
            VisitType: this.patBookList[i].VisitType,
            Status: this.patBookList[i].Status,
            ProviderId: this.patBookList[i].ProviderId,
            HospitalId: this.patBookList[i].HospitalId,
          }
        );
      }
    },
      error => {
        this.alertService.error(error);
        // this.loading = false;
      });
  }
    public paymentselection(data) {
    this.routing.navigate(['/PaymentSelection'], { queryParams: { vid: data.VisitId, charge: data.Charge, id: this.hospitalId } });
  }

  GotoWaitingRoom(data) {
    this.docService.DoctorId = data.ProviderId;
    const dialogRef = this.dialog.open(ConsentFormComponent,
      { data: { DoctorId: data.ProviderId, HospitalId: data.HospitalId }, width: '1300px', height: '1000px' }
    );
    dialogRef.disableClose = true;
    dialogRef.afterClosed().subscribe(result => {
      dialogRef.close();
      console.log(`Dialog result: ${result}`);
      // this.visitObj.DepartmentId = this.hospitalService.DepartmentId;
      this.routing.navigate(['/WaitingRoom'], { queryParams: { vid: data.VisitId, id: this.hospitalId, did: data.ProviderId } });
    });
    //this.patService.VisitId = data.VisitId;
  }
  public AppointmentHistory() {
    this.patservice.getPatHistory(this.token.PatientIdentifier).subscribe(res => this.Success(res),
      res => this.Error(res));
  }

  Success(res) {
    this.patHistoryList = res;
    console.log(this.patHistoryList );
    this.historycollection = { count: this.patHistoryList.length, data: [] }
    for (var i = 0; i < this.historycollection.count; i++) {
      this.historycollection.data.push(
        {
          id: i + 1,
          VisitId: this.patHistoryList[i].VisitId,
          VisitDate: this.patHistoryList[i].VisitDate,
          DoctorName: this.patHistoryList[i].DoctorName,
          HospitalName: this.patHistoryList[i].HospitalName,
          Status: this.patHistoryList[i].Status,
          NMC: this.patHistoryList[i].NMC
        }
      );
    }

  }
  Error(res) {

  }

  public ViewNotes(visitId) {
    this.showViewNotes = true;
    this.patservice.getPatientHistory(this.token.PatientIdentifier).subscribe(res => {
      this.viewList = res;
      this.noteList = res;
      this.viewList = this.viewList.filter(s => s.VisitId == visitId);
      for (var i = 0; i < this.viewList.length; i++) {

        this.viewList[i].VisitDate = moment(this.viewList[i].VisitDate).format('lll');
      }
      for (let index = 0; index < this.viewList.length; index++) {
        const element = this.viewList[index];
        if (element == true) {
          this.prob = false;
        }
      }

    },
      error => {
        this.alertService.error(error);
      });
  }
  ViewPrescription(id) {
    this.visitId = id;
    this.filterPatList = new Array<any>();
    this.filterPatList = JSON.parse(JSON.stringify(this.patHistoryList));//deepcopy
    this.filterPatList = this.filterPatList.filter(x => x.VisitId == this.visitId);
    this.patientFile = this.filterPatList[0].PatientFile;
    if (this.patientFile.length > 0) {
      this.showPrescription = true;
      this.path = this.patientFile[0].FilePath;
      this.fileExtension = this.patientFile[0].FileExtention;
      if (this.fileExtension == 'jpeg' || this.fileExtension == 'JPEG' || this.fileExtension == 'png' || this.fileExtension == 'PNG' || this.fileExtension == 'JPG' || this.fileExtension == 'jpg') {
        this.Filetype = "image";
      }
      if (this.fileExtension == 'pdf' || this.fileExtension == 'PDF') {
        this.Filetype = "pdf";
      }
      if (this.fileExtension == 'docx' || this.fileExtension == 'DOCX' || this.fileExtension == 'DOC' || this.fileExtension == 'doc') {
        this.Filetype = "docfile";
      }

    }
    else {
      this.showPrescription = false;
      this.notifyService.showError("Error!", " Could not found the Prescription file");
    }
  }

  public Close() {
    this.showViewNotes = false;
  }
  Print() {
    let popupWinindow;
    var printContents = document.getElementById("Report").innerHTML;
    popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
    popupWinindow.document.open();
    let documentContent = '<html><head>';
    documentContent += '<link rel="stylesheet" type="text/css" />';
    documentContent += '</head>';
    documentContent += '<body onload="window.print()" style="margin:8px 0px 0px 50px !important;">' + printContents + '</body></html>'
    popupWinindow.document.write(documentContent);
    popupWinindow.document.close();
  }
}
