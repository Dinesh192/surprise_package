import { Component } from '@angular/core'
import { Global } from '../../app.global';
import { Router, ActivatedRoute } from '@angular/router';
import { config } from 'rxjs';

import * as KhaltiCheckout from 'khalti-web'
import { PatientService } from '../../services/patient/patient.service';
import { Visit } from '../../models/visit/visit.model';
import { AlertService } from '../../services/alert.service';
import { HospitalService } from '../../services/hospital/hospital.service';
import { MatDialog } from '@angular/material';
import { PaymentMessageComponent } from '../../PaymentMessage/app.paymentmessage.component';

@Component({
  templateUrl: "./app.paymentselection.html"
})
export class PaymentSelectionComponent {
  IsCardPayButtonDisabled: boolean = false;
  VisitId: string;
  charge: string;
  public hospitalId: string;
  public docId : string;
  public visitInfo: Visit = new Visit();
  constructor(public routing: Router, private route: ActivatedRoute,
    public hospitalService: HospitalService,
    public patService: PatientService, public alertService: AlertService, private dialog: MatDialog) {
    this.hospitalId = this.hospitalService.HospitalId;
    this.VisitId = this.route.snapshot.queryParamMap.get('vid');
    this.docId = this.route.snapshot.queryParamMap.get('id1');
    this.GetHospitalIdAndVisitIdFromLocalStorage();
    this.PaymentCharge();
  }

  paidstatus() {
    this.visitInfo.VisitId = this.VisitId;
    this.patService.updatePaidStatus(this.VisitId).subscribe(data => {
    },
      error => {
        this.alertService.error(error);
      });
  }
  PaymentCharge() {
    this.patService.getPaymentCharge(this.VisitId ? this.VisitId:this.patService.VisitId).subscribe(res => {
      this.charge = res;
    },
      error => {
        this.alertService.error(error);
      }
    );
  }

  //for client side integration of Khalti wallet
  PayThroughKhalti() {
    const config = {
      // replace the publicKey with yours
      publicKey: "test_public_key_9b5d31f8b09c4bfabda2d656cb20cd14",
      productIdentity: "1234567890",
      "productName": "Telemed",
      "productUrl": "https://danphetele.com",
      "eventHandler": {
        onSuccess(payload) {
          // hit merchant api for initiating verfication
          console.log(payload);
        },
        onError(error) {
          console.log(error);
        },
        onClose() {
          console.log('widget is closing');
        }
      }

    }
    
    this.paidstatus(); // uncomment this while testing in localhost !!
    const checkout = new KhaltiCheckout(config);
    checkout.show({ amount: 1000 });
  }

  PayThroughNabil() {
    this.IsCardPayButtonDisabled = true;
    this.SaveHospitalIdAndVisitIdInLocalStorage(this.hospitalId, this.VisitId);
    const formData = new FormData();
    formData.append("visitId", this.VisitId);
    fetch('/Payment/NabilCard', {
      method: 'POST',
      headers: new Headers({ 'Authorization': 'Bearer ' + sessionStorage.getItem("jwtToken") }),
      body: formData
    })
      .then(response => {
        if (response.status == 200) {
          return response.text();
        }
        else if (response.status == 400) {
          this.IsCardPayButtonDisabled = false;
          this.dialog.open(PaymentMessageComponent, { data: { message: "Payment through Card Failed.", method: "Card", status: "failure" } });
        }
      })
      .then(compassPlusURL => {
        // send the User to Nabil PG to enter Card Details and Credentials
        if (compassPlusURL != undefined) {
          window.location.href = compassPlusURL;
        }
      });
  }

  PayThroughCreditCard() {

  }
  PayThroughEsewa() {
    this.SaveHospitalIdAndVisitIdInLocalStorage(this.hospitalId, this.VisitId);
    var path = "https://esewa.com.np/epay/main";
    var params = {
      amt: parseInt(this.charge),
      psc: 0,
      pdc: 0,
      txAmt: 0,
      tAmt: parseInt(this.charge),
      pid: this.VisitId,
      scd: "NP-ES-IMARK",
      su: "https://danphetele.com/Payment/EsewaSuccess",
      fu: "https://danphetele.com/Payment/EsewaFailure"
    }


    let form = document.createElement("form");
    form.setAttribute("method", "POST");
    form.setAttribute("action", path);

    for (let key in params) {
      let hiddenField = document.createElement("input");
      hiddenField.setAttribute("type", "hidden");
      hiddenField.setAttribute("name", key);
      hiddenField.setAttribute("value", params[key]);
      form.appendChild(hiddenField);
    }

    document.body.appendChild(form);
    form.submit();
  }

  public MyBookList() {
    this.routing.navigate(['/PatBookList'], { queryParams: { id: this.hospitalId } });

  }

  public MyPaidBooking() {
    this.routing.navigate(['/PatPaidBookingList'], { queryParams: { id: this.hospitalId } });
  }

  public BookNewVisit() {
    this.routing.navigate(['/PatBookAppointment'], { queryParams: { id: this.hospitalId } });
  }

  GetHospitalIdAndVisitIdFromLocalStorage(): void {
    if (this.hospitalId == null) {
      this.hospitalId = localStorage.getItem("hospitalId");
      localStorage.removeItem("hospitalId");
    }
    if (this.VisitId == null) {
      this.VisitId = localStorage.getItem("visitId");
      localStorage.removeItem("visitId");
    }
  }


  SaveHospitalIdAndVisitIdInLocalStorage(hospitalId: string, visitId: string): void {
    // save hospitalId and visitId in local storage to retrieve when
    // angular app is reloaded after payment
    localStorage.setItem("visitId", visitId);
    localStorage.setItem("hospitalId", hospitalId);
  }

}
