import { Component } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router';
import { PatientService } from '../../services/patient/patient.service';
import { Patient } from '../../models/patient/patient.model';
import { AlertService } from '../../services/alert.service';
import { AuthenticationService } from '../../services/authentication.service';
import * as moment from 'moment/moment';
import { Problem } from '../../models/problem/problem.model';
import { HospitalService } from '../../services/hospital/hospital.service';
import { Global } from '../../app.global';
import { NotificationService } from 'src/app/services/notification.service';
import { SelectControlValueAccessor } from '@angular/forms';

@Component({
  templateUrl: './app.patient_history.html'
})
export class PatientHistoryComponent {
  public config: any;
  public patInfo: Patient = new Patient();
  // public patList:Array<Patient>= new Array<Patient>();
  patList: any;
  filterPatList:any;
  path: any;
  patientFile: any;
  filekind:any;
  fileExtension: any;
  Filetype: any;
  public viewList: any;
  public noteList: any;
  public token: any;
  public prob: boolean = false;
  public hospitalId: any;
  p: number = 1;
  collection = { count: 0, data: [] };
  fileList: any;
  visitId: string;
  public showViewNotes: boolean = false;
  public showPrescription: boolean = false;

  constructor(public routing: Router, private route: ActivatedRoute,
    public patservice: PatientService,
    public alertService: AlertService, public authenticationService: AuthenticationService,
    public hospitalService: HospitalService, public global: Global, private notifyService: NotificationService) {
    this.token = this.authenticationService.currentUserValue;
    this.AppointmentHistory();
    this.hospitalId = this.route.snapshot.queryParamMap.get('id');

  }
  pageChanged(event) {
    this.config.currentPage = event;

  }
  public AppointmentHistory() {
    this.patservice.getPatHistory(this.token.PatientIdentifier). subscribe(res => this.Success(res),
    res => this.Error(res)); }

    Success(res){
      this.patList = res;
      this.collection = { count: this.patList.length, data: [] }
      for (var i = 0; i < this.collection.count; i++) {
        this.collection.data.push(
          {
            id: i + 1,
            VisitId: this.patList[i].VisitId,
            VisitDate: this.patList[i].VisitDate,
            DoctorName: this.patList[i].DoctorName,
            HospitalName: this.patList[i].HospitalName,
            Status: this.patList[i].Status,
            NMC: this.patList[i].NMC
          }
        );
      }

    }
    Error(res){

    }
      
  
  public ViewNotes(visitId) {
    this.showViewNotes = true;
    this.patservice.getPatientHistory(this.token.PatientIdentifier).subscribe(res => {
      this.viewList = res;
      this.noteList = res;
      this.viewList = this.viewList.filter(s => s.VisitId == visitId);
      for (var i = 0; i < this.viewList.length; i++) {

        this.viewList[i].VisitDate = moment(this.viewList[i].VisitDate).format('lll');
      }
      for (let index = 0; index < this.viewList.length; index++) {
        const element = this.viewList[index];
        if (element == true) {
          this.prob = false;
        }
      }

    },
      error => {
        this.alertService.error(error);
      });
  }
  ViewPrescription(id) {
    this.visitId = id;
    this.filterPatList = new Array<any>();
    this.filterPatList = JSON.parse(JSON.stringify(this.patList));//deepcopy
    this.filterPatList = this.filterPatList.filter(x => x.VisitId == this.visitId);
    this.patientFile = this.filterPatList[0].PatientFile;
    if(this.patientFile.length > 0){
      this.showPrescription = true ;
      this.path = this.patientFile[0].FilePath;
      this.fileExtension = this.patientFile[0].FileExtention;
      if (this.fileExtension == 'jpeg' || this.fileExtension== 'JPEG' || this.fileExtension == 'png' || this.fileExtension == 'PNG' || this.fileExtension == 'JPG' || this.fileExtension == 'jpg') {
        this.Filetype = "image";
      }
      if (this.fileExtension == 'pdf' || this.fileExtension == 'PDF') {
        this.Filetype = "pdf";
      }
      if (this.fileExtension == 'docx' || this.fileExtension== 'DOCX' || this.fileExtension == 'DOC' || this.fileExtension == 'doc') {
        this.Filetype = "docfile";
      }

    }
    else{
      this.showPrescription = false ;
      this.notifyService.showError("Error!"," Could not found the Prescription file");
    }
  }

  public Close() {
    this.showViewNotes = false;
  }
  public PaymentHistory() {

  }
  Print() {
    let popupWinindow;
    var printContents = document.getElementById("Report").innerHTML;
    popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
    popupWinindow.document.open();
    let documentContent = '<html><head>';
    documentContent += '<link rel="stylesheet" type="text/css" />';
    documentContent += '</head>';
    documentContent += '<body onload="window.print()" style="margin:8px 0px 0px 50px !important;">' + printContents + '</body></html>'
    popupWinindow.document.write(documentContent);
    popupWinindow.document.close();
  }
  public BackDashBoard() {
    //this.updatePat = false;
    this.routing.navigate(['/PatDashboard'], { queryParams: { id: this.hospitalId } });
  }
}

