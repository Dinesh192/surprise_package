import { Component } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { PatientService } from '../../services/patient/patient.service';
import * as moment from 'moment/moment';
import { AlertService } from '../../services/alert.service';
import { HospitalService } from '../../services/hospital/hospital.service';
@Component({
  templateUrl: './app.mybooklist.html'
})
export class MyBooKListComponent {
  public token: any;
  config: any;
  public patBookList: any;
  public patbook: boolean;
  public hospitalId: any;
  collection = { count: 60, data: [] };
  p: number = 1;
  constructor(public routing: Router, private route: ActivatedRoute,
public authService: AuthenticationService,
    public patService: PatientService, public alertService: AlertService,
    public hospitalService: HospitalService

  ) {
    this.token = this.authService.currentUserValue;
    this.ShowBookList();
    this.hospitalId = this.route.snapshot.queryParamMap.get('id');
  }
  public ShowBookList() {
    this.patbook = true;
    this.patService.getPatientBookingList(this.token.PatientIdentifier).subscribe(res => {
      this.patBookList = res;

      for (var i = 0; i < this.patBookList.length; i++) {
        this.patBookList[i].VisitDate = moment(this.patBookList[i].VisitDate).format('ll');
        this.patBookList[i].VisitStartTime = moment(this.patBookList[i].VisitStartTime).format('LT');
      }
      this.collection = {count: this.patBookList.length,data:[]}
      
      for (var i = 0; i < this.collection.count; i++) {
          this.collection.data.push(
           {
              id: i + 1,
              VisitDate: this.patBookList[i].VisitDate,
              HospitalName:this.patBookList[i].HospitalName,
              DoctorName:this.patBookList[i].DoctorName,
              DepartmentName:this.patBookList[i].DepartmentName,
              VisitType:this.patBookList[i].VisitType,
              PaymentStatus:this.patBookList[i].PaymentStatus,
              VisitId: this.patBookList[i].VisitId,
            }
          );
        }


    },
      error => {
        this.alertService.error(error);
      });

  }
 
  pageChanged(event) {
    this.config.currentPage = event;
  }
  

  public paymentselection(data) {
    this.routing.navigate(['/PaymentSelection'], { queryParams: { vid: data.VisitId, charge: data.Charge, id: this.hospitalId } });

  }
  public BookNewVisit() {
    this.routing.navigate(['/PatBookAppointment'], { queryParams: { id: this.hospitalId } });
  }

  public MyBookList() {
    this.routing.navigate(['/PatBookList'], { queryParams: { id: this.hospitalId } });

  }

  public MyPaidBooking() {
    this.routing.navigate(['/PatPaidBookingList'], { queryParams: { id: this.hospitalId } });
  }

  public Payment() {
    this.routing.navigate(['/PatDashboard/PaymentSelection']);
  }
}
