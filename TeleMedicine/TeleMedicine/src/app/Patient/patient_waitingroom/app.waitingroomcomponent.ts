import { Component, OnDestroy, OnInit } from '@angular/core';
import { timer } from 'rxjs';
import { map, takeWhile } from 'rxjs/operators';
import { AuthenticationService } from '../../services/authentication.service';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Global } from '../../app.global';
import { PatientService } from '../../services/patient/patient.service';
import { Visit } from '../../models/visit/visit.model';
import { NotificationService } from '../../services/notification.service';
import { AlertService } from '../../services/alert.service';
import { DoctorService } from '../../services/doctor/doctor.service';
import { PatientDoc, Patient } from '../../models/patient/patient.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ChatService } from 'src/app/services/ChatNotification/chat-notification.service';
@Component({
  //selector: 'app-root',
  templateUrl: './app.waitingroom.html'
})
export class WaitingRoom implements OnInit {
  public isCollapsed = false;
  timer1: any = timer(3000, 2000);
  public patientName: string;
  token: any;
  status: any;
  VisitId: any;
  HospitalId: any;
  public doctorId: string;
  public receiverId: string;
  public index: number;
  visitObj: Visit = new Visit();
  patients: Array<PatientDoc> = new Array<PatientDoc>();
  public patInfo: Patient = new Patient();
  patlist: any = null;
  loginVal: any;
  ChatForm:FormGroup;
  ChatMessages: Array<any> = new Array<any>();
  ChatReceivedMessages: Array<any> = new Array<any>();
  AllUserChats:any;
  docName:any;
  FirstName:any;
  public doctor: Array<any> = new Array<any>();
  apiStatusDestroy = false;
  apiPatDocDestroy = false;
  redUrl: string;
  constructor(public httpClient: HttpClient, private alertService: AlertService,
    private notifyService: NotificationService, patservice: PatientService, private route: ActivatedRoute, public docService: DoctorService,
    public routing: Router, private authenticationService: AuthenticationService,public formBuilder: FormBuilder,
    public global: Global, public patService: PatientService,public notificationService : ChatService) {
    this.token = this.authenticationService.currentUserValue;
    this.getPatientName();
    this.global.apiLogout = false;
    this.VisitId = this.route.snapshot.queryParamMap.get('vid');
    this.HospitalId = this.route.snapshot.queryParamMap.get('id');
    this.redUrl = this.route.snapshot.queryParamMap.get('redirectUrl');
    //this.doctorId = this.docService.DoctorId;
    this.doctorId = this.route.snapshot.queryParamMap.get('did')
    this.initForm();
    this.notificationService.Connect();
    this.notificationService.EventChatMessage.subscribe(chatData => {
      //if (this.ChatForm.controls['Id'].value != chatData.Id) {
      //  this.ChatForm.controls['Id'].setValue(chatData.Id);
      //  console.log(chatData);
        this.OnChatUserChange();
      //}
    
        const chatMsg = { Name: chatData.Name, Message: chatData.Message, Class: 'receiver-msg' };
        this.ChatMessages.push(chatMsg);
        this.pushChatMsgUserwise(chatData.Name, chatMsg);
     

      //this.cdr.detectChanges();
      //this.scrollBottom.nativeElement.lastElementChild.scrollIntoView(); // scroll to bottom
    });
  
    //this.GetStatusByPatientId();
    this.GetEntrytoDoctorRoom();
    this.GetPatientDoctorList();
    this.GetDoc();

  }
  ngOnInit() {

      this.GetStatusByPatientId();

    
  }
    
  private initForm() {
    this.ChatForm = this.formBuilder.group({
      chatMessage: ['', Validators.required]
    });
  }

  hasError(typeofvalidator: string, controlname: string): boolean {
    const control = this.ChatForm.controls[controlname];
    return control.hasError(typeofvalidator) && control.dirty;
  }

  GetEntrytoDoctorRoom() {
 
    this.timer1.pipe(
      takeWhile(x => this.apiStatusDestroy === false)
    ).subscribe(
      () => {
      //this.timer1.subscribe(
      //  () => {
        //if (this.VisitId == null) {
        //  this.patService.getStatusByPatientId(this.token.PatientIdentifier).subscribe(res =>
        //    //this.Success(res),
        //    this.SuccessStatusVisitId(res),
        //    res => this.Error(res));
        //}
        if (this.redUrl === "PatientUrl") {
          this.patService.getStatusByPatientUrl(this.VisitId).subscribe(res =>
            this.SuccessStatusVisitId(res),
            res => this.Error(res));
        }
        else if (this.redUrl === "DoctorUrl") {
          this.patService.getStatusByPatientUrl(this.VisitId).subscribe(res =>
            this.SuccessStatusVisitId(res),
            res => this.Error(res));
        }

        else {
          const loginP = localStorage.getItem('loginPat');
          if (loginP === "patient")
            this.patService.getPatientByVisitId(this.VisitId).subscribe(res =>
              this.SuccessStatusVisitId(res),
              res => this.Error(res));
          }      
        }
      );
    
    
  }
 
  SuccessStatusVisitId(res) {
    this.status = res;
    if (this.status.Status == "ongoing" && this.status.IsActive == true) {
      this.patService.VisitId = this.status.VisitId;
      this.apiStatusDestroy = true;
      this.routing.navigate(['/DoctorRoom'], { queryParams: { id: this.VisitId,did:this.doctorId } });

    }
    else if (this.status.Status == "initiated" && this.status.IsActive == true) {
      this.apiStatusDestroy = false;

    }
    else if (this.status.Status === "completed" || (this.status.Status === "initiated" && this.status.IsActive == false)) {
      //this.routing.navigate(['/PatDashboard']);
      this.apiStatusDestroy = true;
    }
    else {
      this.apiStatusDestroy = true;
    }
  }

  GetStatusByPatientId() {
    this.patService.getStatusByPatientId(this.VisitId).subscribe(res =>
      this.SuccessResponse(res),
      res => this.Error(res));
  }
  SuccessResponse(res) {
    this.status = res;
    this.patientName = this.status.PatientName;
    if (this.VisitId == null) {
      var visitid = this.status.VisitId;
      this.patService.VisitId = this.status.VisitId;
      this.patService.getPatientByVisitId(visitid)
        .subscribe(res => this.SuccessPatientDetail(res, visitid),
          res => this.Error(res));
    }
    else {
      this.patService.getPatientByVisitId(this.VisitId)
        .subscribe(res => this.SuccessPatientDetail(res, this.VisitId),
          res => this.Error(res));
    }

  }
  SuccessPatientDetail(res, visitid) {
    this.visitObj = res;
    this.visitObj.IsActive = true;
    this.visitObj.Status = "initiated";
    this.patService.updateStatus(this.visitObj, visitid)
      .subscribe(res => this.notifyService.showInfo("Please wait, Your Doctor will call you shortly ", "Welcome to Waiting Room"));
  }

  GetPatientDoctorList() {
    this.timer1.pipe(
      takeWhile(x => this.apiPatDocDestroy === false)
    ).subscribe(
      () => {
      //this.timer1.subscribe(
      //  () => {
          this.loginVal = this.authenticationService.loginValue;
          if (this.loginVal === "patient") {
            this.patService.getPatientDocList(this.doctorId)
              .subscribe(res => this.SuccessPatientList(res),
                res => this.Error(res))
          };
        });
    }


  SuccessPatientList(res) {
    this.patients = res;
    this.index = this.patients.findIndex(x => x.VisitId === this.VisitId);
    this.apiPatDocDestroy = true;
   // console.log(this.index);
  }

  Error(res) {
    console.log(res);
  }
  GetDoc(){
    this.docService.getDoctorName(this.doctorId).subscribe(res => this.Success(res),
    res => this.Error(res));
  }
  Success(res){
    this.doctor = res;
    this.receiverId = res.IdentityUserId;
    this.docName = `${ res.FirstName } ${res.MiddleName ?res.MiddleName :""} ${res.LastName}`;

  }
      
  public getPatientName() {
    //var id = this.token.PatientIdentifier;
    this.patService.getPatient(this.token.PatientIdentifier).subscribe(res => {
      this.patInfo = res;
    },
      error => {
        this.alertService.error(error);
      });
  }
  SendChatMsg() {
    try {
      for (const i in this.ChatForm.controls) {
        this.ChatForm.controls[i].markAsDirty();
        this.ChatForm.controls[i].updateValueAndValidity();
      }
  
      if (this.ChatForm.valid) {
        const chatMsg = {
          SenderId: this.patInfo.IdentityUserId,
          ReceiverId: this.receiverId,
          Name: `${this.patInfo.FirstName} ${this.patInfo.MiddleName ? this.patInfo.MiddleName : ""} ${this.patInfo.LastName}`,
          Message: this.ChatForm.controls['chatMessage'].value
        };
        const chatmsgObj = { Name: 'Me', Message: chatMsg.Message, Class: 'sender-msg' };
        this.ChatMessages.push(chatmsgObj);
      //  this.pushChatMsgUserwise(this.ChatForm.controls['Id'].value, chatmsgObj);
  
       // this.cdr.detectChanges();
       // this.scrollBottom.nativeElement.lastElementChild.scrollIntoView(); // scroll to bottom
  
        this.notificationService.SendChatMessage(chatMsg);
        this.ChatForm.reset();
        //this.ChatForm.controls['Id'].setValue(chatMsg.ReceiverId);
      }
    } catch (e) { }
  }


  OnChatUserChange() {
    try {
      const selUser = this.ChatForm.controls['Id'].value;
      if (this.AllUserChats.hasOwnProperty(selUser)) {
        this.ChatMessages = this.AllUserChats[selUser].slice();
        this.ChatReceivedMessages=this.AllUserChats[selUser].slice();
      } else {
        this.ChatMessages = new Array<any>();
        this.ChatReceivedMessages=new Array<any>();
      }
    } catch (e) { }
  }
  
  onChatEnter(event) {
    if (event.keyCode === 13) {
      this.SendChatMsg();
    }
  }
  pushChatMsgUserwise(user, messageObj) {
    try {
      if (!this.AllUserChats.hasOwnProperty(user)) {
        this.AllUserChats[user] = new Array<any>();
      }
      this.AllUserChats[user].push(messageObj);
    } catch (e) { }
  }


}
