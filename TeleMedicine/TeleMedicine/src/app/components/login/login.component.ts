import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { first } from "rxjs/operators";
import { AuthenticationService } from "../../services/authentication.service";
import { NotificationService } from "../../services/notification.service";
import { HttpClient } from "@angular/common/http";
import { User } from "../../models/user/user.model";
import * as jwt_decode from "jwt-decode";
import { BehaviorSubject, Observable } from "rxjs";
import { catchError } from "rxjs/operators";
import { map } from "rxjs/operators";

@Component({
  templateUrl: "login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  isPatient = false;
  jwtToken: any;
  loginVal: any;
  userType: string;
  showPassword = false;
  constructor(private http: HttpClient,
    private formBuilder: FormBuilder, private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private notifyService: NotificationService,
    public routing: Router
  ) {}
  public get currentUserValue(): User {
    let token = sessionStorage.getItem("jwtToken");
    //if (token != null || token != undefined) {
    var decoded = jwt_decode(token);
    return (this.jwtToken = decoded);
    //}
    //else {
    //  return "NoToken";
    //}

    //let token = this.currentUserSubject.value.token;
    //let token = this.currentUserSubject.value.token;
    //var decoded = jwt_decode(token);
  }
  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      phonenumber: [
        "",
        [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")],
      ],
      //password: ['', [Validators.required, Validators.minLength(6), Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{6,}')]]
      password: ["", [Validators.required, Validators.minLength(6)]],
    });

    // get return url from route parameters or default to '/'
    //this.returnUrl = this.route.snapshot.queryParams["returnUrl"] || "/";
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.loginForm.controls;
  }
  patientregister() {
    this.routing.navigate(["/register"], {
      queryParams: { id: "patientregister", login: "patient" },
    });
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    this.loading = true;
    this.login(this.f.phonenumber.value, this.f.password.value).subscribe(
      data => {
        if (data === "Admin") {
          this.router.navigate(['/admin']);
          this.notifyService.showInfo("Info", "Admin Page");
          
        }
        else if (data === "Patient") {
          this.router.navigate(['/PatDashboard']);
          this.notifyService.showSuccess("Welcome", "login successful");
        }
        else if (data === "Doctor") {
          this.router.navigate(['/DocDashboard']);
          this.notifyService.showSuccess("Welcome", "login successful");
        }
        else {
          this.router.navigate(['']);
        }
          //this.notifyService.showSuccess("Welcome", "login successful");
        },
        error => {
          this.notifyService.showError("Sorry", "Phone Number and Password do not match")
          this.loading = false;
        });

  }
  login(phonenumber: string, password: string) {
    return  this.http.post<any>(`/api/account/login`, { phonenumber, password })
      .pipe(map(user => {
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          sessionStorage.removeItem('jwtToken');
          sessionStorage.setItem('jwtToken', user.token);
          const token = user.token;
          const decoded = jwt_decode(token);
          this.jwtToken = decoded;
          this.userType = decoded.UserType;
          if (this.userType == "Patient") {
            localStorage.removeItem('loginPat');
            localStorage.setItem('loginPat', "patient");
          }
          else if (this.userType == "Doctor") {
            localStorage.removeItem('loginDoc');
            localStorage.setItem('loginDoc', "doctor");
          }
        }
        return this.userType;
      }));
  }
  // Slick Slider this is added new

  slides = [
    { img: "./assets/img/Slider11.jpg" },
    { img: "./assets/img/Slider22.jpg" },
  ];
  slideConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    fade: true,
    cssEase: "linear",
    draggable: false,
    autoplaySpeed: 3000,
    arrows: false,
    dots: false,
  };
  // End SLick slider
  showPass() {
    this.showPassword = !this.showPassword;
  }
}
