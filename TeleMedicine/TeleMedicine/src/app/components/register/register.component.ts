import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AuthenticationService } from '../../services/authentication.service';
import { AlertService } from '../../services/alert.service';
import { DoctorService } from '../../services/doctor/doctor.service';
import { NotificationService } from '../../services/notification.service';
declare var $: any;
@Component({
   selector: 'register',
   templateUrl: 'register.component.html', styleUrls: ['./register.component.css'] 
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  loading = false;
  public today = new Date();
  submitted = false;
  isPatientRegister = false;
  public token: any;
  typeregister: string = "";
  loginpatient: string = "";
  showPassword = false;
  hospitalIdentifier: string;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService,
    private userService: DoctorService,
    private alertService: AlertService, private notifyService: NotificationService, private route: ActivatedRoute
  ) {
    this.typeregister = this.route.snapshot.queryParamMap.get('id');
    this.loginpatient = this.route.snapshot.queryParamMap.get('login');
    this.hospitalIdentifier = this.route.snapshot.queryParamMap.get('hospital');
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      phonenumber: ['', [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
      firstname:['', [Validators.required]],
      lastname:['', [Validators.required]],
      middlename:[''],
      dateofbirth: [''],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmpassword: ['', [Validators.required, Validators.minLength(6)]],
      hospitalIdentifier: this.hospitalIdentifier != null ? this.hospitalIdentifier: "NA"
      //password: ['', [Validators.required, Validators.minLength(6), Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{6,}')]],
      //confirmpassword: ['', [Validators.required, Validators.minLength(6), Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{6,}')]]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;
    //stop here if form is invalid
    // if (this.registerForm.invalid) {
    //  return;
    // }

    this.loading = true;

    if (this.registerForm.value.phonenumber.invalid) {
      this.notifyService.showError("", "Phone number should be in digits");
      this.loading = false;
    }
    if (this.registerForm.value.firstname.invalid) {
      this.notifyService.showError("", "First Name is required");
      this.loading = false;
    }
    if (this.registerForm.value.lastname.invalid) {
      this.notifyService.showError("", "Last Name is required");
      this.loading = false;
    }
    if (this.registerForm.value.password != this.registerForm.value.confirmpassword) {
      this.notifyService.showError("", "Password and Confirm  Password do not match");
      this.loading = false;
    }
    if (this.registerForm.value.password.invalid) {
      this.notifyService.showError("", "Password must be at least 6 characters");
      this.loading = false;
    }
    if (this.typeregister == "docregister" && this.registerForm.value.password == this.registerForm.value.confirmpassword) {
      this.userService.registerDoctor(this.registerForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.alertService.success('Registration successful', true);
            this.router.navigate(['/doclist']);
          },
          error => {
            this.alertService.error(error);
            this.notifyService.showError("", "Phone Number is already taken")
            this.loading = false;
          });
    }
   
    if (this.typeregister == "patientregister" && this.registerForm.value.password == this.registerForm.value.confirmpassword) {
     
      this.userService.registerPatient(this.registerForm.value)
        .pipe(first())
        .subscribe(
          data => {

            this.alertService.success('Registration successful', true);
            if (this.loginpatient === "patient") {
              this.router.navigate(['/login']);
            }
            else {
              this.router.navigate(['/adminpatlist']);
            }
           
          },
          error => {
            this.alertService.error(error);
            this.notifyService.showError("", "Phone Number is already taken")
            this.loading = false;
          });
    }
  }
  showPass() {
    this.showPassword = !this.showPassword;
  }
  BackLogin(){
    this.router.navigate(['/login']);
  }
  BackAdmin(){
    this.router.navigate(['/admin']);
  }
 BackToDoctor(){
   this.router.navigate(['/doclist']);
  }
 BackToPatient(){
   this.router.navigate(['/adminpatlist']);
  }
}
