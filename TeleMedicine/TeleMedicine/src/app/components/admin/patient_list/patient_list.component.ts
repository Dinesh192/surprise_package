import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Global } from '../../../app.global';
import { Router } from '@angular/router';
import { ToastrModule } from 'ngx-toastr';
import { AuthenticationService } from '../../../services/authentication.service';
import { PatientService } from '../../../services/patient/patient.service';
import { AlertService } from '../../../services/alert.service';
import { DoctorService } from '../../../services/doctor/doctor.service';
import * as moment from 'moment';
import { NotificationService } from 'src/app/services/notification.service';
import { MatDialog } from '@angular/material';
import { Patient } from '../../../models/patient/patient.model';
import { PatientUpdateProfileComponent } from '../../../Patient/patient_updateprofile/app.patient_updateprofile';
import { PasswordComponent } from '../../common/password/password.component';
import { Utilities } from '../../../services/utilities';

@Component({
  templateUrl: './patient_list.component.html'
})
export class AdminPatientListComponent implements AfterViewInit {
  config: any;
  token: any;
  public searchText: any;
  patientList: Array<any> = new Array<any>();
  filteredPatientList: any;
  public editModal: boolean;
  collection = { count: 60, data: [] };
  p: number = 1;
  @ViewChild('patientEditor', { static: true })
  patientEditor: PatientUpdateProfileComponent;
  editedPatient: Patient;
  @ViewChild('pwdEditor', { static: true })
  pwdEditor: PasswordComponent;
  editedPwdPatient: Patient;
  isPatEditor = false;
  hospital: string;
  patientName: string;
  public pwdModal: boolean;
  constructor(public routing: Router, public patService: PatientService, public authService: AuthenticationService, public alertService: AlertService,
    public global: Global, public notifyService: NotificationService, public dialog: MatDialog, ) {
    this.token = this.authService.currentUserValue;
    this.showPatientList();
    this.editModal = false;
    this.pwdModal = false;
    this.hospital = this.token.HospitalIdentifier;
  }
  pageChanged(event) {
    this.config.currentPage = event;
  }
  ngAfterViewInit() {

    this.patientEditor.changesSavedCallback = () => {
      this.editModal = false;
      this.showPatientList();
    };
    this.patientEditor.changesCancelledCallback = () => {
      this.editModal = false;
    };
    this.pwdEditor.changesSavedCallback = () => {
      this.pwdModal = false;
      this.showPatientList();
    };
    this.pwdEditor.changesCancelledCallback = () => {
      this.pwdModal = false;
    };

  }

  public showPatientList() {
    this.patService.getAllPatient().subscribe(res => {
      this.patientList = Object.assign(this.patientList, res);
    },


      error => {
        this.alertService.error(error);
      });

  }

  UpdateProfile(row: Patient) {
    this.isPatEditor = true;
    this.editedPatient = this.patientEditor.editAdminPatient(row, this.isPatEditor, this.hospital );
      this.editModal = true;
  }
  ResetPassword(row: Patient) {
    this.patientName = `${row.FirstName + " " + row.MiddleName + " " + row.LastName}`;
    this.editedPwdPatient = this.pwdEditor.resetPwd(row, this.patientName);
    this.pwdModal = true;
  }
  hide() {
    this.editModal = false;
  }
  RegPatient() {
    this.routing.navigate(['register'], { queryParams: { id: "patientregister", hospital: this.hospital } });
  }
  BookAppointment(data) {
    this.patService.PatientId = data.PatientId;
    this.routing.navigate(['/HospitalList']) ,{ queryParams: { routefrom : "admin" } };
  }
  
}
