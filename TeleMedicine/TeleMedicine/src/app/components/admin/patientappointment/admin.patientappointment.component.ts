import { Component } from '@angular/core';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { DoctorService } from '../../../services/doctor/doctor.service';
import { AuthenticationService } from '../../../services/authentication.service';
import { PatientService } from '../../../services/patient/patient.service';
import { AlertService } from '../../../services/alert.service';
import { NotificationService } from '../../../services/notification.service';
import { HttpClient } from '@angular/common/http';
import { Global } from '../../../app.global';
import { RescheduleComponent } from '../../../Doctor/doctor_reschedule/app.doctor_reschedule';
import { MatDialog } from '@angular/material';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import { Visit } from '../../../models/visit/visit.model';
import { PopUpDialogComponent } from '../../common/popup/pop-up.component';
export interface DialogData {
  cancel: string;
}
@Component({
  //selector: 'admin-root',
  templateUrl: './admin.patientappointment.html'
})
export class AdminPanelPatientAppointment {
  config: any;
  token: any;
  public todate = moment().format('YYYY-MM-DD');
  public fromdate = moment().format('YYYY-MM-DD');
  public filterdate = moment().format('YYYY-MM-DD');
  patientList: any;
  patientDanpheCareList: any;
  filteredDailyVisitList: any;
  filteredpatientList: any;
  public showpatientList: boolean;
  public showcancelledAppointmentpatientList: boolean;
  public showView: boolean = false;
  visitObj: Visit = new Visit();
  collection = { count: 60, data: [] };
  progress: number;
  message: string;
  public visitId: string;
  p: number = 1;
  selectedValue = "";
  isCancelled: boolean;
  IsDanpheCare = false;
  PaymentStatus = [
    { "status": "paid" },
    { "status": "free" },
    { "status": "unpaid" },
  ];
  PaymentStatusDanpheCare = [
    { "status": "paid" },
    { "status": "free" },
    { "status": "unpaid" },
    { "status": "credit" },
  ];

  constructor(public routing: Router, public docService: DoctorService, public authService: AuthenticationService, public patService: PatientService, public alertService: AlertService,
    public global: Global, public notify: NotificationService, public http: HttpClient, public dialog: MatDialog) {
    this.token = this.authService.currentUserValue;
    this.ShowPatientList();
    this.ShowData();

  }
  pageChanged(event) {
    this.config.currentPage = event;
  }
  public ShowData() {
    for (var i = 0; i < this.collection.count; i++) {
      this.collection.data.push(
        {
          id: i + 1,
          value: "items number " + (i + 1)
        }
      );
    }

    this.config = {
      itemsPerPage: 5,
      currentPage: 1,
      totalItems: this.collection.count
    };
  }

  public ShowPatientList() {
    this.showcancelledAppointmentpatientList = false;
    this.showpatientList = true;
    //var id = this.token.PatientIdentifier;
    this.docService.getPatientListByAdmin().subscribe(res => {
      this.patientList = res;
      this.patientDanpheCareList = res[0].HospitalName;
      if (this.patientDanpheCareList === "DanpheTeleHealth") {
        this.IsDanpheCare = true;
      }
      else {
        this.IsDanpheCare = false;
      }
      for (var i = 0; i < this.patientList.length; i++) {
        this.patientList[i].VisitDate = moment(this.patientList[i].VisitDate).format("YYYY-MM-DD");
        // this.patBookList[i].VisitStartTime = moment(this.patBookList[i].VisitStartTime).format('LT');
      }
      this.collection = { count: this.patientList.length, data: [] }
      for (var i = 0; i < this.collection.count; i++) {
        this.collection.data.push(
          {
            id: i + 1,
            VisitDate: this.patientList[i].VisitDate,
            PatientName: this.patientList[i].PatientName,
            ContactNumber: this.patientList[i].ContactNumber,
            DoctorName: this.patientList[i].DoctorName,
            Age: moment().diff(this.patientList[i].DateOfBirth, 'years'),
            PaymentStatus: this.patientList[i].PaymentStatus,
            BookingTime: this.patientList[i].BookingTime,
            HospitalId: this.patientList[i].HospitalId,
            DepartmentId: this.patientList[i].DepartmentId,
            DoctorId: this.patientList[i].DoctorId,
            VisitId: this.patientList[i].VisitId,
            SchedulingId: this.patientList[i].SchedulingId,
            VisitStatus: this.patientList[i].Status,
          }
        );
      }

      this.FilterbyDate();
    },
      error => {
        this.alertService.error(error);
        // this.loading = false;
      });

  }
  //ChangeFromDate() {
  //  // moment().format("DD/MM/YYYY");
  //  this.filteredDailyVisitList = new Array<any>();
  //  this.filteredDailyVisitList = JSON.parse(JSON.stringify(this.collection.data));//deepcopy
  //  this.filteredDailyVisitList = this.filteredDailyVisitList.filter(a => a.VisitDate == this.filterdate);
  //  //console.log(this.filterdate);
  //}
  //ChangeToDate() {
  //  // moment().format("DD/MM/YYYY");
  //  this.filteredDailyVisitList = new Array<any>();
  //  this.filteredDailyVisitList = JSON.parse(JSON.stringify(this.collection.data));//deepcopy
  //  this.filteredDailyVisitList = this.filteredDailyVisitList.filter(a => a.VisitDate == this.filterdate);
  //  //console.log(this.filterdate);
  //}
  FilterbyDate() {
    this.filteredpatientList = new Array<any>();
    this.filteredpatientList = JSON.parse(JSON.stringify(this.collection.data));
    this.Search();
  }

  Search() {
    var todatemoment = moment(this.todate, 'YYYY-MM-DD');
    var fromdatemoment = moment(this.fromdate, 'YYYY-MM-DD');
    var diff = moment(todatemoment).diff(fromdatemoment, 'days');
    if (diff >= 0) {
      this.filteredpatientList = new Array<any>();
      this.filteredpatientList = JSON.parse(JSON.stringify(this.collection.data));
      for (var i = 0; i < this.filteredpatientList.length; i++) {
        this.filteredpatientList[i].VisitDate = moment(this.filteredpatientList[i].VisitDate, 'YYYY-MM-DD');
        // this.patBookList[i].VisitStartTime = moment(this.patBookList[i].VisitStartTime).format('LT');
      }
      this.filteredpatientList = this.filteredpatientList.filter(a => a.VisitDate <= todatemoment && a.VisitDate >= fromdatemoment);

    } else {
      this.notify.showWarning("Sorry", "Please check date interval  !!");
    }
  }

  Error(res) {

  }

  ChangeStatus(data) {
    this.selectedValue = data.PaymentStatus;
    this.visitId = data.VisitId;
    this.showView = true;

  }
  paymentstatusChanged() {
    this.visitObj.PaymentStatus = this.selectedValue;
  }
  hide() {
    this.showView = false;
  }

  Reschedule(data) {
    const dialogRef = this.dialog.open(RescheduleComponent,
      { data: { docId: data.DoctorId, deptId: data.DepartmentId, hosId: data.HospitalId, visitId: data.VisitId, schedullingId: data.SchedulingId, bookingTime: data.BookingTime }, width: '1000px', height: '1000px' }
    );
    dialogRef.disableClose = true;
    dialogRef.afterClosed().subscribe(result => {
      dialogRef.close();
      console.log(`Dialog result: ${result}`);
      this.routing.navigate(['/PatientAppointmentList']);
      this.ShowPatientList();
    });
  }

  UpdateStatus() {
    this.patService.updatePaymentStatus(this.visitObj, this.visitId).subscribe(res => {
      this.showView = false;
      this.SuccessUpdate();
    },
      error => {
        this.alertService.error(error);
        // this.loading = false;
      });
  }
  SuccessUpdate() {
    this.notify.showSuccess('Updated successfully', 'Success');
    this.ShowPatientList();
  }

  public Admin() {
    this.routing.navigate(['/admin']);
  }
  CancelBooking(data) {
    if (data.PaymentStatus != "paid") {
      this.visitId = data.VisitId;
      this.visitObj.SchedulingId = data.SchedulingId;
      this.visitObj.BookingTime = data.BookingTime;
      this.patService.cancelBooking(this.visitObj, this.visitId).subscribe(res => {
        this.notify.showSuccess('Booking cancelled !!', 'Success');
        this.ShowPatientList();
      },
        error => {
          this.alertService.error(error);
          // this.loading = false;
        });

    } else {
      this.notify.showError("Paid Booking cannot be cancelled ", "Sorry");
    }
  }

  openDialog(data) {
    const dialogRef = this.dialog.open(PopUpDialogComponent, {
      data: { msg: "fromAdmin" },
      width: '250px',
      height: '300',
    });

    dialogRef.afterClosed().subscribe(result => {
      this.isCancelled = result;
      if (this.isCancelled === true) {
        this.CancelBooking(data);
      }
    });

  }
  ShowCancelledAppointment() {
    this.showpatientList = false;
    this.showcancelledAppointmentpatientList = true;
    //var id = this.token.PatientIdentifier;
    this.docService.getCancelledAppointmentPatientByAdmin().subscribe(res => {
      this.patientList = res;

      for (var i = 0; i < this.patientList.length; i++) {
        this.patientList[i].VisitDate = moment(this.patientList[i].VisitDate).format("YYYY-MM-DD");
        // this.patBookList[i].VisitStartTime = moment(this.patBookList[i].VisitStartTime).format('LT');
      }
      this.collection = { count: this.patientList.length, data: [] }
      for (var i = 0; i < this.collection.count; i++) {
        this.collection.data.push(
          {
            id: i + 1,
            VisitDate: this.patientList[i].VisitDate,
            PatientName: this.patientList[i].PatientName,
            ContactNumber: this.patientList[i].ContactNumber,
            DoctorName: this.patientList[i].DoctorName,
            Age: moment().diff(this.patientList[i].DateOfBirth, 'years'),
            PaymentStatus: this.patientList[i].PaymentStatus,
            BookingTime: this.patientList[i].BookingTime,
            HospitalId: this.patientList[i].HospitalId,
            DepartmentId: this.patientList[i].DepartmentId,
            DoctorId: this.patientList[i].DoctorId,
            VisitId: this.patientList[i].VisitId,
            SchedulingId: this.patientList[i].SchedulingId,
            VisitStatus: this.patientList[i].Status,
          }
        );
      }

      this.FilterbyDate();
    },
      error => {
        this.alertService.error(error);
        // this.loading = false;
      });

  }

}
