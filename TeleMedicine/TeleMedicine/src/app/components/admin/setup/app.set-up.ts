import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SchedulingService } from 'src/app/services/scheduling/scheduling.service';
import { NotificationService } from 'src/app/services/notification.service';
import { HospitalService } from 'src/app/services/hospital/hospital.service';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import { Hospital, HospitalEdit } from 'src/app/models/hospital/hospital.model';
import { HttpRequest, HttpClient, HttpEventType } from '@angular/common/http';
//import { ModalDirective } from 'ngx-bootstrap/modal';
@Component({
  templateUrl: './app.set-up.html',
  styleUrls: ['./app.set-up.css']
})
export class SetUpComponent implements OnInit {
  public edit: boolean = false;
  public editModal: boolean = false;
  public hospException: boolean = false;
  public hospitalList: Array<any> = new Array<any>();
  public hospitalInfo: Hospital = new Hospital();
  public getHospitalInfo: HospitalEdit = new HospitalEdit();
  public hospitalData: any;
  public viewhospital: any;
  public progress: any;
  public message: any;
  public tempvalue: string = "";
  constructor(public routing: Router, public http: HttpClient, public hospitalService: HospitalService, public schedulingService: SchedulingService, public notifyService: NotificationService) {

  }
  ngOnInit() {
    this.getHospital();
  }

  public Admin() {
    this.routing.navigate(['/admin']);
  }
  getHospital() {
    this.hospitalService.getHospitalList().subscribe(res => this.SuccessHospitalList(res),
      res => this.Error(res));
  }
  SuccessHospitalList(res) {
    this.hospitalList = Object.assign(this.hospitalList, res)
  }
  Error(res) {
    this.notifyService.showError("Error", "Internal Error")
  }
  ErrorAdd(res) {
    this.notifyService.showError("Error", "Internal Error")
  }
  ErrorEdit(res) {
    this.notifyService.showError("Error", "Internal Error")
  }
  

  AddHospital(form: NgForm) {
   
    this.edit = false;
    this.hospException = false;
    if (form.invalid) {
      this.notifyService.showError("", "Please fill up the required field");
      return;
    }
    // this.hospitalInfo.ImagePath = this.getHospitalInfo.ImagePath;
    //this.getHospitalInfo = null ;
    if (this.hospitalList.length != 0) {
      for (var i = 0; i < this.hospitalList.length; i++ ) {
        if (this.hospitalInfo.HospitalCode == this.hospitalList[i].HospitalCode && this.hospitalInfo.HospitalName == this.hospitalList[i].HospitalName && this.hospitalInfo.Location == this.hospitalList[i].Location)
        { 
          this.hospException = true;
        }
      }
    }
    if (this.hospException == false) {
      this.hospitalService.addHospital(this.hospitalInfo).subscribe(res => this.Success(res),
        res => this.ErrorAdd(res));
    }
    else {
      var hospname = `${this.hospitalInfo.HospitalName}`;
      var location = `${this.hospitalInfo.Location}`;
      this.notifyService.showError("Sorry",  hospname + " at " + location + " is already exists !");
    }

  }
  Success(res) {
    //this.getHospitalInfo = null ;
    window.location.reload();
    this.notifyService.showSuccess('', 'Hospital Added Successfully!')

  }
  EditHospital(id) {
    this.edit = true;
    this.editModal = true ;
    this.hospitalService.getHospital(id).subscribe(res => this.SuccessHospital(res),
      res => this.ErrorEdit(res));
  }
  SuccessHospital(res) {
    this.getHospitalInfo = res ;
    // Object.assign(this.getHospitalInfo, res);
    this.tempvalue = this.getHospitalInfo.ImagePath;
    this.getHospitalInfo.ImagePath = null;
    //this.hospitalInfo = null;
  }
  UpdateHospital() {
    if (this.getHospitalInfo.ImagePath == null) {
      if (this.tempvalue != null) {
        this.tempvalue = this.tempvalue.slice(10);
      }
      this.getHospitalInfo.ImagePath = this.tempvalue;
    }
    // ngform: NgForm
    // if (ngform.invalid) {
    //   this.notifyService.showError("", "Please fill up the required field");
    //   return;
    // }
  
    this.hospitalService.updateHospital(this.getHospitalInfo).subscribe(res => this.SuccessUpdate(res),
      res => this.Error(res));
  }
  SuccessUpdate(res) {
    this.getHospital();
    this.notifyService.showSuccess('', 'Successfully Updated');
    window.location.reload();
  }

  DeleteHospital(id) {
    this.hospitalService.deleteHospital(id).subscribe(res => this.SuccessDelete(res),
      res => this.Error(res));
  }
  SuccessDelete(res) {
    this.getHospital();
    this.notifyService.showSuccess('', 'Successfully Deleted!');
    window.location.reload();
  }
  hide(){
    this.editModal = false;
  }

  upload(files) {

    if (files.length === 0)
      return;

    const formData = new FormData();

    for (let file of files)
      formData.append(file.name, file);


    const uploadReq = new HttpRequest('POST', `api/Patient/UploadFile/`, formData, {
      reportProgress: true,
    });

    this.http.request(uploadReq).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress)
        this.progress = Math.round(100 * event.loaded / event.total);
      else if (event.type === HttpEventType.Response)
        this.message = event.body.toString();
    });

  }
  uploadEdit(files) {

    if (files.length === 0)
      return;

    const formData = new FormData();

    for (let file of files)
      formData.append(file.name, file);


    const uploadReq = new HttpRequest('POST', `api/Patient/UploadFile/`, formData, {
      reportProgress: true,
    });

    this.http.request(uploadReq).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress)
        this.progress = Math.round(100 * event.loaded / event.total);
      else if (event.type === HttpEventType.Response)
        this.message = event.body.toString();
    });

  }
}
