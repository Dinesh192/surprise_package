import { Component } from '@angular/core';
import { Router } from '@angular/router';


import { AuthenticationService } from '../../services/authentication.service';
import { User } from '../../models/user/user.model';

@Component({ selector: 'footer', templateUrl: 'footer.component.html' })
export class FooterComponent {
  currentUser: User;
  public token: any;
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    this.token = this.authenticationService.currentUserValue;
  }

}
