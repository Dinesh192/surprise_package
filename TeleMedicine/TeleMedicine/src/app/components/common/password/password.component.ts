import { Component } from '@angular/core'
import { Patient } from '../../../models/patient/patient.model';
import { PatientService } from '../../../services/patient/patient.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication.service';
import { DatePipe } from '@angular/common';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { NotificationService } from '../../../services/notification.service';
import { AlertService } from '../../../services/alert.service';
import { Doctor } from '../../../models/doctor/doctor.model';
import { DoctorService } from '../../../services/doctor/doctor.service';

@Component({
  selector: 'password-update',
  templateUrl: './password.component.html'
})
export class PasswordComponent {
  public updatePat: boolean;
  public patInfo: Patient = new Patient();
  public docInfo: Doctor = new Doctor();
  isPatEditor = false;
  patId: any;
  patName: string;
  docId: any;
  docName: string;
  isDoctor = false;
  public hospitalIdentifier: string;
  showPassword = false;
  public changesSavedCallback: () => void;
  public changesCancelledCallback: () => void;
  constructor(public patservice: PatientService, public docservice: DoctorService, private route: ActivatedRoute, private alertService: AlertService,
    public routing: Router, private authenticationService: AuthenticationService, private datePipe: DatePipe,
    private formBuilder: FormBuilder, private notifyService: NotificationService) {
    }

  GetPatient() {

    this.patservice.getPatient(this.patId).subscribe(res => {
      this.patInfo = res;

    },
      error => {
        this.alertService.error(error);
      });
  }
  GetDoctor() {

    this.docservice.getDoctor(this.docId).subscribe(res => {
      this.docInfo = res;

    },
      error => {
        this.alertService.error(error);
      });
  }

  //UpdatePassword(row:Patient) {

  //  debugger;
  //  this.patservice.updatePatient(row).subscribe(data => {
  //    this.alertService.success('Password Reset successful', true);
  //    this.updatePat = false;
  //    if (this.changesSavedCallback) {
  //      this.changesSavedCallback();
  //    }
  //  },
  //    error => {
  //      this.alertService.error(error);

  //    });
  //}
  UpdatePassword(row: Patient) {
    this.authenticationService.resetPassword(this.patInfo.IdentityUserId, row.Password).subscribe(
      data => {
        if (this.changesSavedCallback) {
          this.changesSavedCallback();
        }
      },
      error => {
        this.alertService.error(error);
    
      });

  }
  UpdatePasswordDoctor(row: Doctor) {
    this.authenticationService.resetPassword(this.docInfo.IdentityUserId, row.Password).subscribe(
      data => {
        if (this.changesSavedCallback) {
          this.changesSavedCallback();
        }
      },
      error => {
        this.alertService.error(error);

      });

  }
  //public BackDashBoard() {
  //  this.routing.navigate(['/PatDashboard'], { queryParams: { id: this.hospitalId } });
  //}
  resetPwd(pat: Patient, patName:string) {
    this.patId = pat.PatientId;
    this.patName = patName;
    this.GetPatient();
    return this.patId;
  }
  resetPwdDoctor(pat: Doctor, docName: string, isDoctor:boolean) {
    this.docId = pat.DoctorId;
    this.docName = docName;
    this.isDoctor = isDoctor;
    this.GetDoctor();
    return this.docId;
  }
  cancel() {
    if (this.changesCancelledCallback) {
      this.changesCancelledCallback();
    }
  }
  showPass() {
    this.showPassword = !this.showPassword;
  }
}
