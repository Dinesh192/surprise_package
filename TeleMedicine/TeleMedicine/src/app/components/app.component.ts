import { Component } from '@angular/core';
import { Router } from '@angular/router';


import { AuthenticationService } from '../services/authentication.service';
import { User } from '../models/user/user.model';
import { HospitalService } from '../services/hospital/hospital.service';
import { PatientService } from '../services/patient/patient.service';
import { DoctorService } from '../services/doctor/doctor.service';
import { Global } from '../app.global';
import { AutoLogoutService } from '../services/autologout.service';
import { AlertService } from '../services/alert.service';
import { Patient } from '../models/patient/patient.model';
import { Doctor } from '../models/doctor/doctor.model';

@Component({ selector: 'app', templateUrl: 'app.component.html' })
export class AppComponent {
  currentUser: User;
  public token: any;
  public allVisitsList: any;
  public patInfo: Patient = new Patient();
  public docInfo: Doctor = new Doctor();
  // public patInfo: Patient = new Patient();
  constructor(public global: Global, private autoLogout: AutoLogoutService, public alertService: AlertService,
    public hospitalService: HospitalService, public patService: PatientService, public docService: DoctorService,
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    this.token = this.authenticationService.currentUserValue;
    if (this.token.UserType == 'Doctor') {
      this.GetDoctorName();
    }
    if (this.token.UserType == 'Patient') {
      this.GetPatientName();
    }

  }



  logout() {
    this.PatStatusChange();
    this.global.apiLogout = true;
    //this.authenticationService.logout();
    this.docService.DoctorId = null;
    this.patService.VisitId = null;
    this.hospitalService.HospitalId = null;
    this.hospitalService.DepartmentId = null;
    this.global.DepartmentId = null;
    this.global.DoctorId = null;
    this.global.HospitalId = null;
    this.router.navigate(['/login']);
    //this.PatStatusChange();
  }

  public GetDoctorName() {
    //var id= this.token.DoctorIdentifier;
    this.docService.getDoctor(this.token.DoctorIdentifier).subscribe(res => {
      this.docInfo = res;
    });
    error => {
      this.alertService.error(error);
    }
  }

  public GetPatientName() {
    //var id = this.token.PatientIdentifier;
    this.patService.getPatient(this.token.PatientIdentifier).subscribe(
      (res) => {
        this.patInfo = res;
      },
      (error) => {
        this.alertService.error(error);
      }
    );
  }
  public PatStatusChange() {
    if (this.token.UserType == 'Patient') {
      this.patService.getPatientBookingList(this.token.PatientIdentifier).subscribe(res => {
        this.allVisitsList = res;
        for (var i = 0; i < this.allVisitsList.length; i++) {
          if ((this.allVisitsList[i].Status === 'ongoing' || this.allVisitsList[i].Status === 'initiated') && this.allVisitsList[i].IsActive === true) {
            this.patService.changeStatus(this.allVisitsList[i].VisitId)
              .subscribe(res => this.SuccesschangeStatus(res),
                res => this.Error(res));
          }
        }
       
      },
        error => {
          this.alertService.error(error);
          // this.loading = false;
        });
    }
  }
  Error(res) {

  }
  SuccesschangeStatus(res) {
    this.authenticationService.logout();
    // this.router.navigate(['/login']);
  }

}
