import { Injectable, ChangeDetectorRef } from '@angular/core';
import { Router } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { Hospital } from '../../models/hospital/hospital.model';
import { DoctorEndpoint } from '../doctor/doctor-endpoint.service';
import { HospitalEndpoint } from './hospital-endpoint.service';
import { Doctor } from 'src/app/app.model';

@Injectable()
export class HospitalService {
  constructor(private router: Router, private http: HttpClient, private hospitalEndPoint: HospitalEndpoint) {
  }
  public _HospitalId: string;
  get HospitalId(): string
  {
    return this._HospitalId;
  }
  set HospitalId(HospitalId: string) {
    this._HospitalId = HospitalId;
  }

  public _DepartmentId: string;
  get DepartmentId(): string {
    return this._DepartmentId;
  }
  set DepartmentId(DepartmentId: string) {
    this._DepartmentId = DepartmentId;
  }

  public _PaymentEnable: boolean;
  get PaymentEnable(): boolean {
    return this._PaymentEnable;
  }
  set PaymentEnable(PaymentEnable: boolean) {
    this._PaymentEnable = PaymentEnable;
  }

  getHospitalList() {
    return this.hospitalEndPoint.geHospitalListEndpoint<Hospital[]>();
  }
  getHospital(HospitalId?:string) {
    return this.hospitalEndPoint.geHospitalEndpoint<Hospital>(HospitalId);
  }
  getDocHospitalList(doctorId:string) {
    return this.hospitalEndPoint.getDocHospitalListEndpoint<Doctor>(doctorId);
  }
  addHospital(data: Hospital){
    return this.hospitalEndPoint.addHospitalEndpoint<Hospital[]>(data);
  }
  deleteHospital(HospitalId: string) {
    return this.hospitalEndPoint.deleteHospitalEndpoint<Hospital>(HospitalId);
  }
  updateHospital(hospital: Hospital) {
    if (hospital.HospitalId) {
      return this.hospitalEndPoint.updateHospitalEndpoint(hospital, hospital.HospitalId);
    }
    else {
      return this.hospitalEndPoint.updateHospitalEndpoint<Hospital>(hospital, hospital.HospitalId).pipe(
        mergeMap(map => {
          hospital.HospitalId = hospital.HospitalId;
          return this.hospitalEndPoint.updateHospitalEndpoint(hospital, hospital.HospitalId)
        }));
    }
  }
}
