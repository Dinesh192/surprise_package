import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators'
import { map } from 'rxjs/operators';
import * as jwt_decode from 'jwt-decode';


import { User } from '../models/user/user.model';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from './notification.service';
import { decode } from 'querystring';
import { error } from 'protractor';
import { Global } from '../app.global';


@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  userType: string;
  jwtToken: any;
  loginVal: any;
  result: boolean;
  visitId: any;
  constructor(private http: HttpClient, private route: ActivatedRoute,
    private router: Router, private notifyService: NotificationService, private global: Global) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get loginValue() {
    const token = sessionStorage.getItem('jwtToken');
    const loginP = localStorage.getItem('loginPat');
    const loginD = localStorage.getItem('loginDoc');
    const decoded = jwt_decode(token);
    this.userType = decoded.UserType;

    if (loginP !== null || loginD !== null) {
      if (this.userType === "Patient") {
        this.loginVal = loginP;
      }
      else if (this.userType === "Doctor") {
        this.loginVal = loginD;
      }
    }
    return this.loginVal
  }
  public get visitid() {
    const vid = localStorage.getItem('vid');
    this.visitId = vid;
    return this.visitId
  }
  public get currentUserValue(): User {
    const token = sessionStorage.getItem('jwtToken');
    //if (token != null || token != undefined) {
    const decoded = jwt_decode(token);
   
      return this.jwtToken = decoded;
    //}
    //else {
    //  return "NoToken";
    //}

    //let token = this.currentUserSubject.value.token;
    //let token = this.currentUserSubject.value.token;
    //var decoded = jwt_decode(token);
  }

  login(phonenumber: string, password: string, isPatient: boolean) {
    return this.http.post<any>(`/api/account/login`, { phonenumber, password })
      .pipe(map(user => {
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          sessionStorage.removeItem('jwtToken');
          //localStorage.setItem('currentUser', JSON.stringify(user));
          sessionStorage.setItem('jwtToken', user.token);
          const token = user.token;
          const decoded = jwt_decode(token);
          this.jwtToken = decoded;
          this.userType = decoded.UserType;
          debugger;
          if (this.userType == "Patient") {
            localStorage.setItem('loginPat', "patient");
            this.router.navigate(['/HospitalList']);
            //this.currentUserSubject.next(user);
          }
          else if (this.userType == "Doctor") {
            localStorage.setItem('loginDoc', "doctor");
            this.router.navigate(['/DocDashboard']);
            //this.currentUserSubject.next(user);
          }

          else if (this.userType == "Admin") {
            this.router.navigate(['/admin']);
            this.notifyService.showInfo("Info", "Admin Page")
          }
          //else {
          //  decoded.UserType = "admin"
          //  this.jwtToken = decoded;
          //  this.router.navigate(['/register']);
          //  this.notifyService.showInfo("Info", "Registration for new Doctor")
          //}

        }
        return ;
      }));
  }

  logout() {
    // remove user from session storage to log user out
   
    const token = sessionStorage.getItem('jwtToken');
    const loginP = localStorage.getItem('loginPat');
    const loginD = localStorage.getItem('loginDoc');
    const decoded = jwt_decode(token);
    this.userType = decoded.UserType;
    if (loginP !== null || loginD !== null) {
      if (this.userType === "Patient") {
        localStorage.removeItem('loginPat');
        localStorage.setItem('loginPat', "logoutPat");
        
        localStorage.removeItem('vid');
      }
      else if (this.userType === "Doctor") {
        localStorage.removeItem('loginDoc');
        localStorage.removeItem('vid');
    
      }
    }

    this.currentUserSubject.next(null);
  }

  visitIdToken(vid) {
    localStorage.removeItem('vid');
    localStorage.setItem('vid', `${vid}`);
  }

  public get IsTokenValid(): Observable<any> {
    
     return this.http.get<any>('/api/account/ValidUserCheck');
    //  .subscribe(res => {
    //    debugger;
    //    this.result = true;
    //    console.log(res)
    //  },
    //    (err: HttpErrorResponse) => {     
    //      if (err.status == 401) {
    //        debugger;
    //        this.result = false;
    //      }
    //    },
    //    () => console.log("Request Complete"));
    //return this.result;
  }
//  validateSession() {
//  let seassionVal = sessionStorage.getItem('jwtToken');
//  if (seassionVal !== null) {
//    let sessionObj = JSON.parse(seassionVal);
//    let expiredAt = new Date(value.expiredAt);
//    if (expiredAt > new Date()) { // Validate expiry date.
//      return sessionObj.value;
//    } else {
//      sessionStorage.removeItem(key);
//    }
//  }
//  return null;
//}

  resetPassword(userId: string, password: string) {
    return this.http.post<any>(`/api/account/resetPassword`, { userId, password })
      .pipe(map(user => {
        // login successful if there's a jwt token in the response
        this.notifyService.showInfo("Info", "Reset Password successfully")
        return ;
      }));
  }
}
