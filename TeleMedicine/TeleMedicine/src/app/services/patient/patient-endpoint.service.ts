
import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { EndpointFactory } from '../endpoint-factory.service';
import { catchError } from 'rxjs/operators';

@Injectable()
export class PatientEndpoint extends EndpointFactory {
  
  private readonly _problemPostUrl: string = "/api/patient/postProblem";
  private readonly _patientListUrl: string = "/api/patient/listPatientByProviderId";
  private readonly _patientStatusUrl: string = "/api/patient/patientStatus";
  private readonly _patientStatusByPatIdUrl: string = "/api/patient/patientStatusByPatId";
  private readonly _patientStatusByPatientUrl: string = "/api/patient/patientStatusByPatientUrl";
  private readonly _patientHistoryUrl: string = "/api/patient/patientHistory";
  private readonly _patientStatusChangeUrl: string = "/api/patient/patientStatusChange";
  private readonly _patientStatusChangefromDocUrl: string = "/api/patient/patientStatusChangefomDoc";
  private readonly _patientLoginUrl: string = "/api/patient/login";
  private readonly _patientRegisterUrl: string = "/api/account/registerPatient";
  private readonly _patientTreatmentAdviceUpdateUrl: string ="/api/patient/updateTreatmentAdvice";
  private readonly _updateStatusUrl: string = "/api/patient/updateStatus";
  private readonly _patientUpdateUrl: string = "/api/patient/updatePatient";
  private readonly _patientDetailsUrl: string = "/api/patient/getDetails";
  private readonly _patientDetailsUrlByVisitId: string = "/api/patient/getDetailsByVisitId";
  private readonly _patientFinalReportByVisitId: string = "/api/patient/getFinalReportData";
  private readonly _patientDocumentsByVisitId: string = "/api/patient/getPatientDocuments";
  private readonly _patientDeleteUrl: string = "/api/patient/deletePatient";
  private readonly _updatePaidStatusUrl: string = "/api/patient/updatePaidStatus";
  private readonly _updatePaymentStatusUrl: string = "/api/patient/updatePaymentStatus";
  private readonly _cancelBookingUrl: string = "/api/patient/cancelBooking";
  private readonly _paymentChargeUrl: string = "/api/patient/paymentChargeByVisitId";
  private readonly _patientStatusByVisitIdUrl: string = "/api/patient/patientStatusByVisitId";
  private readonly _patHistoryUrl: string = "/api/patient/viewPrescription";
  private readonly _patientAllPatientListUrl: string = "/api/patient/adminPatient";
  
  constructor(http: HttpClient, injector: Injector) {
    super(http, injector);
  }

  getPatientByIdEndpoint<T>(PatientId: string): Observable<T> {
    let endpointUrl = `${this._patientDetailsUrl}/${PatientId}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getPatientByIdEndpoint(PatientId)));
      }));
  }
  getPatientByVisitIdEndpoint<T>(VisitId: string): Observable<T> {
    let endpointUrl = `${this._patientDetailsUrlByVisitId}/${VisitId}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getPatientByVisitIdEndpoint(VisitId)));
      }));
  }
  t
  getFinalReportDataEndpoint<T>(VisitId: string): Observable<T> {
    let endpointUrl = `${this._patientFinalReportByVisitId}/${VisitId}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getFinalReportDataEndpoint(VisitId)));
      }));
  }
  getPatientDocumentsEndpoint<T>(VisitId: string): Observable<T> {
    let endpointUrl = `${this._patientDocumentsByVisitId}/${VisitId}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getPatientDocumentsEndpoint(VisitId)));
      }));
  }
  getPatientListEndpoint<T>(DoctorId: string): Observable<T> {
    let endpointUrl = `${this._patientListUrl}/${DoctorId}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getPatientListEndpoint(DoctorId)));
      }));
  }

  getPatientHistoryEndpoint<T>(PatientId: string): Observable<T> {
    let endpointUrl = `${this._patientHistoryUrl}/${PatientId}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getPatientHistoryEndpoint(PatientId)));
      }));
  }
  getPatHistoryEndpoint<T>(PatientId: string): Observable<T> {
    let endpointUrl = `${this._patHistoryUrl}/${PatientId}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getPatHistoryEndpoint(PatientId)));
      }));
  }
  getAllPatientEndPoint<T>(): Observable<T> {
    let endpointUrl = `${this._patientAllPatientListUrl}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getAllPatientEndPoint()));
      }));
  }
  getPatientBookingListEndpoint<T>(PatientId: string): Observable<T> {
    let endpointUrl = `${this._patientHistoryUrl}/${PatientId}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getPatientBookingListEndpoint(PatientId)));
      }));
  }
  
  changeStatusEndpoint<T>(VisitId: string): Observable<T> {
    let endpointUrl = `${this._patientStatusChangeUrl}/${VisitId}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.changeStatusEndpoint(VisitId)));
      }));
  }
  
  changeStatusFromDocEndpoint<T>(VisitId: string): Observable<T> {
    let endpointUrl = `${this._patientStatusChangefromDocUrl}/${VisitId}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.changeStatusEndpoint(VisitId)));
      }));
  }
  getPatientDocListEndpoint<T>(DoctorId: string): Observable<T> {
    let endpointUrl = `${this._patientListUrl}/${DoctorId}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getPatientDocListEndpoint(DoctorId)));
      }));
  } 
  getStatusEndpoint<T>(VisitId: string): Observable<T> {
    let endpointUrl = `${this._patientStatusUrl}/${VisitId}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getStatusEndpoint(VisitId)));
      }));
  } 
  
  getStatusByPatientIdEndpoint<T>(VisitId: string): Observable<T> {
    let endpointUrl = `${this._patientStatusByPatIdUrl}/${VisitId}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getStatusByPatientIdEndpoint(VisitId)));
      }));
  }
  getStatusByPatientUrlEndpoint<T>(PatientId: string): Observable<T> {
    let endpointUrl = `${this._patientStatusByPatientUrl}/${PatientId}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getStatusByPatientUrlEndpoint(PatientId)));
      }));
  }
  getStatusByVisitIdEndpoint<T>(VisitId: string): Observable<T> {
    let endpointUrl = `${this._patientStatusByVisitIdUrl}/${VisitId}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getStatusByVisitIdEndpoint(VisitId)));
      }));
  }






  //postVisitEndpoint<T>(userObject: any, formToPost): Observable<T> {

  //  return this.http.post<T>(this._problemPostUrl, JSON.stringify(userObject), this.getRequestHeaders()).pipe(
  //    catchError(error => {
  //      return throwError(this.handleError(error, () => this.postVisitEndpoint(userObject, formToPost)));
  //    }));
  //}
  postVisitEndpoint<T>(userObject: any, ScheduleIntervalId?: string, SchedulingId?: string): Observable<T> {
    let endpointUrl = ScheduleIntervalId && SchedulingId ? `${this._problemPostUrl}/${ScheduleIntervalId}/${SchedulingId}` : this._problemPostUrl;
    return this.http.post<T>(endpointUrl, JSON.stringify(userObject ), this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.postVisitEndpoint(userObject, ScheduleIntervalId, SchedulingId)));
      }));
  }
 
  getUpdatePatientEndpoint<T>(userObject: any, PatientId?: string): Observable<T> {
    let endpointUrl = PatientId ? `${this._patientUpdateUrl}/${PatientId}` : this._patientUpdateUrl;

    return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getUpdatePatientEndpoint(userObject, PatientId)));
      }));
  }
  updateTreatmentAdviceEndpoint<T>(userObject: any, VisitId?: string): Observable<T> {
    let endpointUrl = VisitId ? `${this._patientTreatmentAdviceUpdateUrl}/${VisitId}` : this._patientTreatmentAdviceUpdateUrl;

    return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.updateTreatmentAdviceEndpoint(userObject, VisitId)));
      }));
  }

  updateStatusEndpoint<T>(userObject: any, VisitId?: string): Observable<T> {
    let endpointUrl = VisitId ? `${this._updateStatusUrl}/${VisitId}` : this._patientTreatmentAdviceUpdateUrl;

    return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.updateStatusEndpoint(userObject, VisitId)));
      }));
  }

  updatePaidStatusEndpoint<T>(VisitId?: string): Observable<T> {
    let endpointUrl = VisitId ? `${this._updatePaidStatusUrl}/${VisitId}` : this._updatePaidStatusUrl;

    return this.http.put<T>(endpointUrl, JSON.stringify(VisitId), this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.updatePaidStatusEndpoint(VisitId)));
      }));
  }
  
  updatePaymentStatusEndpoint<T>(userObject: any,VisitId?: string): Observable<T> {
    let endpointUrl = VisitId ? `${this._updatePaymentStatusUrl}/${VisitId}` : this._updatePaymentStatusUrl;

    return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.updatePaymentStatusEndpoint(userObject,VisitId)));
      }));
  }
  
  cancelBookingEndpoint<T>(userObject: any, VisitId?: string): Observable<T> {
    let endpointUrl = VisitId ? `${this._cancelBookingUrl}/${VisitId}` : this._cancelBookingUrl;

    return this.http.put<T>(endpointUrl, JSON.stringify(userObject), this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.cancelBookingEndpoint(userObject, VisitId)));
      }));
  }
  registerPatientEndpoint<T>(userObject: any): Observable<T> {

    return this.http.post<T>(this._patientRegisterUrl, JSON.stringify(userObject), this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.registerPatientEndpoint(userObject)));
      }));
  }
  getPaymentChargeEndpoint<T>(VisitId: string): Observable<T> {
    let endpointUrl = `${this._paymentChargeUrl}/${VisitId}`;

    return this.http.get<T>(endpointUrl, this.getRequestHeaders()).pipe(
      catchError(error => {
        return throwError(this.handleError(error, () => this.getPaymentChargeEndpoint(VisitId)));
      }));
  }
}
