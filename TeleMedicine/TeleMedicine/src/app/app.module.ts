import { BrowserModule } from "@angular/platform-browser";
import { NgModule, APP_INITIALIZER } from "@angular/core";
import {
  APP_BASE_HREF,
  LocationStrategy,
  HashLocationStrategy,
} from "@angular/common";

//import { LoginComponent } from './app.login';
import { Global } from "./app.global";
import { DoctorRoomComponent } from "./Doctor/doctor_doctorroom/app.doctorroomcomponent";
import { WaitingRoom } from "./Patient/patient_waitingroom/app.waitingroomcomponent";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { MAT_DIALOG_DATA } from "@angular/material";
import { RouterModule } from "@angular/router";
import { TeleAppRoutingModule } from "./app.routing";
import { ClinicComponent } from "./app.cliniccomponent";
import { FinalReportComponent } from "./app.finalreportcomponent";
import { YesNoPipe } from "src/common/YesNo.pipe";
import "zone.js/dist/zone";
import { ConfigService } from "src/common/common.appconfig";
import { SafePipe } from "src/common/common.safe";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { DoctorDashboardComponent } from "./Doctor/doctor_dashboard/app.doctor-dashboard";
import { DoctorUpdateProfileComponent } from "./Doctor/doctor_updateprofile/app.doctor_updateprofile";
import { DoctorListComponent } from "./Doctor/doctor_list/doctor_list.component";
import { RegisterComponent } from "./components/register/register.component";
import { DoctorService } from "../../src/app/services/doctor/doctor.service";
import { LoginComponent } from "./components/login/login.component";
import { DoctorEndpoint } from "./services/doctor/doctor-endpoint.service";
import { PatientBookAppointmentComponent } from "../app/Patient/patient_booking/app.patient_bookappointment";
import { HospitalEndpoint } from "./services/hospital/hospital-endpoint.service";
import { HospitalService } from "./services/hospital/hospital.service";
import { PatientDashboardComponent } from "./Patient/patient_dashboard/app.patient_dashboard";
import { PatientUpdateProfileComponent } from "./Patient/patient_updateprofile/app.patient_updateprofile";
import { PatientHistoryComponent } from "./Patient/patient_history/app.patient_history";
import { PatientService } from "./services/patient/patient.service";
import { PatientEndpoint } from "./services/patient/patient-endpoint.service";
import { Visit } from "./models/visit/visit.model";
import { DailyVisitComponent } from "./Doctor/doctor_dailyvisit/app.dailyvisitcomponent";
import { ToastrModule } from "ngx-toastr";
import * as moment from "moment/moment";
import { NgxPaginationModule } from "ngx-pagination";
import {
  NgbModalModule,
  NgbTabsetModule,
  NgbModule,
} from "@ng-bootstrap/ng-bootstrap";
import { CalendarModule, DateAdapter } from "angular-calendar";
import { adapterFactory } from "angular-calendar/date-adapters/date-fns";
//import * as fns from  'date-fns';
import { FullCalendarModule } from "@fullcalendar/angular";
import { OwlDateTimeModule, OwlNativeDateTimeModule } from "ng-pick-datetime";
import { TokenInterceptorService } from "./services/tokeninterceptor.service";
import { PatientBookingHomeComponent } from "./Patient/patient_booking/app.booking_home";
import { Ng2SearchPipeModule } from "ng2-search-filter";
import { NgxDocViewerModule } from "ngx-doc-viewer";
import { AppComponent } from "./components/app.component";
import { MyBooKListComponent } from "./Patient/patient_booking/app.mybooklist";
import { DatePipe } from "@angular/common";
import { PayThroughCreditCardComponent } from "./Patient/patient_payment/app.paythroughcreditcardcomponent";
import { PaidBookingListComponent } from "./Patient/patient_booking/app.paidbookinglistcomponent";
import { PdfViewerModule } from "ng2-pdf-viewer";
import { NgxExtendedPdfViewerModule } from "ngx-extended-pdf-viewer";
import { PaymentSelectionComponent } from "./Patient/patient_payment/app.paymentselection.component";
import { HospitalComponent } from "./app.hospitalcomponent";
import { AdminComponent } from "./components/admin/admin.component";
import { DoctorSchedulingComponent } from "./components/admin/doctorscheduling/doctor-scheduling.component";
import { SchedulingService } from "./services/scheduling/scheduling.service";
import { SchedulingEndpoint } from "./services/scheduling/scheduling-endpoint.service";
import { FooterComponent } from "./components/common/footer.component";
import { BookingNavigatorComponent } from "./components/common/booking_navigator/booking.navigator.component";
import {
  MAT_DATE_LOCALE,
  MAT_DATE_FORMATS,
  MatCheckboxModule,
  MatDatepickerModule,
  MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatDividerModule,
  MatButtonModule,
  MatListModule,
  MatRadioModule,
  MatAutocompleteModule,
  MatTabsModule,
  MatCardModule,
  MatIconModule,
  MatSelectModule,
  MatNativeDateModule,
  MatChipsModule,
} from '@angular/material';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MatMomentDateModule } from '@angular/material-moment-adapter';
import { AppointmentsAddComponent } from './components/admin/doctorscheduling/appointmentadd/appointment-add.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { DoctorProfileComponent } from './Doctor/doctor_profile/app.doctor_profile';
//import { DoctorSchedulingNewComponent } from './components/admin/doctorscheduling/doctor-scheduling-new.component';
import { EqualValidator } from "./components/directives/equal-validator.directive";
import { OnlyNumber } from "./components/directives/numbers-only.directive";
import { PaymentMessageComponent } from "./PaymentMessage/app.paymentmessage.component";
import { PatientListComponent } from "./Doctor/doctor_patientlist/app.patientlistcomponent";
import { AddAppointmentComponent } from "./components/admin/doctorscheduling/add-appointment/add-appointment.component";
import { ConsentFormComponent } from "./ConsentForm/app.consentformcomponent";
import { TimeFormat } from "./components/common/date_pipe/date_pipe";
import { RescheduleComponent } from "./Doctor/doctor_reschedule/app.doctor_reschedule";
import { ChatService } from "./services/ChatNotification/chat-notification.service";
import { SetUpComponent } from "./components/admin/setup/app.set-up";
import { ModalDirective } from "ngx-bootstrap/modal";
import { AutoLogoutService } from "./services/autologout.service";
import { AdminPanelPatientAppointment } from "./components/admin/patientappointment/admin.patientappointment.component";
import { AdminPatientListComponent } from "./components/admin/patient_list/patient_list.component";
import { PopUpDialogComponent } from "./components/common/popup/pop-up.component";
import { SlickCarouselModule } from "ngx-slick-carousel";
import { HospitalSpecificDoctorListComponent } from "./app.hospitalspecificdoclist.component";
import { PasswordComponent } from "./components/common/password/password.component";


const initializerConfigFn = (config: ConfigService) => {
  return () => {
    var ret: any = config.loadAppConfig();
    return ret;
  };
};
@NgModule({
  declarations: [
    LoginComponent, DoctorRoomComponent, OnlyNumber, PaymentMessageComponent, DoctorProfileComponent,
    WaitingRoom, ClinicComponent, FinalReportComponent, PatientListComponent, YesNoPipe, SafePipe, DoctorDashboardComponent, DoctorUpdateProfileComponent, EqualValidator,
    PatientDashboardComponent, PatientUpdateProfileComponent, PatientBookAppointmentComponent, PaymentSelectionComponent, PatientBookingHomeComponent, AppComponent,
    RegisterComponent, PatientHistoryComponent, DailyVisitComponent, PatientBookingHomeComponent, MyBooKListComponent, PayThroughCreditCardComponent, PaidBookingListComponent,
    HospitalComponent, AdminComponent, DoctorSchedulingComponent, AppointmentsAddComponent, FooterComponent, AddAppointmentComponent, ConsentFormComponent, BookingNavigatorComponent,
    TimeFormat, RescheduleComponent, SetUpComponent, DoctorListComponent, AdminPanelPatientAppointment, AdminPatientListComponent, PopUpDialogComponent, HospitalSpecificDoctorListComponent, PasswordComponent
  ],
  entryComponents: [AppointmentsAddComponent, PaymentMessageComponent, ConsentFormComponent, RescheduleComponent, PopUpDialogComponent],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDocViewerModule,
    HttpClientModule,
    PdfViewerModule,
    NgxPaginationModule,
    NgxExtendedPdfViewerModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    TeleAppRoutingModule,
    BrowserAnimationsModule,
    NgxDocViewerModule,
    NgbModule,
    NgbModalModule,
    NgbTabsetModule,
    FullCalendarModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
    MatCheckboxModule,
    MatDatepickerModule,
    MatDialogModule,
    MatFormFieldModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatDividerModule,
    MatButtonModule,
    MatDialogModule,
    MatRadioModule,
    MatListModule,
    MatCardModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatIconModule,
    MatTabsModule,
    MatNativeDateModule,
    MatMomentDateModule,
    MatChipsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    NgbModule,
    Ng2SearchPipeModule,

    BsDatepickerModule.forRoot(),

    SlickCarouselModule,
  ],
  providers: [
    {
      //provide: APP_BASE_HREF,
      ////useValue: '/' + (window.location.pathname.split('/')[1] || ''),
      //useValue: '/',
      provide: LocationStrategy,
      useClass: HashLocationStrategy,
    },
    {
      provide: APP_INITIALIZER,
      useFactory: initializerConfigFn,
      multi: true,
      deps: [ConfigService],
    },
    {
      provide: MAT_DIALOG_DATA,
      useValue: [],
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true,
    },
    MatDatepickerModule,
    MatNativeDateModule,
    MatAutocompleteModule,
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
    Global,
    DoctorService,
    DoctorEndpoint,
    HospitalEndpoint,
    HospitalService,
    PatientEndpoint,
    PatientService,
    DatePipe,
    SchedulingService,
    SchedulingEndpoint,
    ChatService,
    AutoLogoutService,
    TokenInterceptorService,
  ],
  bootstrap: [ClinicComponent],
})
export class AppModule {
  constructor(g: Global) {
    // alert(g.config.videoUrl);
  }
}
