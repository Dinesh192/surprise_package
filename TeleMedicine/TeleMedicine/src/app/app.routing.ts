import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
//import { LoginComponent } from './app.login';
import { WaitingRoom } from './Patient/patient_waitingroom/app.waitingroomcomponent';
import { DoctorRoomComponent } from './Doctor/doctor_doctorroom/app.doctorroomcomponent';
import { FinalReportComponent } from './app.finalreportcomponent';
import { DoctorDashboardComponent } from './Doctor/doctor_dashboard/app.doctor-dashboard';
import { DoctorUpdateProfileComponent } from './Doctor/doctor_updateprofile/app.doctor_updateprofile';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { PatientDashboardComponent } from './Patient/patient_dashboard/app.patient_dashboard';
import { PatientUpdateProfileComponent } from './Patient/patient_updateprofile/app.patient_updateprofile';
import { PatientBookAppointmentComponent } from './Patient/patient_booking/app.patient_bookappointment';
import { PatientHistoryComponent } from './Patient/patient_history/app.patient_history';
import { DailyVisitComponent } from './Doctor/doctor_dailyvisit/app.dailyvisitcomponent';
import { PatientBookingHomeComponent } from './Patient/patient_booking/app.booking_home';
import { MyBooKListComponent } from './Patient/patient_booking/app.mybooklist';
import { PayThroughCreditCardComponent } from './Patient/patient_payment/app.paythroughcreditcardcomponent';
import { PaidBookingListComponent } from './Patient/patient_booking/app.paidbookinglistcomponent';
import { PaymentSelectionComponent } from './Patient/patient_payment/app.paymentselection.component';
import {  HospitalComponent } from './app.hospitalcomponent';
import { AdminComponent } from './components/admin/admin.component';
import { DoctorSchedulingComponent } from './components/admin/doctorscheduling/doctor-scheduling.component';
import { AddAppointmentComponent } from './components/admin/doctorscheduling/add-appointment/add-appointment.component';
import { DoctorProfileComponent } from './Doctor/doctor_profile/app.doctor_profile';
import { PatientListComponent } from './Doctor/doctor_patientlist/app.patientlistcomponent';
import { SetUpComponent } from './components/admin/setup/app.set-up';
import { AdminPanelPatientAppointment } from './components/admin/patientappointment/admin.patientappointment.component';
import { DoctorListComponent } from './Doctor/doctor_list/doctor_list.component';

import { AdminPatientListComponent } from './components/admin/patient_list/patient_list.component';
import { HospitalSpecificDoctorListComponent } from './app.hospitalspecificdoclist.component';

@NgModule({
  imports: [
    RouterModule.forRoot([
      { path: '', component: LoginComponent },
      { path: 'DocUpdateProfile', component: DoctorUpdateProfileComponent },
      { path: 'DocDashboard', component: DoctorDashboardComponent },
      { path: 'PatDashboard', component: PatientDashboardComponent },
      { path: 'PatUpdateProfile', component: PatientUpdateProfileComponent },
      { path: 'PatBookHome', component: PatientBookingHomeComponent },
      { path: 'PatBookAppointment', component: PatientBookAppointmentComponent },
      { path: 'PayThroughCreditCard', component: PayThroughCreditCardComponent },
      { path: 'PatBookList', component: MyBooKListComponent },
      { path: 'PatHistory', component: PatientHistoryComponent },
      { path: 'PatPaidBookingList', component: PaidBookingListComponent },
      { path: 'HospitalList' , component:HospitalComponent},
      { path: 'HospitalSpecificDoctorList', component: HospitalSpecificDoctorListComponent},
      { path: 'WaitingRoom', component: WaitingRoom },
      { path: 'DoctorRoom', component: DoctorRoomComponent },
      { path: 'FinalReport', component: FinalReportComponent },
      //{ path: 'BookAppointment', component: BookAppointmentComponent },
      { path: 'DailyVisit', component: DailyVisitComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'PaymentSelection', component: PaymentSelectionComponent },
      { path: 'PatBookList', component: MyBooKListComponent },
      { path: 'admin', component: AdminComponent },
      { path: 'scheduling', component: DoctorSchedulingComponent },
      { path: 'DocProfile', component: DoctorProfileComponent },
      { path: 'doclist', component: DoctorListComponent },
      { path: 'DocPatientList', component: PatientListComponent },
      { path: 'addAppointment',component:AddAppointmentComponent},
      { path: 'Setup', component:SetUpComponent},
      { path: 'PatientAppointmentList', component: AdminPanelPatientAppointment },
      { path: 'adminpatlist', component: AdminPatientListComponent },
  // otherwise redirect to login
  { path: '**', redirectTo: '' }

    ])
  ],
  exports: [
    RouterModule
  ]
})
export class TeleAppRoutingModule { }

