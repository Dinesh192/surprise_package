﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestTele.Authorization
{
    //this is just a Marker Interface required by .net framework for authorization
    public class VisitOfPatientRequirement: IAuthorizationRequirement
    {
    }
}
