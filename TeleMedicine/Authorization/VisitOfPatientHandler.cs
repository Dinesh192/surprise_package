﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telemedicine.DalLayer;

namespace TestTele.Authorization
{
    public class VisitOfPatientHandler : AuthorizationHandler<VisitOfPatientRequirement, Guid>
    {
        private IUnitOfWork _unitOfWork;

        public VisitOfPatientHandler(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, VisitOfPatientRequirement requirement, Guid visitId)
        {
            Guid patientIdOfVisit = _unitOfWork.Visits.GetSingleOrDefault(v => v.VisitId == visitId).PatientId;
            Guid loggedInUserId = Guid.Parse(context.User.FindFirst(Constants.PatientIdentifier).Value);
            if(patientIdOfVisit == loggedInUserId)
            {
                context.Succeed(requirement);
            }
            return Task.CompletedTask;
        }
    }
}
