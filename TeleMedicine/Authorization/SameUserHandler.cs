﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestTele.Authorization
{
    public class SameUserHandler : AuthorizationHandler<SameUserRequirement, Guid>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, SameUserRequirement requirement, Guid userId)
        {
            if(requirement.UserType == "Doctor")
            {
                if (Guid.Parse(context.User.FindFirst(Constants.DoctorIdentifier).Value) == userId)
                {
                    context.Succeed(requirement);
                }

            }
            if (requirement.UserType == "Admin")
            {
                if (Guid.Parse(context.User.FindFirst(Constants.AdminIdentifier).Value) == userId)
                {
                    context.Succeed(requirement);
                }

            }
            else if (requirement.UserType == "Patient")
            {
                if (Guid.Parse(context.User.FindFirst(Constants.PatientIdentifier).Value) == userId)
                {
                    context.Succeed(requirement);
                }
            }
            return Task.CompletedTask;
        }
    }
}
