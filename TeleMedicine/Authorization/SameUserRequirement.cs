﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestTele.Authorization
{
    public class SameUserRequirement : IAuthorizationRequirement
    {
        public SameUserRequirement(string userType)
        {
            this.UserType = userType;
        }
        public string UserType { get; private set; }
    }
}
