using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json.Serialization;
using Telemedicine.DalLayer;
using Telemedicine.DALLayer;
using TestTele.Models;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Telemedicine.ServerModel;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using TestTele.AutoMapper;
using TestTele.Service;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.OpenApi.Models;
using TestTele.Authorization;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Amazon.OpsWorks.Model;
using TestTele.Service.NotificationService;
using Microsoft.Azure.Management.Storage.Fluent.Models;
using EO.Internal;
using Microsoft.Azure.Management.Storage.Models;

namespace TestTele
{
    // changes
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddSingleton<List<Doctor>>();
            services.AddSingleton<List<Patient>>();
            services.AddCors(o => o.AddPolicy("AllowMyOrigin", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));
            services.AddControllers().AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ContractResolver = new DefaultContractResolver();
            });
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(Configuration.GetConnectionString("TeleMed"));
            });

            services.AddIdentity<ApplicationUser, IdentityRole>(options =>
            {
                //options.User.RequireUniqueEmail = true;
                //options.Password.RequireDigit = true;  // password strenght yeta configure garna milcha
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
                options.SignIn.RequireConfirmedEmail = false;
            })
             .AddEntityFrameworkStores<ApplicationDbContext>();


            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters()
                    {
                        // The signing key must match!
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Tokens:JwtKey"])),

                        // Validate the JWT Issuer (iss) claim
                        ValidateIssuer = true,
                        ValidIssuer = Configuration["Tokens:JwtIssuer"],

                        // Validate the JWT Audience (aud) claim
                        ValidateAudience = true,
                        ValidAudience = Configuration["Tokens:JwtAudience"],

                        // Validate the token expiry
                        ValidateLifetime = true,

                        // Clock skew compensates for server time drift.
                        // We recommend 5 minutes or less:
                        // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                        // If you want to allow a certain amount of clock drift, set that here:
                        ClockSkew = TimeSpan.Zero
                    };
                    options.Events = new JwtBearerEvents
                    {

                        OnMessageReceived = context =>
                        {

                            if (context.Request.Path.Value.StartsWith("/NotificationHub"))
                            {
                                context.Token = context.Request.Query["token"];
                            }
                            return Task.CompletedTask;
                        },
                        OnAuthenticationFailed = context =>
                        {
                            var te = context.Exception;
                            return Task.CompletedTask;
                        }
                    };
                });



            services.AddAuthorization(options =>
            {
                options.AddPolicy("CanRegisterDoctor", policyBuilder =>
                                              policyBuilder.RequireAuthenticatedUser()
                                                           .RequireClaim(Constants.UserType, "Admin"));

                options.AddPolicy("IsVistOfPatient", policyBuilder =>
                {
                    policyBuilder.AddRequirements(new VisitOfPatientRequirement());
                });
                options.AddPolicy("IsPatientSame", policyBuilder =>
                {
                    policyBuilder.AddRequirements(new SameUserRequirement("Patient"));
                });

                options.AddPolicy("IsDoctorSame", policyBuilder =>
                {
                    policyBuilder.AddRequirements(new SameUserRequirement("Doctor"));
                });
            });
            services.AddScoped<IAuthorizationHandler, SameUserHandler>();
            services.AddScoped<IAuthorizationHandler, VisitOfPatientHandler>();

            // Repositories
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddAutoMapper(typeof(Startup));

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
            });
            services.Configure<IISServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });
            services.AddSignalR((options =>
            {
                options.EnableDetailedErrors = true;
                options.KeepAliveInterval = TimeSpan.FromMinutes(120);
            }));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                });
                app.UseMiddleware<StackifyMiddleware.RequestTracerMiddleware>();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            // Custom Middleware to add X-Frame-Options to prevent clickjacking attacks
            app.Use(async (context, next) =>
            {
                context.Response.Headers.Add("X-Frame-Options", "SAMEORIGIN");
                await next();
            });

            // uncommit these to host locallost in public internet using Ngrok
            //app.UseForwardedHeaders(new ForwardedHeadersOptions
            //{
            //    ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            //});
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
            app.UseCors("AllowMyOrigin");
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapFallbackToController("Index", "Home");

            });
            app.UseEndpoints(endpoints => {
                endpoints.MapHub<NotificationHub>("/NotificationHub");
            });
        }
    }

    
}
