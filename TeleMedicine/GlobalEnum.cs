﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestTele
{
    public enum TeleConstants
    {
        InCall = 1,
        InActive = 1,
        NotInCall = -1,
        NotInActive = -1,
    }

    public static class Constants
    {
        public const string UserType = "UserType";
        public const string DoctorIdentifier = "DoctorIdentifier";
        public const string PatientIdentifier = "PatientIdentifier";
        public const string AdminIdentifier = "AdminIdentifier";
        public const string Hospital = "HospitalIdentifier";
        public const string Name = "Name";
    }
}
