﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Telemedicine.DalLayer;
using Telemedicine.ServerModel;
using TestTele.Service.PaymentService;

namespace TestTele.Controllers
{
    public class PaymentController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IWebHostEnvironment _environment;
        private readonly IConfiguration _configuration;
        private readonly ILogger<PaymentController> _logger;
        private readonly ILoggerFactory _loggerFactory;
        private readonly IAuthorizationService _authorizationService;
        public PaymentController(IUnitOfWork unitOfWork, IWebHostEnvironment environment,
                                 IConfiguration configuration, ILogger<PaymentController> logger,
                                 ILoggerFactory loggerFactory,
                                 IAuthorizationService authorizationService)
        {
            _unitOfWork = unitOfWork;
            _environment = environment;
            _configuration = configuration;
            _logger = logger;
            _loggerFactory = loggerFactory;
            _authorizationService = authorizationService;
        }
        // this end point is hit by esewa on successful transaction
        public async Task<IActionResult> EsewaSuccess(string oid, string amt, string refId)
        {
            try
            {
                var paymentService = new PaymentService(new EsewaPaymentProvider(_unitOfWork, _configuration));
                PaymentParameters paymentParameters = new PaymentParameters
                {
                    VisitId = oid,
                    Amount = amt,
                    ProviderReferenceId = refId
                };
                //Payment Verification by calling esewa's API to prevent fraudulent transactions
                if (await paymentService.IsTransactionValid(paymentParameters))
                {
                    //if transaction valid save it into our system
                    paymentService.SaveTransaction(paymentParameters);

                    // redirect to Home controller's Index Action Method to load angular app
                    TempData["PaymentMethod"] = "Esewa";
                    TempData["PaymentStatus"] = "success";
                    return RedirectToAction("Index", "Home");
                }
                TempData["PaymentMethod"] = "Esewa";
                TempData["PaymentStatus"] = "failure";
                return RedirectToAction("Index", "Home");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error in Esewa Success Call");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }

        // this end point is hit by esewa on failure of transaction
        public IActionResult EsewaFailure()
        {
            TempData["PaymentMethod"] = "Esewa";
            TempData["PaymentStatus"] = "failure";
            return RedirectToAction("Index", "Home");
        }
        // this end point is to Initiate Payment through Card
        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> NabilCard(string visitId)
        {
            try
            {
                //Check done to prevent Insecure Direct Object References(IDOR) vulnerability
                if (!(await _authorizationService.AuthorizeAsync(User, Guid.Parse(visitId), policyName: "IsVistOfPatient")).Succeeded)
                {
                    return Forbid();
                }
                var nabilPaymentService = new CompassPlusPaymentService(_unitOfWork, _environment, _configuration, _loggerFactory);
                decimal charge = nabilPaymentService.ChargeForVisit(visitId);
                //for amount/charge we should pass multiple of 100. For Rs 1 pass 100 
                //Nabil Bank said this
                int chargeToPG = Convert.ToInt32(charge) * 100;
                XDocument orderRequestXML = nabilPaymentService.GetOrderRequestData(visitId, chargeToPG.ToString());
                // save to as Transaction to database as pending
                nabilPaymentService.SaveCreateOrderRequest(orderRequestXML.ToString(SaveOptions.DisableFormatting), visitId, charge);
                XDocument createOrderResponseXML = await nabilPaymentService.CreateOrderRequestAsync(orderRequestXML);
                nabilPaymentService.SaveCreateOrderResponse(visitId, createOrderResponseXML.ToString(SaveOptions.DisableFormatting));
                if (createOrderResponseXML != null)
                {
                    _logger.LogInformation(createOrderResponseXML.ToString());
                    string responseStatusPG = createOrderResponseXML.Descendants("Response").First().Descendants("Status").First().Value;
                    if (responseStatusPG == "00")
                    {
                        // Purchase Request Successful 
                        // redirect Patient to Nabil PG to enter Card Details
                        string encryptedOrderId = createOrderResponseXML.Descendants("Response").First().Descendants("Order").First().Element("OrderID").Value;
                        string encryptedSessionId = createOrderResponseXML.Descendants("Response").First().Descendants("Order").First().Element("SessionID").Value;
                        string urlToRedirect = createOrderResponseXML.Descendants("Response").First().Descendants("Order").First().Element("URL").Value;
                        string prefixText = "@encrypted@1@";
                        string sessionId = nabilPaymentService.Decrypt(encryptedSessionId.Substring(prefixText.Length)).Trim();
                        string orderId = nabilPaymentService.Decrypt(encryptedOrderId.Substring(prefixText.Length)).Trim();
                        nabilPaymentService.SaveOrderIdAndSessionId(visitId, orderId, sessionId);
                        _logger.LogInformation($"Status 00 success from Nabil Compass Plus PG with OrderID {orderId} and SessionID {sessionId}");
                        var queryParams = new Dictionary<string, string>()
                        {
                            {"OrderID", encryptedOrderId},
                            {"SessionID", encryptedSessionId}
                        };
                        string clientLoginUrl = QueryHelpers.AddQueryString(urlToRedirect, queryParams);
                        return Ok(clientLoginUrl);
                    }
                    else
                    {
                        string errorMessage = nabilPaymentService.GetPGErrorMessage(responseStatusPG);
                        _logger.LogInformation($"Nabil PG responded with response status {responseStatusPG} with message {errorMessage}");
                        return BadRequest();
                    }
                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        // this end-point is hit by Compass Plus on Successful Payment
        [HttpPost]
        public async Task<IActionResult> NabilApprove()
        {
            try
            {
                var nabilPaymentService = new CompassPlusPaymentService(_unitOfWork, _environment, _configuration, _loggerFactory);
                XDocument nabilResponseXML = await nabilPaymentService.ReadBodyAsync(Request.Body);
                string visitId = nabilResponseXML.Descendants("OrderDescription").First().Value;
                nabilPaymentService.SaveResponseXMLOut(visitId, nabilResponseXML.ToString(SaveOptions.DisableFormatting));
                _logger.LogInformation(nabilResponseXML.ToString());
                string approvedOrderId = nabilResponseXML.Descendants("OrderID").First().Value;
                string approvedSessionId = nabilPaymentService.GetSesssionID(visitId);
                XDocument orderStatusRequestXML = nabilPaymentService.GetOrderStatusRequestData(approvedOrderId, approvedSessionId);
                nabilPaymentService.SaveGetOrderStatusRequest(visitId, orderStatusRequestXML.ToString(SaveOptions.DisableFormatting));
                // verifing if Payment is successful by calling Nabil API
                XDocument orderStatusResponseXML = await nabilPaymentService.GetOrderStatusNabilAsync(orderStatusRequestXML);
                nabilPaymentService.SaveGetOrderStatusResponse(visitId, orderStatusResponseXML.ToString(SaveOptions.DisableFormatting));
                _logger.LogInformation(orderStatusResponseXML.ToString());
                string responseStatusPG = orderStatusResponseXML.Descendants("Status").First().Value;
                if (responseStatusPG == "00")
                {
                    // Transaction Verification Successful
                    nabilPaymentService.CompleteTransactionNabil(visitId);
                    _logger.LogInformation($"Payment Successful of OrderId  {approvedOrderId}");
                    TempData["PaymentMethod"] = "Card";
                    TempData["PaymentStatus"] = "success";
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    // PG response Status other than 00
                    string errorMessage = nabilPaymentService.GetPGErrorMessage(responseStatusPG);
                    _logger.LogInformation(responseStatusPG);
                    TempData["PaymentMethod"] = "Card";
                    TempData["PaymentStatus"] = "failure";
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        // this end-point is hit by Compass Plus on Cancel of Payment by User
        [HttpPost]
        public async Task<IActionResult> NabilCancel()
        {
            try
            {
                var nabilPaymentService = new CompassPlusPaymentService(_unitOfWork, _environment, _configuration, _loggerFactory);
                XDocument nabilResponseXML = await nabilPaymentService.ReadBodyAsync(Request.Body);
                string visitId = nabilResponseXML.Descendants("OrderDescription").First().Value;
                _logger.LogInformation(nabilResponseXML.ToString());
                string cancelMessage = nabilResponseXML.Descendants("OrderStatus").First().Value;
                string cancelOrderId = nabilResponseXML.Descendants("OrderID").First().Value;
                nabilPaymentService.UpdateNabilPaymentStatus(visitId, NabilPaymentStatus.Cancel);
                TempData["PaymentMethod"] = "Card";
                TempData["PaymentStatus"] = "cancel";
                return RedirectToAction("Index", "Home");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Nabil Cancel Exception");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        // this end-point is hit by Compass Plus on Failure of Payment due to wrong entry of Card details, insufficent balance
        [HttpPost]
        public async Task<IActionResult> NabilDecline()
        {
            try
            {
                var nabilPaymentService = new CompassPlusPaymentService(_unitOfWork, _environment, _configuration, _loggerFactory);
                XDocument nabilResponseXML = await nabilPaymentService.ReadBodyAsync(Request.Body);
                _logger.LogInformation(nabilResponseXML.ToString());
                string visitId = nabilResponseXML.Descendants("OrderDescription").First().Value;
                nabilPaymentService.SaveResponseXMLOut(visitId, nabilResponseXML.ToString(SaveOptions.DisableFormatting));
                string declineMessage = nabilResponseXML.Descendants("ResponseDescription").First().Value;
                string declineOrderId = nabilResponseXML.Descendants("OrderID").First().Value;
                string declineSessionId = nabilPaymentService.GetSesssionID(visitId);
                XDocument orderStatusRequestXML = nabilPaymentService.GetOrderStatusRequestData(declineOrderId, declineSessionId);
                nabilPaymentService.SaveGetOrderStatusRequest(visitId, orderStatusRequestXML.ToString(SaveOptions.DisableFormatting));
                XDocument orderStatusResponseXML = await nabilPaymentService.GetOrderStatusNabilAsync(orderStatusRequestXML);
                nabilPaymentService.SaveGetOrderStatusResponse(visitId, orderStatusResponseXML.ToString(SaveOptions.DisableFormatting));
                _logger.LogInformation(orderStatusResponseXML.ToString());
                string responseStatusPG = orderStatusResponseXML.Descendants("Status").First().Value;
                if (responseStatusPG == "00")
                {
                    // verification for decline successful
                    nabilPaymentService.UpdateNabilPaymentStatus(visitId, NabilPaymentStatus.Declined);
                    _logger.LogInformation($"Decline of OrderId {declineOrderId} with decline message {declineMessage}");
                    TempData["PaymentMethod"] = "Card";
                    TempData["PaymentStatus"] = "failure";
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    string errorMessage = nabilPaymentService.GetPGErrorMessage(responseStatusPG);
                    _logger.LogInformation(responseStatusPG);
                    TempData["PaymentMethod"] = "Card";
                    TempData["PaymentStatus"] = "failure";
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
