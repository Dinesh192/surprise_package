﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Telemedicine.ServerModel;
using TestTele.ViewModel;
using Microsoft.AspNetCore.Authorization;
using System.Transactions;
using Telemedicine.DALLayer;
using System.Security.Claims;
using Microsoft.Extensions.Logging;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Telemedicine.DalLayer;

namespace TestTele.Controllers
{
    [Route("api/[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ApplicationDbContext _dbContext;
        private readonly ILogger<AccountController> _logger;
        private readonly IConfiguration _configuration;

        public AccountController(UserManager<ApplicationUser> userManager,
                                 SignInManager<ApplicationUser> signInManager,
                                 ApplicationDbContext dbContext,
                                 ILogger<AccountController> logger,
                                 IConfiguration configuration,
                                 IUnitOfWork unitOfWork)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _dbContext = dbContext;
            _logger = logger;
            _configuration = configuration;
            _unitOfWork = unitOfWork;
        }
        [HttpPost]
        [Authorize(Policy = "CanRegisterDoctor", AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> RegisterDoctor([FromBody] DoctorViewModel doctorVM)
        {
            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    if (ModelState.IsValid)
                    {
                        var user = new ApplicationUser
                        {
                            //ahile ko lagi phone no use garne
                            UserName = doctorVM.PhoneNumber,
                        };
                        var userCreatedResult = await _userManager.CreateAsync(user, doctorVM.Password);
                        if (userCreatedResult.Succeeded)
                        {
                            Doctor doctor = new Doctor
                            {
                                FirstName = doctorVM.FirstName,
                                UserName = doctorVM.UserName,
                                Address = doctorVM?.Address,
                                LastName = doctorVM?.LastName,
                                PhoneNumber = doctorVM.PhoneNumber,
                                Gender = doctorVM.Gender,
                                MiddleName = doctorVM?.MiddleName,
                                DepartmentId = doctorVM?.DepartmentId,
                                IdentityUserId = user.Id,
                                DateOfBirth = doctorVM.DateOfBirth,
                                //Password = doctorVM.Password,
                                //ConfirmPassword = doctorVM.ConfirmPassword,

                                CreatedBy = user.Id,
                                HospitalIdentifier = doctorVM.HospitalIdentifier
                            };
                            _dbContext.Doctors.Add(doctor);
                            if ((await _dbContext.SaveChangesAsync()) > 0)
                            {   // Guid id = Guid.Parse(Constants.DoctorIdentifier);
                                //    var docname = _unitOfWork.Doctors.GetGuid(id);
                                //grab the Id of the newly created doctor and assign its value as a claim to IdentityUser
                                if (!String.IsNullOrEmpty(doctorVM.HospitalIdentifier))
                                {
                                    var userClaims = new Claim[]
                              {
                                    new Claim(Constants.UserType, nameof(Doctor)),
                                    new Claim(Constants.DoctorIdentifier, doctor.DoctorId.ToString()),
                                     new Claim(Constants.Hospital,doctorVM.HospitalIdentifier)
                                  //  new Claim(Constants.Name, docname.FirstName + " " + (String.IsNullOrEmpty(docname.MiddleName) ? " " : docname.MiddleName) + " " + docname.LastName ),


                              };
                                    var userClaimsCreatedResult = await _userManager.AddClaimsAsync(user, userClaims);
                                    if (userClaimsCreatedResult.Succeeded)
                                    {
                                        scope.Complete();
                                    }
                                    else
                                    {
                                        foreach (var error in userClaimsCreatedResult.Errors)
                                        {
                                            ModelState.AddModelError(string.Empty, error.Description);
                                        }
                                        scope.Dispose();
                                        return BadRequest(ModelState);
                                    }
                                }
                                else
                                {
                                    var userClaims = new Claim[]
                                      {
                                    new Claim(Constants.UserType, nameof(Doctor)),
                                    new Claim(Constants.DoctorIdentifier, doctor.DoctorId.ToString()),
                                 //  new Claim(Constants.Name, docname.FirstName + " " + (String.IsNullOrEmpty(docname.MiddleName) ? " " : docname.MiddleName) + " " + docname.LastName ),


                                       };
                                    var userClaimsCreatedResult = await _userManager.AddClaimsAsync(user, userClaims);
                                    if (userClaimsCreatedResult.Succeeded)
                                    {
                                        scope.Complete();
                                    }
                                    else
                                    {
                                        foreach (var error in userClaimsCreatedResult.Errors)
                                        {
                                            ModelState.AddModelError(string.Empty, error.Description);
                                        }
                                        scope.Dispose();
                                        return BadRequest(ModelState);
                                    }

                                }
                                
                                
                            }

                        }
                        else
                        {
                            //  IdentityResult.Succeed == false failure while creating IdentityUser
                            foreach (var error in userCreatedResult.Errors)
                            {
                                ModelState.AddModelError(string.Empty, error.Description);
                            }
                            scope.Dispose();
                            return BadRequest(ModelState);
                        }
                    }
                    else
                    {
                        //model state not valid
                        scope.Dispose();
                        return BadRequest(ModelState);
                    }
                }
            }
            catch (Exception ex)
            {
                // Transaction not commited if any errors occurs
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            //return Created("", "User Registered");
            return Ok();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> RegisterPatient([FromBody] PatientViewModel patientVM)
        {
            try
            {
                using (var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
                {
                    if (ModelState.IsValid)
                    {
                        var user = new ApplicationUser
                        {
                            //ahile ko lagi phone no use garne
                            UserName = patientVM.PhoneNumber,
                        };
                        var userCreatedResult = await _userManager.CreateAsync(user, patientVM.Password);

                        if (userCreatedResult.Succeeded)
                        {
                            Patient patient = new Patient
                            {
                                FirstName = patientVM.FirstName,
                                //UserName = patientVM.UserName,
                                Address = patientVM?.Address,
                                LastName = patientVM?.LastName,
                                ContactNumber = patientVM.PhoneNumber,
                                Gender = patientVM.Gender,
                                MiddleName = patientVM?.MiddleName,
                                DateOfBirth = patientVM.DateOfBirth,
                                IdentityUserId = user.Id,
                                CreatedBy = user.Id,
                                 HospitalIdentifier = patientVM.HospitalIdentifier != null ? patientVM.HospitalIdentifier: "NA"
                            };
                            _dbContext.Patients.Add(patient);
                            if ((await _dbContext.SaveChangesAsync()) > 0)
                            {
                                // assign the id of the newly created Patient to IdentityUser's Claims
                                // and also set the UserType Claim
                                if (!String.IsNullOrEmpty(patientVM.HospitalIdentifier))
                                {
                                    var userClaims = new Claim[]
                                    {
                                    new Claim(Constants.PatientIdentifier,patient.PatientId.ToString()),
                                    new Claim(Constants.UserType,nameof(Patient)),
                                    new Claim(Constants.Hospital,patientVM.HospitalIdentifier)
                                    };
                                    var userClaimsCreatedResult = await _userManager.AddClaimsAsync(user, userClaims);
                                    if (userClaimsCreatedResult.Succeeded)
                                    {
                                        scope.Complete();
                                    }
                                    else
                                    {
                                        foreach (var error in userClaimsCreatedResult.Errors)
                                        {
                                            ModelState.AddModelError(string.Empty, error.Description);
                                        }
                                        scope.Dispose();
                                        return BadRequest(ModelState);
                                    }
                                }
                                else
                                {
                                    var userClaims = new Claim[]
                                    {
                                    new Claim(Constants.PatientIdentifier,patient.PatientId.ToString()),
                                    new Claim(Constants.UserType,nameof(Patient)),
                                    new Claim(Constants.Hospital,"NA")
                                    };
                                    var userClaimsCreatedResult = await _userManager.AddClaimsAsync(user, userClaims);
                                    if (userClaimsCreatedResult.Succeeded)
                                    {
                                        scope.Complete();
                                    }
                                    else
                                    {
                                        foreach (var error in userClaimsCreatedResult.Errors)
                                        {
                                            ModelState.AddModelError(string.Empty, error.Description);
                                        }
                                        scope.Dispose();
                                        return BadRequest(ModelState);
                                    }
                                }
                                //if (userClaimsCreatedResult.Succeeded)
                                //{
                                //    scope.Complete();
                                //}
                                //else
                                //{
                                //    foreach (var error in userClaimsCreatedResult.Errors)
                                //    {
                                //        ModelState.AddModelError(string.Empty, error.Description);
                                //    }
                                //    scope.Dispose();
                                //    return BadRequest(ModelState);
                                //}
                            }

                        }
                        else
                        {
                            //  IdentityResult.Succeed == false failure while creating IdentityUser
                            foreach (var error in userCreatedResult.Errors)
                            {
                                ModelState.AddModelError(string.Empty, error.Description);
                            }
                            scope.Dispose();
                            return BadRequest(ModelState);
                        }
                    }
                    else
                    {
                        //model state not valid
                        scope.Dispose();
                        return BadRequest(ModelState);
                    }
                }
            }
            catch (Exception ex)
            {
                // Transaction not commited if any errors occurs
                _logger.LogError(ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            //return Created("", "User Registered");
            return Ok();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody] LoginViewModel loginVM)
        {
            if (ModelState.IsValid)
            {
                var identityUser = await _userManager.FindByNameAsync(loginVM.PhoneNumber);
                if (identityUser != null)
                {
                    //this will sign the User in with a Cookie
                    //var result = await _signInManager.PasswordSignInAsync(loginVM.PhoneNumber, loginVM.Password, loginVM.RememberMe, lockoutOnFailure: false);

                    //this is used if we don't need to issue a cookie 
                    var result = await _signInManager.CheckPasswordSignInAsync(identityUser, loginVM.Password, lockoutOnFailure: false);
                    if (result.Succeeded)
                    {
                        List<Claim> userAdminClaims = (await _userManager.GetClaimsAsync(identityUser)).ToList();
                        if (identityUser.Email == "admin@telemedicine.com" && userAdminClaims.Count() == 0)
                        {
                            var adminClaim = new Claim[]
                                  {
                                    new Claim(Constants.UserType, "Admin"),
                                    new Claim(Constants.AdminIdentifier, identityUser.ToString()),
                                     new Claim(Constants.Hospital, "mediciti")
                                  };
                            await _userManager.AddClaimsAsync(identityUser, adminClaim);
                        }
                        if (identityUser.Email == "mikc@telemedicine.com" && userAdminClaims.Count() == 0)
                        {
                            var adminClaim = new Claim[]
                                  {
                                    new Claim(Constants.UserType, "Admin"),
                                    new Claim(Constants.AdminIdentifier, identityUser.ToString()),
                                     new Claim(Constants.Hospital, "mikc")
                                  };
                            await _userManager.AddClaimsAsync(identityUser, adminClaim);
                        }
                        if (identityUser.Email == "danphetelehealth@telemedicine.com" && userAdminClaims.Count() == 0)
                        {
                            var adminClaim = new Claim[]
                                  {
                                    new Claim(Constants.UserType, "Admin"),
                                    new Claim(Constants.AdminIdentifier, identityUser.ToString()),
                                     new Claim(Constants.Hospital, "DanpheTeleHealth")
                                  };
                            await _userManager.AddClaimsAsync(identityUser, adminClaim);
                        }
                        //create claims for the user
                        // extract existing claims from database
                        List<Claim> userClaims = (await _userManager.GetClaimsAsync(identityUser)).ToList();
                        var userType = userClaims.Find(x => x.Type == "UserType");
                        var userId = userClaims.Find(x => x.Type == "DoctorIdentifier");
                        if (userType.Value == "Doctor")
                        {
                            var doctor = _unitOfWork.Doctors.GetAll().Where(x => x.DoctorId.ToString() == userId.Value).FirstOrDefault();
                            doctor.DoctorRoomName = Guid.NewGuid();
                            _unitOfWork.Doctors.UpdateProperties(doctor);
                            _unitOfWork.SaveChanges();
                        }
                   
                        userClaims.Add(new Claim(JwtRegisteredClaimNames.UniqueName, identityUser.Id));
                        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Tokens:JwtKey"]));
                        var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                        var token = new JwtSecurityToken(
                            issuer: _configuration["Tokens:JwtIssuer"],
                            audience: _configuration["Tokens:JwtAudience"],
                            claims: userClaims,
                            expires: DateTime.UtcNow.AddMinutes(Convert.ToDouble(_configuration["Tokens:JwtValidMinutes"])),
                            signingCredentials: creds
                            );

                        var results = new
                        {
                            token = new JwtSecurityTokenHandler().WriteToken(token),
                            expiration = token.ValidTo
                        };

                        _logger.LogInformation("User logged in.");
                        return Created("", results);
                        //return Ok();
                    }
                    if (result.RequiresTwoFactor)
                    {
                        // logic for two factor
                    }
                    if (result.IsLockedOut)
                    {
                        // logic for lockout
                    }
                    if (!result.Succeeded)
                    {
                        ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                        return Unauthorized();
                    }
                    //return Ok();
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                    return Unauthorized(ModelState);
                }
            }
            return BadRequest(ModelState);
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> LogOut()
        {
            await _signInManager.SignOutAsync();
            _logger.LogInformation("User logged out.");
            return Ok();
        }

        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> ResetPassword([FromBody] UserViewModel userVM)

        {
            var identityUser = await _userManager.FindByIdAsync(userVM.UserId);
            var hashPassword = _userManager.PasswordHasher.HashPassword(identityUser, userVM.Password);
            identityUser.PasswordHash = hashPassword;
            await _userManager.UpdateAsync(identityUser);
            return Ok();
        }

        [HttpGet]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult ValidUserCheck()
        {
            return Ok();
        }
    }
}