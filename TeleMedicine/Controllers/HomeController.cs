﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Telemedicine.DalLayer;
using TestTele.Models;
using Telemedicine.ServerModel;
using Telemedicine.DALLayer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace TestTele.Controllers
{

    // [Route("api/[controller]/[action]")]
    public class HomeController : Controller
    {
        public ApplicationDbContext db;
        private readonly ILogger<HomeController> _logger;
        // DoctorCabin _doctorcabin = null;
        // WaitingRoom _waitingroom = null;
       // private int idletime = 0;

        public IConfiguration Configuration { get; }
        public IActionResult Index()
        {
            ViewData["HospitalName"] = Configuration["HospitalName"];
            ViewData["PaymentMethod"] = TempData["PaymentMethod"];
            ViewData["PaymentStatus"] = TempData["PaymentStatus"];
            return View();
        }

        public HomeController(ILogger<HomeController> logger,
         IConfiguration configuration, ApplicationDbContext context)
        {
            Configuration = configuration;
            _logger = logger;
            // _doctorcabin = doctorcabin;
            // _waitingroom = waitingroom;
            //idletime = Convert.ToInt32(configuration["IdleTime"]);
            db = context;
            //}
            ////public IActionResult GetDoctor()
            ////{
            ////   // return Json(_doctorcabin.Doctor);
            ////}
            //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
            ////public IActionResult CurrentPatients()
            ////{
            ////    this.RemoveIdle();
            ////   // return Json(_waitingroom.Patients);
            ////}
            //private void RemoveIdle()
            //{
            //    //var removepats = new List<Patients>();

            //    //foreach (var t in _waitingroom.Patients)
            //    //{
            //    //    var diffInSeconds = DateTime.Now.Subtract(t.LastUpdated).TotalSeconds;
            //    //    if (diffInSeconds > idletime)
            //    //    {
            //    //        removepats.Add(t);
            //    //    }
            //    //}
            //    //foreach (var t in removepats)
            //    //{
            //    //    _waitingroom.Patients.Remove(t);
            //    //}
            //}

            //public IActionResult ShouldIGoOut([FromBody]Patients obj)
            //{
            //    foreach (var t in _waitingroom.Patients)
            //    {
            //        if ((obj.PatientName == t.PatientName))
            //        {
            //            t.LastUpdated = DateTime.Now;
            //            if ((t.Status == (int)TeleConstants.NotInCall))
            //            {
            //                return Ok(true);
            //            }
            //        }
            //    }

            //    return Ok(false);

            //}
            //public IActionResult CanIComeIn([FromBody]Patients obj)
            //{
            //    foreach (var t in _waitingroom.Patients)
            //    {
            //        if ((obj.PatientName == t.PatientName))
            //        {
            //            t.LastUpdated = DateTime.Now;
            //            if ((t.Status == (int)TeleConstants.InActive))
            //            {
            //                return Ok(true);
            //            }
            //        }
            //    }

            //    return Ok(false);

            //}
            //public IActionResult WaitingRoom()
            //{
            //    return Json(_waitingroom);
            //}
            //public IActionResult LoginPatient([FromBody] Patients obj)
            //{
            //    if (!(getPatientbyName(obj.PatientName) is null))
            //    {
            //        return StatusCode(500, "Patient already logged in");
            //    }
            //    obj.LastUpdated = DateTime.Now;
            //    _waitingroom.Patients.Add(obj);
            //    Patient pat = new Patient();
            //    Problem cl = new Problem();
            //    pat.PatientId = obj.PatientId;
            //    pat.MailingAddress = obj.Email;
            //    pat.FirstName = obj.PatientName;
            //    pat.Gender = obj.Sex;
            //    pat.Address = obj.Address;
            //    pat.ContactNumber = obj.MobileNumber;
            //    cl.Bodyache = obj.Bodyache;
            //    cl.BreathingDifficulty = obj.BreathingDifficulty;
            //    cl.ChestPain = obj.ChestPain;
            //    cl.Cough = obj.Cough;
            //    cl.Diarrhea = obj.Diarrhea;
            //    cl.Fever = obj.Fever;
            //    cl.HeartDisease = obj.HeartDisease;
            //    cl.AnyOtherSymptoms = obj.AnyOtherSymptoms;
            //    try
            //    {

            //        db.Patients.Add(pat);
            //        db.SaveChanges();
            //        var patid = pat.PatientId;
            //        //   cl.PatientId = patid;
            //        db.Problems.Add(cl);
            //        db.SaveChanges();
            //    }
            //    catch (Exception ex)
            //    {

            //        throw ex;
            //    }

            //    if (_waitingroom.Patients != null)
            //    {
            //        if (_waitingroom.Patients.Count > 0)
            //        {
            //            //obj.PatientId = _waitingroom.Patients.Count;
            //            obj.PatientCount = _waitingroom.Patients.Count;
            //        }
            //    }
            //    //obj.PatientId = _waitingroom.Patients.Count+1;
            //    return Ok(Json(obj));
            //}
            ////public IActionResult LoginDoctor([FromBody] TestTele.Models.Doctor obj, [FromBody] string Password)
            ////{
            ////    if (obj.Password == "test123")
            ////    {
            ////        _doctorcabin.Doctor = obj;
            ////        return Ok(Json(obj));
            ////    }
            ////    else
            ////    {
            ////        return StatusCode(401);
            ////    }

            ////}

            //private Patients getPatientbyName(string PatName)
            //{
            //    foreach (var t in _waitingroom.Patients)
            //    {
            //        if (PatName == t.PatientName)
            //        {
            //            return t;
            //        }
            //    }
            //    return null;
            //}
            //public IActionResult WriteMedication([FromBody]Patients obj)
            //{
            //    Patients p = getPatientbyName(_doctorcabin.Patient.PatientName);
            //    if (p.Status == (int)TeleConstants.NotInCall)
            //    {
            //        p.Medication = obj.Medication;
            //        return Ok(true);
            //    }
            //    else
            //    {
            //        return Ok(true);
            //    }
            //}
            //public IActionResult TakeFinalReport([FromBody]Patients p1)
            //{
            //    Patients p = getPatientbyName(p1.PatientName);
            //    if (p is null) { return Ok(null); }
            //    if (p.Status == (int)TeleConstants.NotInCall)
            //    {
            //        _waitingroom.Patients.Remove(p);
            //        return Ok(p);

            //    }
            //    else
            //    {
            //        return Ok(null);
            //    }
            //}
            //public IActionResult PatientAttended([FromBody]Patients obj)
            //{
            //    Patients p = getPatientbyName(obj.PatientName);
            //    if (p is null)
            //    {
            //        return StatusCode(500);
            //    }
            //    else
            //    {
            //        _doctorcabin.Patient = new Patients();
            //        p.Status = (int)TeleConstants.NotInCall;
            //        p.Medication = obj.Medication;
            //        return Ok(p);
            //    }
            //}
            //public IActionResult CallPatient([FromBody]Patients obj)
            //{
            //    Patients p = getPatientbyName(obj.PatientName);
            //    if (p is null)
            //    {
            //        return StatusCode(500);
            //    }
            //    else
            //    {
            //        p.Status = (int)TeleConstants.InCall;
            //        p.LastUpdated = DateTime.Now;
            //        _doctorcabin.Patient = p;
            //        return Ok(p);
            //    }
            //}


            //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
            //public IActionResult Error()
            //{
            //    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
            //}
        }
    }
}


