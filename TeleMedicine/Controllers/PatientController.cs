﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telemedicine.DalLayer;
using TestTele.ViewModel;
using AutoMapper;
using Telemedicine.ServerModel;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.CodeAnalysis;
using Telemedicine.ServerModel.Interfaces;
using Newtonsoft.Json;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using System.Net.Http.Headers;
using System.Reflection.Metadata;
using System.Runtime.InteropServices.WindowsRuntime;
using Microsoft.AspNetCore.Identity;
using System.Globalization;
using System.Security.Claims;

namespace TestTele.Controllers
{
    //[Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PatientController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private IWebHostEnvironment _hostingEnvironment;
        private readonly IAuthorizationService _authorizationService;
        private readonly UserManager<ApplicationUser> _userManager;

        public PatientController(IUnitOfWork unitOfWork, IMapper mapper, IWebHostEnvironment hostingEnvironment, IAuthorizationService authorizationService, UserManager<ApplicationUser> userManager)
        {
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
            _hostingEnvironment = hostingEnvironment;
            _authorizationService = authorizationService;
            _userManager = userManager;
        }

        //List<Visit> visit = new List<Visit>();
        //List<Patient> pat = new List<Patient>();

        [HttpGet("{id}")]
        //[Produces(typeof(List<PatientViewModel>))]
        public async Task<ActionResult> ListPatientByProviderId(Guid id)
        {
            try
            {
                if (id != Guid.Empty)
                {
                    var currentDate = DateTime.Now;
                    var patients = await _unitOfWork.Patients.GetAllAsync();
                    var visits = _unitOfWork.Visits.GetAll().ToList();
                    var docs = _unitOfWork.Doctors.GetAll().ToList();
                    var probs = _unitOfWork.Problems.GetAll().ToList();
                    var schedules = _unitOfWork.DoctorSchedulings.GetAll().ToList();
                    var patlist = (from vst in visits
                                       //   join doc in docs on vst.ProviderId equals doc.DoctorId 
                                   join pat in patients on vst.PatientId equals pat.PatientId
                                   join prob in probs on vst.VisitId equals prob.VisitId
                                   join schedule in schedules on vst.SchedulingId equals schedule.SchedulingId
                                   //where vst.ProviderId == id && vst.IsActive == true && (vst.Status == "initiated" || vst.Status == "ongoing") && (DateTime.Now.Subtract(vst.LastUpdateTime).TotalSeconds <= 20)/*((DateTime.Now.Subtract(vst.LastUpdateTime).TotalSeconds) <= (schedule.StartTime.Subtract(schedule.EndTime).TotalSeconds))*/
                                   where vst.ProviderId == id && vst.IsActive == true && (vst.Status == "initiated" || vst.Status == "ongoing") && (vst.IsConversationCompleted == false)
                                   select new
                                   {
                                       vst.PatientId,
                                       vst.VisitId,
                                       pat.IdentityUserId,
                                       vst.VisitDate,
                                       pat.ContactNumber,
                                       pat.Address,
                                       PatientName = pat.FirstName + " " + (String.IsNullOrEmpty(pat.MiddleName) ? " " : pat.MiddleName) + " " + pat.LastName,
                                       pat.Gender,
                                       prob.Cough,
                                       prob.AnyOtherSymptoms,
                                       prob.Bodyache,
                                       prob.Cancer,
                                       prob.ChestPain,
                                       prob.Copd,
                                       prob.Diabetes,
                                       prob.Diarrhea,
                                       prob.Exposure,
                                       prob.Fever,
                                       prob.HeartDisease,
                                       prob.HighBloodPressure,
                                       prob.OtherPertientInformation,
                                       prob.RecentTravel,
                                       prob.SoreThroat,
                                       prob.Tiredness,
                                       prob.Transplant,
                                       prob.BreathingDifficulty,
                                       vst.Status,
                                       vst.IsActive,
                                       vst.BookingTime
                                   }).Where(x => x.VisitDate.Year == currentDate.Year && x.VisitDate.Month == currentDate.Month && x.VisitDate.Day == currentDate.Day).OrderBy(x => x.BookingTime).ToList();

                    return Ok(patlist);
                }
                else
                {
                    return Ok();
                }
            }
            catch
            {
                throw;
            }

        }
        
        [HttpGet("{id}")]
        //[Produces(typeof(List<PatientViewModel>))]
        public async Task<ActionResult> getFinalReportData(Guid id)
        {
            var patients = await _unitOfWork.Patients.GetAllAsync();
            var visits = _unitOfWork.Visits.GetAll().ToList();
            var docs = _unitOfWork.Doctors.GetAll().ToList();
            var probs = _unitOfWork.Problems.GetAll().ToList();
            var patlist = (from vst in visits
                           join doc in docs on vst.ProviderId equals doc.DoctorId
                           join pat in patients on vst.PatientId equals pat.PatientId
                           join prob in probs on vst.VisitId equals prob.VisitId
                           where vst.VisitId == id
                           select new
                           {
                               vst.TreatmentAdvice,
                               vst.Medication,
                               vst.FollowUp,
                               vst.PatientId,
                               vst.VisitId,
                               pat.ContactNumber,
                               pat.Address,
                               pat.MailingAddress,
                               pat.DateOfBirth,
                               doc.NMC,
                               PatientName = pat.FirstName + " " + (String.IsNullOrEmpty(pat.MiddleName) ? " " : pat.MiddleName) + " " + pat.LastName,
                               pat.Gender,
                               DoctorName = doc.FirstName + " " + (String.IsNullOrEmpty(doc.MiddleName) ? " " : doc.MiddleName) + " " + doc.LastName,
                               prob.Cough,
                               prob.AnyOtherSymptoms,
                               prob.Bodyache,
                               prob.Cancer,
                               prob.ChestPain,
                               prob.Copd,
                               prob.Diabetes,
                               prob.Diarrhea,
                               prob.Exposure,
                               prob.Fever,
                               prob.HeartDisease,
                               prob.HighBloodPressure,
                               prob.OtherPertientInformation,
                               prob.RecentTravel,
                               prob.SoreThroat,
                               prob.Tiredness,
                               prob.Transplant,
                               prob.BreathingDifficulty,
                               vst.Status
                           }).ToList();

            return Ok(patlist);

        }

        [HttpGet("{id}")]
        public async Task<ActionResult> ViewPrescription(Guid id)
        {
            //Check done to prevent Insecure Direct Object References(IDOR) vulnerability
            if (!(await _authorizationService.AuthorizeAsync(User, id, "IsPatientSame")).Succeeded)
            {
                return Forbid();
            }

            var visits = await _unitOfWork.Visits.GetAllAsync();
            var docs = _unitOfWork.Doctors.GetAll().ToList();
            var probs = _unitOfWork.Problems.GetAll().ToList();
            var hospital = _unitOfWork.Hospitals.GetAll().ToList();
            var department = _unitOfWork.Departments.GetAll().ToList();
            var patlist = (from vst in visits
                           join doc in docs on vst.ProviderId equals doc.DoctorId
                           join prob in probs on vst.VisitId equals prob.VisitId
                           join hosp in hospital on vst.HospitalId equals hosp.HospitalId
                           join dept in department on vst.DepartmentId equals dept.DepartmentId
                           where vst.PatientId == id
                           select new
                           {
                               vst.PatientId,
                               DoctorName = doc.FirstName + " " + (String.IsNullOrEmpty(doc.MiddleName) ? " " : doc.MiddleName) + " " + doc.LastName,
                               vst.VisitId,
                               vst.VisitDate,
                               vst.VisitEndTime,
                               vst.VisitDate.Year,
                               vst.VisitDate.Month,
                               vst.VisitDate.Day,
                               GoToWaitingRoom = false,
                               vst.PaymentStatus,
                               vst.VisitStartTime,
                               vst.VisitType,
                               vst.Status,
                               dept.DepartmentName,
                               hosp.HospitalName,
                               prob.Cough,
                               prob.AnyOtherSymptoms,
                               prob.Bodyache,
                               prob.Cancer,
                               prob.ChestPain,
                               prob.Copd,
                               prob.Diabetes,
                               prob.Diarrhea,
                               prob.Exposure,
                               prob.Fever,
                               prob.HeartDisease,
                               prob.HighBloodPressure,
                               prob.OtherPertientInformation,
                               prob.RecentTravel,
                               prob.SoreThroat,
                               prob.Tiredness,
                               prob.Transplant,
                               prob.BreathingDifficulty,
                               vst.TreatmentAdvice,
                               vst.Medication,
                               vst.FollowUp,
                               vst.ProviderId,
                               PatientFile = _unitOfWork.PatientFilesUpload.GetAll().Where(x => x.VisitId == vst.VisitId && x.FileKind == "Prescription").ToList()
                           }).OrderByDescending(x => x.VisitDate).ToList();

            return Ok(patlist);

        }


        [HttpGet("{id}")]
        //[Produces(typeof(List<PatientViewModel>))]
        public async Task<ActionResult> GetPatientDocuments(Guid id)
        {
            var visits = await _unitOfWork.Visits.GetAllAsync();
            var file = _unitOfWork.PatientFilesUpload.GetAll().Where(x=>x.VisitId==id).ToList();


            var patdoc = (from vst in visits
                          join fl in file on vst.VisitId equals fl.VisitId
                          where vst.VisitId == id 
                          select new
                          {
                              vst.VisitDate,
                              fl.FileType,
                              fl.FileName,
                              fl.PatientFileId,
                              fl.FilePath,
                              fl.FileExtention,
                              fl.Title
                          }).ToList();

            return Ok(patdoc);

        }


        [HttpGet("{id}")]
        public IActionResult GetDetails(Guid id)
        {
            try
            {

                var _getPatient = _unitOfWork.Patients.GetGuid(id);
                var patient = _mapper.Map<Patient, PatientViewModel>(_getPatient);
                return Ok(patient);
            }
            catch
            {
                throw;
            }
        }


        [HttpGet("{id}")]
        public IActionResult PatientStatus(Guid id)
        {
            try
            {
                if (id != Guid.Empty)
                {
                    var visits = _unitOfWork.Visits.GetAll().ToList();
                    var result = (from vst in visits
                                  where vst.VisitId == id
                                  select new
                                  {
                                      vst.VisitId,
                                      vst.Status,
                                      vst.IsActive
                                  });
                    //Visit visit = (from i in visits
                    //               where i.VisitId == id
                    //               select i).First();
                    //visit.LastUpdateTime = DateTime.Now;
                    //_unitOfWork.Visits.Update(visit);
                    //_unitOfWork.SaveChanges();
                    return Ok(result);
                }
                else
                {
                    return Ok();
                }
            }
            catch
            {
                throw;
            }
        }

        
      

        [HttpGet("{id}")]
        public IActionResult PatientStatusByPatId(Guid id)
        {
            try
            {
                var currentDate = DateTime.Now;
                var visits = _unitOfWork.Visits.GetAll().ToList();
                var pats = _unitOfWork.Patients.GetAll().ToList();
                var vstQuery = _unitOfWork.Visits.GetAll().Where(x => x.VisitId == id).FirstOrDefault();
                //var result = (from vst in visits
                //              join pat in pats on vst.PatientId equals pat.PatientId
                //              where vst.PatientId == id
                //              select new
                //              {
                //                //  PatientName = pat.FirstName + " " + (String.IsNullOrEmpty(pat.MiddleName) ? " " : pat.MiddleName) + " " + pat.LastName,
                //                  vst.VisitId,
                //                  vst.VisitDate,
                //                  vst.Status,
                //                  vst.IsActive

                //              }).Where(x => x.VisitDate.Year == currentDate.Year && x.VisitDate.Month == currentDate.Month && x.VisitDate.Day <= currentDate.Day).OrderByDescending(s => s.VisitDate).FirstOrDefault();

                //Visit visit = (from i in visits
                //               where i.VisitId == result.VisitId
                //               select i).First();
                vstQuery.LastUpdateTime = DateTime.Now;
                vstQuery.IsActive = true;
                _unitOfWork.Visits.Update(vstQuery);
                _unitOfWork.SaveChanges();
                //  var Status = _getPatient.IsActive;
                return Ok(vstQuery);
            }
            catch
            {
                throw;
            }
        }
        [HttpGet("{id}")]
        public IActionResult PatientStatusByPatientUrl(Guid id)
        {
            try
            {
                var currentDate = DateTime.Now;
                var visits = _unitOfWork.Visits.GetAll().ToList();
                var pats = _unitOfWork.Patients.GetAll().ToList();
                var result = (from vst in visits
                              join pat in pats on vst.PatientId equals pat.PatientId
                              where vst.VisitId == id 
                              select new
                              {
                                  //  PatientName = pat.FirstName + " " + (String.IsNullOrEmpty(pat.MiddleName) ? " " : pat.MiddleName) + " " + pat.LastName,
                                  vst.VisitId,
                                  vst.VisitDate,
                                  vst.Status,
                                  vst.IsActive

                              }).Where(x => x.VisitDate.Year == currentDate.Year && x.VisitDate.Month == currentDate.Month && x.VisitDate.Day <= currentDate.Day).OrderByDescending(s => s.VisitDate).FirstOrDefault();

                //Visit visit = (from i in visits
                //               where i.VisitId == result.VisitId
                //               select i).First();
                //visit.LastUpdateTime = DateTime.Now;
                //visit.IsActive = true;
                //visit.Status = "ongoing";
                //_unitOfWork.Visits.Update(visit);
                //_unitOfWork.SaveChanges();
                //  var Status = _getPatient.IsActive;
                return Ok(result);
            }
            catch
            {
                throw;
            }
        }
        [HttpGet("{id}")]
        public IActionResult patientStatusChange(Guid id)
        {
            try
            {
               
                var visits = _unitOfWork.Visits.GetAll().ToList();
               
                Visit visit = (from i in visits
                               where i.VisitId == id
                               select i).First();
                visit.Status = "initiated";
                visit.IsActive = false;
                visit.IsConversationCompleted = false;
                _unitOfWork.Visits.Update(visit);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch
            {
                throw;
            }
        }
        [HttpGet("{id}")]
        public IActionResult PatientStatusChangefomDoc(Guid id)
        {
            try
            {

                var visits = _unitOfWork.Visits.GetAll().ToList();

                Visit visit = (from i in visits
                               where i.VisitId == id
                               select i).First();
                if ((visit.Status == "initiated" || visit.Status == "ongoing" ) && visit.IsActive == true)
                {
                    visit.Status = "initiated";
                    visit.IsActive = true;
                }
               else
                {
                    visit.Status = "initiated";
                    visit.IsActive = false;
                }
                visit.IsConversationCompleted = false;
                // visit.LastUpdateTime = DateTime.Now;
                _unitOfWork.Visits.Update(visit);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch
            {
                throw;
            }
        }
        [HttpGet("{id}")]
        //[Produces(typeof(List<PatientViewModel>))]
        public async Task<ActionResult> PatientHistory(Guid id)
        {
            //Check done to prevent Insecure Direct Object References(IDOR) vulnerability
            //if (!(await _authorizationService.AuthorizeAsync(User, id, "IsPatientSame")).Succeeded)
            //{
            //    return Forbid();
            //}

            var patient = await _unitOfWork.Patients.GetAllAsync();
            var visits = await _unitOfWork.Visits.GetAllAsync();
            var docs = _unitOfWork.Doctors.GetAll().ToList();
            var probs = _unitOfWork.Problems.GetAll().ToList();
            var hospital = _unitOfWork.Hospitals.GetAll().ToList();
           var docSchedule = _unitOfWork.DoctorSchedulings.GetAll().ToList();
            var hospdepartment = _unitOfWork.HospitalDoctorMaps.GetAll().ToList();
            var department = _unitOfWork.Departments.GetAll().ToList();
            var consultations = _unitOfWork.Consultations.GetAll().ToList();
            var patlist = (from vst in visits
                           join pat in patient on vst.PatientId equals pat.PatientId
                           join doc in docs on vst.ProviderId equals doc.DoctorId
                           join prob in probs on vst.VisitId equals prob.VisitId
                           join hosp in hospital on vst.HospitalId equals hosp.HospitalId
                           join docsch in docSchedule on vst.SchedulingId equals docsch.SchedulingId
                           join dept in department on vst.DepartmentId equals dept.DepartmentId
                           join consultation in consultations on vst.ConsultationId equals consultation.ConsultationId
                           where vst.PatientId == id 
                           select new
                           {
                               vst.PatientId,
                               PatientName = pat.FirstName + " " + (String.IsNullOrEmpty(pat.MiddleName) ? " " : pat.MiddleName) + " " + pat.LastName,
                               DoctorName = doc.FirstName + " " + (String.IsNullOrEmpty(doc.MiddleName) ? " " : doc.MiddleName) + " " + doc.LastName,
                               vst.VisitId,
                               vst.ProviderId,
                               vst.HospitalId,
                               vst.VisitDate,
                               doc.NMC,
                               DocStartTime = DateTime.Parse(docsch.StartTime.ToString()).ToString("hh:mm tt", CultureInfo.GetCultureInfo("en-US")),
                               DocEndTime = DateTime.Parse(docsch.EndTime.ToString()).ToString("hh:mm tt", CultureInfo.GetCultureInfo("en-US")),
                               vst.VisitEndTime,
                               vst.BookingTime,
                               vst.VisitDate.Year,
                               vst.VisitDate.Month,
                               vst.SchedulingId,
                               vst.VisitDate.Day,
                               GoToWaitingRoom = false,
                               AccessPermission = false,
                               EntryStatus = "",
                               vst.PaymentStatus,
                               vst.VisitStartTime,
                               vst.VisitType,
                               vst.Status,
                               vst.IsActive,
                               dept.DepartmentName,
                               hosp.HospitalName,
                               prob.Cough,
                               prob.AnyOtherSymptoms,
                               prob.Bodyache,
                               prob.Cancer,
                               prob.ChestPain,
                               prob.Copd,
                               prob.Diabetes,
                               prob.Diarrhea,
                               prob.Exposure,
                               prob.Fever,
                               prob.HeartDisease,
                               prob.HighBloodPressure,
                               prob.OtherPertientInformation,
                               prob.RecentTravel,
                               prob.SoreThroat,
                               prob.Tiredness,
                               prob.Transplant,
                               prob.BreathingDifficulty,
                               consultation.Charge,
                               vst.TreatmentAdvice,
                               vst.Medication,
                               vst.FollowUp,
                               vst.IsConversationCompleted,
                           }).OrderByDescending(x => x.VisitDate).ToList();

            return Ok(patlist);

        }



        [HttpGet("{id}")]
        public IActionResult getDetailsByVisitId(Guid id)
        {
            try
            {
                if (id != Guid.Empty)
                {
                    var _getPatient = _unitOfWork.Visits.GetGuid(id);
                    //var visits = _unitOfWork.Visits.GetAll().ToList();
                    //Visit visit = (from i in visits
                    //               where i.VisitId == id
                    //               select i).First();
                    //_getPatient.LastUpdateTime = DateTime.Now;
                    //_unitOfWork.Visits.Update(_getPatient);
                    //_unitOfWork.SaveChanges();
                    return Ok(_getPatient);
                }
                else
                {
                    return Ok();
                }
            }
            catch
            {
                throw;
            }
        }

        [HttpPost, DisableRequestSizeLimit]
        public ActionResult UploadFile()
        {
            try
            {
                var file = Request.Form.Files[0];
                var fl = new PatientFilesUpload();
                string folderName = "Upload";
                string webRootPath = _hostingEnvironment.WebRootPath;
                string newPath = Path.Combine(webRootPath, folderName);
                if (!Directory.Exists(newPath))
                {
                    Directory.CreateDirectory(newPath);
                }
                if (file.Length > 0)
                {
                    string fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    fl.FileName = fileName;
                    string fullPath = Path.Combine(newPath, fileName);
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }

                }
                return Json("Upload Successful.");
            }
            catch (System.Exception ex)
            {
                return Json("Upload Failed: " + ex.Message);
            }
        }

        [HttpPost("{schIntid}/{schId}")]
        [Produces(typeof(PatientViewModel))]
        public IActionResult PostProblem([FromBody]VisitViewModel _visitVM, Guid schIntid, Guid schId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var problemVM = new ProblemViewModel();
                var _problem = _mapper.Map<ProblemViewModel, ProblemViewModel>(_visitVM.Problem, problemVM);
                _visitVM.IsActive = false;
                var visit = _mapper.Map<Visit>(_visitVM);
                visit.VisitId = Guid.NewGuid();
                visit.SchedulingId = schId;
                visit.LastUpdateTime = DateTime.Now;
                visit.HospitalId = _visitVM.HospitalId;
                visit.ConsultationId = _unitOfWork.Consultations
                                                 .GetSingleOrDefault(x => x.Description == "500 wala consultation")
                                                 .ConsultationId;
                visit.HospitalDoctorMapId = _unitOfWork.HospitalDoctorMaps
                                            .GetSingleOrDefault(x => x.DepartmentId == visit.DepartmentId
                                            && x.DoctorId == visit.ProviderId && x.HospitalId == visit.HospitalId).HospitalDoctorMapId;
                _unitOfWork.Visits.Add(visit);
                var _schedulingIntervalbyId = _unitOfWork.ScheduleInterval.GetAll().ToList();
                ScheduleInterval schInt = (from i in _schedulingIntervalbyId
                                        where i.ScheduleIntervalId == schIntid
                                           select i).First();
                schInt.IsBooked = true;
                schInt.VisitId = visit.VisitId;
                _unitOfWork.ScheduleInterval.Update(schInt);

                _problem.ProblemId = Guid.NewGuid();
                _problem.VisitId = visit.VisitId;
                var problem = _mapper.Map<Problem>(_problem);
                _unitOfWork.Problems.Add(problem);

                //var hospitalIdentifier = _unitOfWork.Hospitals.GetAll().Where(x => x.HospitalId == _visitVM.HospitalId).Select(s => s.HospitalIdentifier).FirstOrDefault();
                //var patient = _unitOfWork.Patients.GetAll().Where(x => x.PatientId == _visitVM.PatientId).FirstOrDefault();
                //patient.HospitalIdentifier = hospitalIdentifier;
                //_unitOfWork.Patients.Update(patient);

                

                //var hospitalName = User.FindFirst("HospitalIdentifier").Value;

                if (_visitVM.PatientFiles.Count > 0)
                {
                    foreach (var item in _visitVM.PatientFiles)
                    {
                        if (item.FilePath != "" && item.FileType != "")
                        {
                            var uploadfile = new PatientFilesUpload();
                            uploadfile.Title = item.Title;
                            uploadfile.PatientId = _visitVM.PatientId;
                            uploadfile.VisitId = visit.VisitId;
                            uploadfile.Comment = item.Comment;
                            uploadfile.PatientFileId = Guid.NewGuid();
                            uploadfile.FilePath = "../Upload/" + item.FilePath.Substring(item.FilePath.LastIndexOf('\\') + 1); ;
                            uploadfile.FileName = item.FilePath.Substring(item.FilePath.LastIndexOf('\\') + 1);
                            uploadfile.FileExtention = item.FilePath.Substring(item.FilePath.LastIndexOf('.') + 1);
                            uploadfile.FileType = item.FileType;
                            uploadfile.FileKind = "Patfile";
                            _unitOfWork.PatientFilesUpload.Add(uploadfile);
                        }

                    }

                }
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK, visit.VisitId));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPut("{id}")]
        [Produces(typeof(PatientViewModel))]
        public async Task<ActionResult> UpdatePatient([FromBody] PatientViewModel _patientVM)
        {
            try
            {
                var hospitalName = User.FindFirst("HospitalIdentifier").Value;
               
                //Check done to prevent Insecure Direct Object References(IDOR) vulnerability
                //if (!(await _authorizationService.AuthorizeAsync(User, _patientVM.PatientId, policyName: "IsPatientSame")).Succeeded)
                //{
                //    return Forbid();
                //}
                if (!String.IsNullOrEmpty(_patientVM.AdminId))
                {
                    if (!(await _authorizationService.AuthorizeAsync(User, _patientVM.AdminId, policyName: "CanRegisterDoctor")).Succeeded)
                    {
                        return Forbid();
                    }
                }
                else
                {
                    if (!(await _authorizationService.AuthorizeAsync(User, _patientVM.PatientId, policyName: "IsPatientSame")).Succeeded)
                    {
                        return Forbid();
                    }
                }

                var patient = _mapper.Map<Patient>(_patientVM);
                //patient.HospitalIdentifier = hospitalName;
                _unitOfWork.Patients.UpdateProperties(patient, p => p.FirstName, p => p.DateOfBirth, p => p.LastName, p => p.MiddleName
                , p => p.MailingAddress, p => p.Gender, p => p.ContactNumber);
                //_unitOfWork.Patients.Update(patient);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }



        [HttpPut("{id}")]
        [Produces(typeof(VisitViewModel))]
        public ActionResult UpdateTreatmentAdvice([FromBody] VisitViewModel _visitVM)
        {
            try
            {
                var visit = _mapper.Map<Visit>(_visitVM);
                visit.IsConversationCompleted = true;
                visit.VisitEndTime = DateTime.Now;
                _unitOfWork.Visits.Update(visit);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }

        [HttpPut("{id}")]
        [Produces(typeof(VisitViewModel))]
        public ActionResult UpdateStatus([FromBody] VisitViewModel _visitVM)
        {
            try
            {
                _visitVM.LastUpdateTime = DateTime.Now;
                var visit = _mapper.Map<Visit>(_visitVM);
                _unitOfWork.Visits.Update(visit);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }

        [HttpPut("{id}")]
        [Produces(typeof(VisitViewModel))]
        public ActionResult UpdatePaidStatus([FromBody]  Guid id)
        {
            var _visit = _unitOfWork.Visits.Find(x => x.VisitId == id).FirstOrDefault();
            _visit.PaymentStatus = "paid";
            //_visit.IsActive = true;
            try
            {
                var visit = _mapper.Map<Visit>(_visit);
                _unitOfWork.Visits.Update(visit);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }

        [HttpPut("{id}")]
        [Produces(typeof(VisitViewModel))]
        public ActionResult UpdatePaymentStatus([FromBody] VisitViewModel _visitVM, Guid id)
        {
          
            try
            {
                var visitdata = _mapper.Map<Visit>(_visitVM);
                var visits = _unitOfWork.Visits.GetAll().ToList();

                Visit visit = (from i in visits
                               where i.VisitId == id
                                           select i).First();
                visit.PaymentStatus = visitdata.PaymentStatus;
                _unitOfWork.Visits.Update(visit);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        [HttpPut("{id}")]
        [Produces(typeof(VisitViewModel))]
        public ActionResult CancelBooking([FromBody] VisitViewModel _visitVM, Guid id)
        {

            try
            {
                var visitdata = _mapper.Map<Visit>(_visitVM);
                var visits = _unitOfWork.Visits.GetAll().ToList();

                Visit visit = (from i in visits
                               where i.VisitId == id
                               select i).First();
                visit.Status = "cancelled";
                visit.BookingTime = null;
                visit.SchedulingId = null;
                visit.IsActive = false;
                _unitOfWork.Visits.Update(visit);
                var _schedulingIntervalbyId = _unitOfWork.ScheduleInterval.GetAll().ToList();
                var index = visitdata.BookingTime.IndexOf("-");
               // var temp = visitdata.BookingTime.Substring(visitdata.BookingTime.Length - index);
                var temp1 = visitdata.BookingTime.Remove(index - 1);
                ScheduleInterval schInt = (from i in _schedulingIntervalbyId
                                           where i.SchedulingId == visitdata.SchedulingId && i.StartTime == temp1
                                           select i).First();
                schInt.IsBooked = false;
                schInt.VisitId = null;
                _unitOfWork.ScheduleInterval.Update(schInt);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }

        [HttpDelete("{id}")]
        [Produces(typeof(PatientViewModel))]
        public void Delete(Guid id)
        {
            //Check done to prevent Insecure Direct Object References(IDOR) vulnerability
            //if (!(await _authorizationService.AuthorizeAsync(User, id, policyName: "IsPatientSame")).Succeeded)
            //{
            //    return;
            //}
            var _patientToDelete = _unitOfWork.Patients.GetGuid(id);
            try
            {
                _unitOfWork.Patients.Remove(_patientToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }

        [HttpGet("{id}")]
        public IActionResult PaymentChargeByVisitId(Guid id)
        {
            try
            {
                var _getMapCharge = _unitOfWork.Visits.FindByIncluding(v => v.VisitId == id, v => v.HospitalDoctorMap)
                                     .Select(v => v.HospitalDoctorMap.Charge)
                                     .FirstOrDefault();
                return Ok(_getMapCharge);
            }
            catch
            {
                throw;
            }
        }
        [HttpGet]
        [Produces(typeof(List<PatientViewModel>))]
        public IActionResult AdminPatient()
        {
            var hospitalName = User.FindFirst("HospitalIdentifier").Value;
           
            var patients = _unitOfWork.Patients.GetAll().ToList();
            if (!string.IsNullOrEmpty(hospitalName))
            {
                patients = _unitOfWork.Patients.GetAll().Where(x=>x.HospitalIdentifier == hospitalName || x.HospitalIdentifier == "NA").ToList();
            }
            else
            {
                //patients = _unitOfWork.Patients.GetAll().Where(x=>string.IsNullOrEmpty(x.HospitalIdentifier)).ToList();
                patients = _unitOfWork.Patients.GetAll().ToList();  /*for Super Admin*/
            }
            var patlist = (from pat in patients

                           select new
                           {
                               pat.PatientId,
                               pat.ContactNumber,
                               PatientName = pat.FirstName + " " + (String.IsNullOrEmpty(pat.MiddleName) ? " " : pat.MiddleName) + " " + pat.LastName,
                               pat.Gender,
                               pat.DateOfBirth,

                           }).ToList();

            return Ok(patients);
        }
    }
}
