﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Telemedicine.DalLayer;
using Telemedicine.ServerModel;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.Logging;
using TestTele.ViewModel;

namespace TestTele.Controllers
{
    [Route("api/[controller]/[action]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class DoctorController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private IWebHostEnvironment _hostingEnvironment;
        private readonly IAuthorizationService _authorizationService;
        private readonly ILogger<DoctorController> _logger;
        public DoctorController(IUnitOfWork unitOfWork,
                                IMapper mapper,
                                IWebHostEnvironment hostingEnvironment,
                                IAuthorizationService authorizationService,
                                ILogger<DoctorController> logger)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _hostingEnvironment = hostingEnvironment;
            _authorizationService = authorizationService;
            _logger = logger;
        }

        [HttpGet]
        [Produces(typeof(List<DepartmentViewModel>))]
        public IActionResult GetDeptList()
        {
            var departments = _unitOfWork.Departments.GetAll();
            var departmentsVM = _mapper.Map<List<Department>, List<DepartmentViewModel>>(departments.ToList());

            return Ok(departmentsVM);

        }
        [HttpGet("{id}")]
        
        public IActionResult GetDocName(Guid id)
        {
            var doctor = _unitOfWork.Doctors.GetGuid(id);

            return Ok(doctor);

        }


        [HttpGet("{id}")]
        public async Task<ActionResult> GetHosDocList(Guid id)
        {
            var hosdocmap = await _unitOfWork.HospitalDoctorMaps.GetAllAsync();
            var docs = _unitOfWork.Doctors.GetAll();
            var hospdocmaps = _unitOfWork.HospitalDoctorMaps.GetAll().Where(x => x.IsActive == true && x.HospitalId==id).ToList();
            //var doctormap = (from hospdocmap in hospdocmaps  
            //                 join doc in docs on doc.DoctorId equals hospdocmap.DoctorId
            //                 select new
            //                 {
            //                     DoctorName = doc.FirstName + " " + (String.IsNullOrEmpty(doc.MiddleName) ? " " : doc.MiddleName) + " " + doc.LastName,
            //                     doc.DoctorId,
            //                     doc.DepartmentId,
            //                     doc.HospitalId,
            //                     doc.FilePath,
            //                     doc.PhoneNumber,
            //                     doc.NMC,
            //                     doc.LongSignature,
            //                     doc.HospitalIdentifier
            //                 }).ToList();
            var qualification = _unitOfWork.Qualification.GetAll();
            var hosdocVM = _mapper.Map<List<HospitalDoctorMap>, List<HospitalDoctorMapViewModel>>(hosdocmap.ToList());
            var hosdoclist = (from hosdoc in hospdocmaps
                              join doc in docs on hosdoc.DoctorId equals doc.DoctorId
                              //where hosdoc.HospitalId == id
                              select new
                              {
                                  DoctorName= doc.FirstName + " " + (String.IsNullOrEmpty(doc.MiddleName) ? " " : doc.MiddleName) + " " + doc.LastName,
                                  doc.DoctorId,
                                  hosdoc.DepartmentId,
                                  hosdoc.HospitalId,
                                  hosdoc.HospitalDoctorMapId,
                                  doc.FilePath,
                                  doc.PhoneNumber,
                                  doc.NMC,
                                  doc.LongSignature
                              }).ToList().GroupBy(a => a.DoctorId).Select(g => new
                              {
                                  DoctorId = g.Key,
                                  DoctorName = g.Select(a => a.DoctorName).FirstOrDefault(),
                                  DepartmentList = hosdocmap.Where(x => x.DoctorId == g.Key && x.HospitalId == id).ToList(),
                                  //DoctorId = g.Select(a=>a.DoctorId).FirstOrDefault(),
                                  DepartmentId = hosdocmap.Where(x => x.HospitalId == id).ToList(),
                                  HospitalId = g.Select(a => a.HospitalId).FirstOrDefault(),
                                  HospitalDoctorMapId = g.Select(a => a.HospitalDoctorMapId).FirstOrDefault(),
                                  FilePath = g.Select(a => a.FilePath).FirstOrDefault(),
                                  NMC = g.Select(a => a.NMC).FirstOrDefault(),
                                  PhoneNumber = g.Select(a => a.PhoneNumber).FirstOrDefault(),
                                  LongSignature = g.Select(a => a.LongSignature).FirstOrDefault()

                              }).ToList();

            return Ok(hosdoclist);
        }

        [HttpGet]
        [Produces(typeof(List<DoctorViewModel>))]
        public IActionResult GetList()
        {
            var hospitalName = User.FindFirst("HospitalIdentifier").Value;

            var doctors = _unitOfWork.Doctors.GetAll().ToList();
            var hospdocmaps = _unitOfWork.HospitalDoctorMaps.GetAll().ToList();
           var doctormap = (from doc in doctors
                       join hospdoc in hospdocmaps on doc.DoctorId equals hospdoc.DoctorId  into hospdocmap
                       from hosdoc in  hospdocmap.DefaultIfEmpty()
                       //where doc.IsActive == true || hosdoc.IsActive == true
                            select new
                       {
                           DoctorName = doc.FirstName + " " + (String.IsNullOrEmpty(doc.MiddleName) ? " " : doc.MiddleName) + " " + doc.LastName,
                           doc.DoctorId,
                           doc.DepartmentId,
                           doc.HospitalId,
                           doc.FilePath,
                           doc.PhoneNumber,
                           doc.NMC,
                           doc.LongSignature,
                           doc.HospitalIdentifier,
                           IsActive = hosdoc != null ? hosdoc.IsActive : false
                       }).ToList();

            if (!string.IsNullOrEmpty(hospitalName) && hospitalName != "NA")
            {
                doctormap = doctormap.Where(x => x.HospitalIdentifier == hospitalName).ToList();
            }
            else
            {
                //doctors = _unitOfWork.Doctors.GetAll().Where(x=> string.IsNullOrEmpty(x.HospitalIdentifier)).ToList();
                doctormap = doctormap.ToList();  /*for Danphe Care*/
            }
            var qualification = _unitOfWork.Qualification.GetAll().ToList();
            // var doctorsVM = _mapper.Map<List<Doctor>, List<DoctorViewModel>>(doctors.ToList());
            if (qualification.Count() > 0)
            {
                var doclist = (from doc in doctormap
                               join qualf in qualification on doc.DoctorId equals qualf.DoctorId into qua
                               from outputqualification in qua.DefaultIfEmpty()
                               select new
                               {
                                   doc.DoctorName ,
                                   doc.DoctorId,
                                   doc.DepartmentId,
                                   doc.HospitalId,
                                   doc.FilePath,
                                   doc.PhoneNumber,
                                   doc.NMC,
                                   doc.LongSignature,
                                   Designation = outputqualification != null ? outputqualification.Designation: "NA",
                                   Education = outputqualification != null ? outputqualification.Education : "NA",
                                   Experience = outputqualification != null? outputqualification.Experience : "NA",
                                   PastAffiliation = outputqualification != null ? outputqualification.PastAffiliation : "NA",
                                   Membership = outputqualification != null ? outputqualification.Membership : "NA",
                                   doc.IsActive
                               }).ToList().GroupBy(a => a.DoctorId).Select(g => new
                               {
                                   DoctorId = g.Key,
                                   DoctorName = g.Select(a => a.DoctorName).FirstOrDefault(),
                                   FilePath = g.Select(a => a.FilePath).FirstOrDefault(),
                                   NMC = g.Select(a => a.NMC).FirstOrDefault(),
                                   PhoneNumber = g.Select(a => a.PhoneNumber).FirstOrDefault(),
                                   LongSignature = g.Select(a => a.LongSignature).FirstOrDefault(),
                                   Designation = g.Select(a => a.Designation).ToList(),
                                   Education = g.Select(a => a.Education).ToList(),
                                   Experience = g.Select(a => a.Experience).ToList(),
                                   PastAffiliation = g.Select(a => a.PastAffiliation).ToList(),
                                   Membership = g.Select(a => a.Membership).ToList(),
                                   HospitalId = g.Select(a => a.HospitalId).FirstOrDefault(),
                                   IsActive = g.Select(a=>a.IsActive).FirstOrDefault()
                               }).ToList();



                return Ok(doclist);
            }
            else
            {
                var doclist = (from doc in doctormap

                               select new
                               {
                                   doc.DoctorName,
                                   doc.DoctorId,
                                   doc.DepartmentId,
                                   doc.HospitalId,
                                   doc.FilePath,
                                   doc.PhoneNumber,
                                   doc.NMC,
                                   doc.LongSignature,
                                   doc.IsActive
                                  
                               }).ToList().GroupBy(a => a.DoctorId).Select(g => new
                               {
                                   DoctorId = g.Key,
                                   DoctorName = g.Select(a => a.DoctorName).FirstOrDefault(),
                                   FilePath = g.Select(a => a.FilePath).FirstOrDefault(),
                                   NMC = g.Select(a => a.NMC).FirstOrDefault(),
                                   PhoneNumber = g.Select(a => a.PhoneNumber).FirstOrDefault(),                               
                                   HospitalId = g.Select(a => a.HospitalId).FirstOrDefault(),
                                   IsActive = g.Select(a=>a.IsActive).FirstOrDefault()
                               }).ToList();



                return Ok(doclist);
            }

        }
        [HttpGet("{id}")]
        //[Produces(typeof(List<PatientViewModel>))]
        public async Task<ActionResult> ListofPatientForStatusUpdateByProviderId(Guid id)
        {
            try
            {
                if (id != Guid.Empty)
                {
                    var currentDate = DateTime.Now;
                    var patients = await _unitOfWork.Patients.GetAllAsync();
                    var visits = _unitOfWork.Visits.GetAll().ToList();
                    var docs = _unitOfWork.Doctors.GetAll().ToList();
                    var probs = _unitOfWork.Problems.GetAll().ToList();
                    var patlist = (from vst in visits
                                       //   join doc in docs on vst.ProviderId equals doc.DoctorId 
                                   join pat in patients on vst.PatientId equals pat.PatientId
                                   join prob in probs on vst.VisitId equals prob.VisitId
                                   where vst.ProviderId == id
                                   select new
                                   {
                                       vst.PatientId,
                                       vst.VisitId,
                                       pat.IdentityUserId,
                                       vst.VisitDate,
                                       vst.IsActive,
                                       pat.ContactNumber,
                                       pat.Address,
                                       PatientName = pat.FirstName + " " + (String.IsNullOrEmpty(pat.MiddleName) ? " " : pat.MiddleName) + " " + pat.LastName,
                                       vst.Status
                                   }).ToList();

                    return Ok(patlist);
                }
                else
                {
                    return Ok();
                }
            }
            catch
            {
                throw;
            }

        }
        [HttpGet("{id}")]
        //[Produces(typeof(List<PatientViewModel>))]
        public async Task<ActionResult> GetPatientListByProviderId(Guid id)
        {
            var patients = await _unitOfWork.Patients.GetAllAsync();
            var visits = _unitOfWork.Visits.GetAll().Where(x=>x.ProviderId == id && x.Status == "completed").ToList();
            var docs = _unitOfWork.Doctors.GetAll().ToList();
            var probs = _unitOfWork.Problems.GetAll().ToList();
            var patlist = (from vst in visits
                           join pat in patients on vst.PatientId equals pat.PatientId
                           join prob in probs on vst.VisitId equals prob.VisitId
                           select new
                           {
                               vst.PatientId,
                               vst.VisitId,
                               vst.VisitDate,
                               vst.VisitEndTime,
                               pat.ContactNumber,
                               PatientName = pat.FirstName + " " + (String.IsNullOrEmpty(pat.MiddleName) ? " " : pat.MiddleName) + " " + pat.LastName,
                               pat.Gender,
                               vst.TreatmentAdvice,
                               vst.Medication,
                               vst.FollowUp
                           }).ToList();

            return Ok(patlist);

        }

       

        [HttpGet]
        //[Produces(typeof(List<PatientViewModel>))]\
        //Appointment List Api
        public async Task<ActionResult> GetPatientListByAdmin()
        {
            var hospitalName = User.FindFirst("HospitalIdentifier").Value;
            var hospital= _unitOfWork.Hospitals.GetAll().Where(x=>x.HospitalIdentifier==hospitalName).Select(s=>s.HospitalId).FirstOrDefault();
            var patients = await _unitOfWork.Patients.GetAllAsync();
            if (!string.IsNullOrEmpty(hospitalName))
            {
                patients = _unitOfWork.Patients.GetAll().Where(x => x.HospitalIdentifier == hospitalName || x.HospitalIdentifier=="NA").ToList();
            }
            else
            {
                //patients = _unitOfWork.Patients.GetAll().Where(x => string.IsNullOrEmpty(x.HospitalIdentifier)).ToList();
                patients = _unitOfWork.Patients.GetAll().ToList(); /*for Danphe Care*/
            }
            var visits = _unitOfWork.Visits.GetAll().ToList();
            var docs = _unitOfWork.Doctors.GetAll().ToList();
            if (!string.IsNullOrEmpty(hospitalName))
            {
                docs = _unitOfWork.Doctors.GetAll().Where(x => x.HospitalIdentifier == hospitalName).ToList();
            }
            else
            {
                docs = _unitOfWork.Doctors.GetAll().Where(x => string.IsNullOrEmpty(x.HospitalIdentifier)).ToList();
            }
            var probs = _unitOfWork.Problems.GetAll().ToList();
            var patlist = (from vst in visits 
                           join doc in docs on vst.ProviderId equals doc.DoctorId 
                           join pat in patients on vst.PatientId equals pat.PatientId
                           join prob in probs on vst.VisitId equals prob.VisitId
                           where vst.HospitalId==hospital
                           select new 
                           {
                               vst.PatientId,
                               vst.VisitId,
                               vst.SchedulingId,
                               vst.VisitDate,
                               vst.HospitalId,
                               vst.DepartmentId,
                               doc.DoctorId,
                               vst.VisitEndTime,
                               pat.ContactNumber,
                               PatientName = pat.FirstName + " " + (String.IsNullOrEmpty(pat.MiddleName) ? " " : pat.MiddleName) + " " + pat.LastName,
                               pat.Gender,
                               pat.DateOfBirth,
                               vst.TreatmentAdvice,
                               vst.Medication,
                               DoctorName = doc.FirstName + " " + (String.IsNullOrEmpty(doc.MiddleName) ? " " : doc.MiddleName) + " " + doc.LastName,
                               vst.BookingTime,
                               vst.PaymentStatus,
                               vst.FollowUp,
                               vst.Status,
                               vst.IsActive,
                               HospitalName= hospitalName
                           }).ToList();

            return Ok(patlist);

        }
        [HttpGet]
        //[Produces(typeof(List<PatientViewModel>))]\
        //Appointment List Api
        public async Task<ActionResult> GetCancelledAppointmentPatientListByAdmin()
        {
            var hospitalName = User.FindFirst("HospitalIdentifier").Value;
            var patients = await _unitOfWork.Patients.GetAllAsync();
            if (!string.IsNullOrEmpty(hospitalName))
            {
                patients = _unitOfWork.Patients.GetAll().Where(x => x.HospitalIdentifier == hospitalName).ToList();
            }
            else
            {
                //patients = _unitOfWork.Patients.GetAll().Where(x => string.IsNullOrEmpty(x.HospitalIdentifier)).ToList();
                patients = _unitOfWork.Patients.GetAll().ToList(); /*for Danphe Care*/
            }
            var visits = _unitOfWork.Visits.GetAll().ToList();
            var docs = _unitOfWork.Doctors.GetAll().ToList();
            if (!string.IsNullOrEmpty(hospitalName))
            {
                docs = _unitOfWork.Doctors.GetAll().Where(x => x.HospitalIdentifier == hospitalName).ToList();
            }
            else
            {
                docs = _unitOfWork.Doctors.GetAll().Where(x => string.IsNullOrEmpty(x.HospitalIdentifier)).ToList();
            }
            var probs = _unitOfWork.Problems.GetAll().ToList();
            var patlist = (from vst in visits
                           join doc in docs on vst.ProviderId equals doc.DoctorId
                           join pat in patients on vst.PatientId equals pat.PatientId
                           join prob in probs on vst.VisitId equals prob.VisitId
                           where vst.Status == "cancelled"
                           select new
                           {
                               vst.PatientId,
                               vst.VisitId,
                               vst.SchedulingId,
                               vst.VisitDate,
                               vst.HospitalId,
                               vst.DepartmentId,
                               doc.DoctorId,
                               vst.VisitEndTime,
                               pat.ContactNumber,
                               PatientName = pat.FirstName + " " + (String.IsNullOrEmpty(pat.MiddleName) ? " " : pat.MiddleName) + " " + pat.LastName,
                               pat.Gender,
                               pat.DateOfBirth,
                               vst.TreatmentAdvice,
                               vst.Medication,
                               DoctorName = doc.FirstName + " " + (String.IsNullOrEmpty(doc.MiddleName) ? " " : doc.MiddleName) + " " + doc.LastName,
                               vst.BookingTime,
                               vst.PaymentStatus,
                               vst.FollowUp,
                               vst.Status,
                               vst.IsActive
                           }).ToList();

            return Ok(patlist);

        }

        [HttpGet("{id}")]
        public async Task<ActionResult> GetPatientList(Guid id)
        {
            var patients = await _unitOfWork.Patients.GetAllAsync();
            var visits = _unitOfWork.Visits.GetAll().Where(x=>x.ProviderId == id && (x.PaymentStatus == "paid" || x.PaymentStatus == "free")).ToList();
            var patlist = (from vst in visits
                           join pat in patients on vst.PatientId equals pat.PatientId
                           //where vst.PaymentStatus == "paid" || vst.PaymentStatus =="free"
                           select new
                           {
                               vst.PatientId,
                               vst.VisitId,
                               vst.Status,
                               vst.HospitalId,
                               vst.DepartmentId,
                               vst.ProviderId,
                               vst.VisitDate,
                               pat.ContactNumber,
                               PatientName = pat.FirstName + " " + (String.IsNullOrEmpty(pat.MiddleName) ? " " : pat.MiddleName) + " " + pat.LastName,
                               pat.Gender,
                           }).ToList();

            return Ok(patlist);

        }

        [HttpGet("{id}")]
        public IActionResult GetDetails(Guid id)
        {
            try
            {
                var _getDoctor = _unitOfWork.Doctors.GetGuid(id);
                var _doctor = new DoctorViewModel();
                var _doctorVM = _mapper.Map<Doctor, DoctorViewModel>(_getDoctor);
                var _mapHosDep = _unitOfWork.HospitalDoctorMaps.GetAll().Where(x => x.DoctorId == id).ToList();
                var _qualification = _unitOfWork.Qualification.GetAll().Where(x => x.DoctorId == id).ToList();
                _doctorVM.HospitalDepartmentMap = new List<HospitalDoctorMapViewModel>();
                _doctorVM.DoctorQualification = new List<QualificationViewModel>();
                if (_mapHosDep.Count > 0)
                {
                    foreach (var item in _mapHosDep)
                    {
                        var _map = new HospitalDoctorMapViewModel();
                        _map.DepartmentId = item.DepartmentId.ToString();
                        _map.HospitalId = item.HospitalId.ToString();
                        _map.HospitalDoctorMapId = item.HospitalDoctorMapId.ToString();
                        _map.Charge = item.Charge;
                        _map.ConsultationType = item.ConsultationType;
                        _map.HospitalName = _unitOfWork.Hospitals.GetAll().Where(x => x.HospitalId == item.HospitalId).Select(s => s.HospitalName).FirstOrDefault();
                        _map.DepartmentName = _unitOfWork.Departments.GetAll().Where(x => x.DepartmentId == item.DepartmentId).Select(s => s.DepartmentName).FirstOrDefault();
                        _doctorVM.HospitalDepartmentMap.Add(_map);
                    }
                }
                if (_qualification.Count > 0)
                {
                    foreach ( var item in _qualification)
                    {
                        var _qua = new QualificationViewModel();
                        _qua.Designation = item.Designation;
                        _qua.Education = item.Education;
                        _qua.Experience = item.Experience;
                        _qua.Membership = item.Membership;
                        _qua.PastAffiliation = item.PastAffiliation;
                        _qua.QualificationId = item.QualificationId.ToString();
                        _qua.DoctorId = item.DoctorId.ToString();


                        _doctorVM.DoctorQualification.Add(_qua);
                    }
                }

                return Ok(_doctorVM);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetDoctorHospital(Guid id)
        {
            try
            {
                var _getDoctor = _unitOfWork.Doctors.GetAll().ToList();
                var result = (from doc in _getDoctor
                              where doc.DoctorId == id
                              select new
                              {
                                  DoctorName = doc.FirstName + " " + (String.IsNullOrEmpty(doc.MiddleName) ? " " : doc.MiddleName) + " " + doc.LastName,
                                  doc.Department,
                                  doc.FilePath,
                                  doc.FirstName,
                              }).ToList();

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("{id}/{hosId}")]
        public async Task<ActionResult> GetDoc(Guid id, Guid hosId)
        {
            var CurrentDate = DateTime.Now;
            var currentHour = DateTime.Now.Hour;
            var CurrentDay = CurrentDate.Day;
            var doclist = await _unitOfWork.Doctors.GetAllAsync();
            var qualification = _unitOfWork.Qualification.GetAll();
            var hosDoc = _unitOfWork.HospitalDoctorMaps.GetAll();
            var hospitalDoctormapId = _unitOfWork.HospitalDoctorMaps.GetAll().Where(x => x.DoctorId == id && x.HospitalId == hosId).Select(s => s.HospitalDoctorMapId).ToList();
            var SchedulingListId = _unitOfWork.DoctorSchedulings.GetAll().Where(x => hospitalDoctormapId.Contains(x.HospitalDoctorMapId)).Select(s => s.SchedulingId).DefaultIfEmpty().ToList();
            var doctor = (from docs in doclist
                          join qualf in qualification on docs.DoctorId equals qualf.DoctorId into qua
                          from outputqualification in qua.DefaultIfEmpty()
                          join hosDoctor in hosDoc on docs.DoctorId equals hosDoctor.DoctorId
                        //  join docSch in docSchedule on hosDoctor.HospitalDoctorMapId equals docSch.HospitalDoctorMapId into sch
                          //from shcheduling in sch.DefaultIfEmpty()
                          where docs.DoctorId == id && hosDoctor.HospitalId == hosId
                          select new
                          {
                              DoctorName = docs.FirstName + " " + (String.IsNullOrEmpty(docs.MiddleName) ? " " : docs.MiddleName) + " " + docs.LastName,
                              docs.FilePath,
                              hosDoctor.HospitalId,
                              docs.PhoneNumber,
                              docs.MailingAddress,
                              HospitalDoctorMapId = _unitOfWork.HospitalDoctorMaps.GetAll().Where(x => x.DoctorId == id && x.HospitalId == hosId).ToList(),
                              hosDoctor.DoctorId,
                              Qualification = _unitOfWork.Qualification.GetAll().Where(x => x.DoctorId == id).ToList(),
                              DepartmentId = _unitOfWork.HospitalDoctorMaps.GetAll().Where(x => x.DoctorId == id && x.HospitalId == hosId).Select(s => s.DepartmentId).ToList(),
                              SchedulingList = _unitOfWork.DoctorSchedulings.GetAll().Where(x=> SchedulingListId.Contains(x.SchedulingId) && x.Date >= CurrentDate).ToList().OrderBy(s => s.Date),
                             // ScheduleInterval = _unitOfWork.ScheduleInterval.GetAll().Where(x => SchedulingListId.Contains(x.SchedulingId)).ToList().OrderBy(s=>s.Date),
                          }).FirstOrDefault();

            return Ok(doctor);
        }

        [HttpPost]
        [Produces(typeof(DoctorViewModel))]
        public IActionResult Post([FromBody]DoctorViewModel _doctorVM)
        {
            //Check done to prevent Insecure Direct Object References(IDOR) vulnerability
            //if (!(await _authorizationService.AuthorizeAsync(User, _doctorVM.DoctorId, policyName: "IsDoctorSame")).Succeeded)
            //{
            //    return Forbid();
            //}
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                //_doctorVM.DoctorId = ;
                var doctor = _mapper.Map<Doctor>(_doctorVM);
                _unitOfWork.Doctors.Add(doctor);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status201Created));
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }


        [HttpPost, DisableRequestSizeLimit]
        public ActionResult UploadFile()
        {
            try
            {
                //var docvm = _mapper.Map<Doctor>(_);
                var file = Request.Form.Files[0];
                var fl = new Doctor();
                string folderName = "Upload";
                string webRootPath = _hostingEnvironment.WebRootPath;
                string newPath = Path.Combine(webRootPath, folderName);

                if (!Directory.Exists(newPath))
                {
                    Directory.CreateDirectory(newPath);
                }
                if (file.Length > 0)
                {
                    string fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    fl.FileName = fileName;
                    // fl.FilePath = "../Upload/" + fileName;
                    string fullPath = Path.Combine(newPath, fileName);
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }

                }

                return Json("Upload Successful.");

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return Json("Upload Failed");
            }
        }
        [HttpPost()]
        [Produces(typeof(PatientFilesUploadViewModel))]
        public IActionResult UploadPrescription([FromBody] PatientFilesUploadViewModel _UploadPrescription)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var prescription = _mapper.Map<PatientFilesUpload>(_UploadPrescription);
                prescription.PatientId = Guid.Parse(_UploadPrescription.PatientId);
                prescription.VisitId = Guid.Parse(_UploadPrescription.VisitId);
                prescription.FilePath = "../Upload/" + _UploadPrescription.FilePath.Substring(_UploadPrescription.FilePath.LastIndexOf('\\') + 1); ;
                prescription.FileName = _UploadPrescription.FilePath.Substring(_UploadPrescription.FilePath.LastIndexOf('\\') + 1);
                prescription.FileExtention = _UploadPrescription.FilePath.Substring(_UploadPrescription.FilePath.LastIndexOf('.') + 1);
                prescription.FileKind = "Prescription";

                _unitOfWork.PatientFilesUpload.Add(prescription);

                _unitOfWork.SaveChanges();
                
                return Ok(StatusCode(StatusCodes.Status201Created));

            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        [HttpPut("{id}")]
        [Produces(typeof(DoctorViewModel))]
        public async Task<ActionResult> UpdateDoctor([FromBody] DoctorViewModel _doctorVM)
        {

            //Check done to prevent Insecure Direct Object References(IDOR) vulnerability
            if (!String.IsNullOrEmpty(_doctorVM.AdminId))
            {
                if (!(await _authorizationService.AuthorizeAsync(User, _doctorVM.AdminId, policyName: "CanRegisterDoctor")).Succeeded)
                {
                    return Forbid();
                }
            }
            else {
                if (!(await _authorizationService.AuthorizeAsync(User, _doctorVM.DoctorId, policyName: "IsDoctorSame")).Succeeded)
                {
                    return Forbid();
                }
            }
        

            var _map = JsonConvert.DeserializeObject<List<HospitalDoctorMapViewModel>>(_doctorVM.HospitalDoctorMap);
            var _mapQualification = JsonConvert.DeserializeObject<List<QualificationViewModel>>(_doctorVM.Qualification);

            try
            {
                var hospitalName = User.FindFirst("HospitalIdentifier").Value;
                var doctor = _mapper.Map<Doctor>(_doctorVM);
                doctor.HospitalIdentifier = hospitalName;
               
                if (_doctorVM.FilePath != null || _doctorVM.FileName != null)
                {
                    doctor.FilePath = "../Upload/" + _doctorVM.FilePath.Substring(_doctorVM.FilePath.LastIndexOf('\\') + 1);
                    doctor.FileName = _doctorVM.FilePath.Substring(_doctorVM.FilePath.LastIndexOf('\\') + 1);
                    doctor.FileExtention = _doctorVM.FilePath.Substring(_doctorVM.FilePath.LastIndexOf('.') + 1);
                    _unitOfWork.Doctors.UpdateProperties(doctor, p => p.FirstName, p => p.DateOfBirth, p => p.LastName, p => p.MiddleName, p => p.NMC, p => p.LongSignature,
               p => p.MailingAddress, p => p.Gender, p => p.PhoneNumber, p => p.DepartmentId, p => p.FilePath, p => p.FileName, p => p.FileExtention, p => p.EnablePhNo, p => p.HospitalIdentifier);

                }
                else
                {
                    _unitOfWork.Doctors.UpdateProperties(doctor, p => p.FirstName, p => p.DateOfBirth, p => p.LastName, p => p.MiddleName, p => p.NMC, p => p.LongSignature,
                p => p.MailingAddress, p => p.Gender, p => p.PhoneNumber, p => p.DepartmentId, p => p.EnablePhNo, p => p.HospitalIdentifier);
                }




                var hosdep = new List<HospitalDoctorMapViewModel>();
                var qualification = new List<QualificationViewModel>();
                var _mapDocQualification = _mapper.Map<List<QualificationViewModel>, List<QualificationViewModel>>(_mapQualification, qualification);
                var _mapHosDep = _mapper.Map<List<HospitalDoctorMapViewModel>, List<HospitalDoctorMapViewModel>>(_map, hosdep);
                if (_mapHosDep.Count > 0)
                {
                    foreach (var item in _mapHosDep)
                    {
                        if (item.HospitalId != "" && item.DepartmentId != "")
                        {
                            HospitalDoctorMap _hospitalDoctorMap = new HospitalDoctorMap();
                            _hospitalDoctorMap.HospitalDoctorMapId = Guid.NewGuid();
                            _hospitalDoctorMap.HospitalId = new Guid(item.HospitalId);
                            _hospitalDoctorMap.DepartmentId = new Guid(item.DepartmentId);
                            _hospitalDoctorMap.DoctorId = _doctorVM.DoctorId;
                            _hospitalDoctorMap.Charge = item.Charge;
                            _hospitalDoctorMap.ConsultationType = item.ConsultationType;
                            _hospitalDoctorMap.IsActive = true;
                            _unitOfWork.HospitalDoctorMaps.Add(_hospitalDoctorMap);
                        }
                    }
                }
                if (_mapDocQualification.Count > 0)
                {
                    foreach (var item in _mapDocQualification)
                    {
                        if (item.Designation != "" && item.Education != "")
                        {
                            Qualification _qualification = new Qualification();
                            _qualification.Designation = item.Designation;
                            _qualification.Education = item.Education;
                            _qualification.Experience = item.Experience;
                            _qualification.PastAffiliation = item.PastAffiliation;
                            _qualification.Membership = item.Membership;
                            _qualification.DoctorId = _doctorVM.DoctorId;
                            _unitOfWork.Qualification.Add(_qualification);

                        }
                    }
                }

                _unitOfWork.SaveChanges();
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpDelete("{id}")]
        [Produces(typeof(DoctorViewModel))]
        public void Delete(Guid id)
        {
            //Check done to prevent Insecure Direct Object References(IDOR) vulnerability
            //if (!(await _authorizationService.AuthorizeAsync(User, id, policyName: "IsDoctorSame")).Succeeded)
            //{
            //    return;
            //}
            var _doctorToDelete = _unitOfWork.Doctors.GetGuid(id);
            try
            {
                _unitOfWork.Doctors.Remove(_doctorToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ex.Message);
            }
        }
        [HttpDelete("{id}")]
        [Produces(typeof(HospitalDoctorMapViewModel))]
        public void DeleteHospitalMap(Guid id)
        {
            var _hospitalMapToDelete = _unitOfWork.HospitalDoctorMaps.GetGuid(id);

            try
            {
                _unitOfWork.HospitalDoctorMaps.Remove(_hospitalMapToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        [HttpPut("{id}")]
        [Produces(typeof(HospitalDoctorMapViewModel))]
        public ActionResult UpdateHosDocMap([FromBody] HospitalDoctorMapViewModel _hosDocVM)
        {
            try
            {
                var hosdocmap = _mapper.Map<HospitalDoctorMap>(_hosDocVM);
                _unitOfWork.HospitalDoctorMaps.UpdateProperties(hosdocmap, p => p.HospitalId, p => p.DepartmentId, p => p.Charge , p => p.ConsultationType);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        [HttpPut("{id}")]
        [Produces(typeof(QualificationViewModel))]
        public ActionResult UpdateQualification([FromBody] QualificationViewModel _qualificationVM)
        {
            try
            {
                var qualification = _mapper.Map<Qualification>(_qualificationVM);
                _unitOfWork.Qualification.UpdateProperties(qualification, p => p.Designation, p => p.Education, p => p.PastAffiliation, p => p.Membership , p => p.Experience);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        [HttpPut("{id}")]
        [Produces(typeof(DoctorViewModel))]
        public ActionResult Deactivate([FromBody] DoctorViewModel _doctorVm)
        {
            try
            {
                var hospitalName = User.FindFirst("HospitalIdentifier").Value;
                //var doctorVM = _mapper.Map<Doctor>(_doctorVm);
                //doctorVM.IsActive = false;
                var hospitalId = _unitOfWork.Hospitals.GetAll().Where(x => x.HospitalIdentifier == hospitalName).Select(s => s.HospitalId).FirstOrDefault();
                var docSchedule = _unitOfWork.HospitalDoctorMaps.GetAll().Where(x => x.HospitalId == hospitalId && x.DoctorId == _doctorVm.DoctorId).ToList();
                if(docSchedule != null)
                {
                    if(_doctorVm.IsActive == true)
                    {
                        if(docSchedule.Count() > 0)
                        {
                            foreach (var item in docSchedule)
                            {
                                item.IsActive = false;
                                _unitOfWork.HospitalDoctorMaps.Update(item);
                            }
                        }
                    }
                    else
                    {
                        if (docSchedule.Count() > 0)
                        {
                            foreach (var item in docSchedule)
                            {
                                item.IsActive = true;
                                _unitOfWork.HospitalDoctorMaps.Update(item);
                            }
                        }
                    }

                    
                }
                //_unitOfWork.Doctors.Update(doctorVM);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
    }
}
