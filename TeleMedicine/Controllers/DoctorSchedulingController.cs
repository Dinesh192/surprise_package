﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telemedicine.DalLayer;
using TestTele.ViewModel;
using AutoMapper;
using Telemedicine.ServerModel;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Index = System.Index;
using System.Globalization;

namespace TestTele.Controllers
{
    [Route("api/[controller]/[action]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class DoctorSchedulingController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private int index;

        public DoctorSchedulingController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
        }

       

        [HttpGet]
        public async Task<ActionResult> GetDoctor()
        {
            var hospitalName = User.FindFirst("HospitalIdentifier").Value;
            var hosdocmap =  _unitOfWork.HospitalDoctorMaps.GetAll().Where(x=>x.IsActive ==  true).ToList();
            var doctor = _unitOfWork.Doctors.GetAll().ToList();
            if (!string.IsNullOrEmpty(hospitalName))
            {
                doctor = _unitOfWork.Doctors.GetAll().Where(x => x.HospitalIdentifier == hospitalName).ToList();
            }
            else
            {
                //doctor = _unitOfWork.Doctors.GetAll().Where(x => string.IsNullOrEmpty(x.HospitalIdentifier)).ToList();
                doctor = _unitOfWork.Doctors.GetAll().ToList(); /*for Danphe Care*/
            }
            var department = await _unitOfWork.Departments.GetAllAsync();
            var hospital = _unitOfWork.Hospitals.GetAll().ToList();
            Guid guid = System.Guid.NewGuid();

            var doctorlist = (from doc in doctor
                              join hospitaldoc in hosdocmap on doc.DoctorId equals hospitaldoc.DoctorId into hosdoctor
                              from hosdoc in hosdoctor.DefaultIfEmpty()
                              select new
                              {
                                  DoctorName = doc.FirstName + " " + (String.IsNullOrEmpty(doc.MiddleName) ? " " : doc.MiddleName) + " " + doc.LastName,
                                  doctorId = hosdoc != null ? hosdoc.DoctorId : doc.DoctorId,
                                  departmentId = hosdoc != null ? hosdoc.DepartmentId : guid,
                                  hospitalId = hosdoc != null ? hosdoc.HospitalId : guid ,
                                   hospitaldoctormapId = hosdoc != null ? hosdoc.HospitalDoctorMapId : guid,
                              }).ToList();
            var deptHospital = doctorlist.GroupJoin(department, a => a.departmentId, b => b.DepartmentId, (a, b) => new HospitalDoctorMapViewModel
            {
                DoctorName = a.DoctorName,
                DoctorId = a.doctorId.ToString(),
                DepartmentId = b.Select( x => x.DepartmentId).FirstOrDefault().ToString(),
                HospitalDoctorMapId = a.hospitaldoctormapId.ToString(),
                DepartmentName = b.Select(x => x.DepartmentName).FirstOrDefault(),
                HospitalId = a.hospitalId.ToString()
            }).ToList();
            var docDeptHospital = deptHospital.GroupJoin(hospital, c => new Guid (c.HospitalId), d => d.HospitalId, (c, d) => new HospitalDoctorMapViewModel
                  {
                      DoctorName = c.DoctorName,
                      DoctorId = c.DoctorId,
                      DepartmentId = c.DepartmentId,
                      HospitalDoctorMapId = c.HospitalDoctorMapId,
                      DepartmentName = c.DepartmentName,
                      HospitalId = d.Select( x => x.HospitalId).FirstOrDefault().ToString(),
                      HospitalName = d.Select ( y => y.HospitalName).FirstOrDefault()
                  }).ToList();

            return Ok(docDeptHospital);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> GetSchedulingTime(Guid id)
        {
            try
            {
                var _schedulingTime = await _unitOfWork.DoctorSchedulings.GetAllAsync();
               
                 var _docScheduling = (from sch in _schedulingTime
                               where sch.HospitalDoctorMapId == id
                               orderby sch.Date
                               select new
                               {
                                  sch.SchedulingId,
                                  sch.Date,
                                   StartTime=   DateTime.Parse(sch.StartTime.ToString()).ToString("hh:mm tt", CultureInfo.GetCultureInfo("en-US")),
                                   EndTime = DateTime.Parse(sch.EndTime.ToString()).ToString("hh:mm tt", CultureInfo.GetCultureInfo("en-US")),
                                   StartHours = sch.StartTime.Hours,
                                   StartMinutes = sch.StartTime.Minutes,
                                   EndHours = sch.EndTime.Hours,
                                   EndMinutes = sch.EndTime.Minutes,

                                   //StartMeridian = sch.StartTime.TotalMinutes <= 720 ? "A.M" : "P.M",
                                   //EndMeridian = sch.EndTime.TotalMinutes <= 720 ? "A.M" : "P.M",
                                   //DoctorAvailable = sch.StartTime + " " + " - " + " " + sch.EndTime,


                               }). ToList();


                return Ok(_docScheduling);
            }
            catch
            {
                throw;
            }
        }
        [HttpGet("{id}")]
        public IActionResult GetScheduleIntervalById(Guid id)
        {
            try
            {
               var _schedulingIntervalbyId = _unitOfWork.ScheduleInterval.GetAll().ToList();
                //return Ok(_schedulingIntervalbyId);
                var _results = (from schinterval in _schedulingIntervalbyId
                              where schinterval.ScheduleIntervalId == id
                              select new
                              {
                                  ScheduleIntervalId = schinterval.ScheduleIntervalId,
                                  SchedulingId =  schinterval.SchedulingId,
                                  HospitalDoctorMapId =   schinterval.HospitalDoctorMapId,
                                  IsActive = schinterval.IsActive,
                                  IsBooked = schinterval.IsBooked,
                                  StartTime = schinterval.StartTime,
                                  VisitId = schinterval.VisitId,
                                  EndTime = schinterval.EndTime,
                                  Date = schinterval.Date
                              }).ToList();

                return Ok(_results);
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        [HttpGet("{id}")]
        public IActionResult GetScheduleIntervalBySchedulingId(Guid id)
        {
            try
            {
                var _schedulingIntervalbyId = _unitOfWork.ScheduleInterval.GetAll().ToList();
                //return Ok(_schedulingIntervalbyId);
                var _results = (from schinterval in _schedulingIntervalbyId
                                where schinterval.SchedulingId == id
                                select new
                                {
                                    ScheduleIntervalId = schinterval.ScheduleIntervalId,
                                    SchedulingId = schinterval.SchedulingId,
                                    HospitalDoctorMapId = schinterval.HospitalDoctorMapId,
                                    IsActive = schinterval.IsActive,
                                    IsBooked = schinterval.IsBooked,
                                    StartTime = schinterval.StartTime,
                                    VisitId = schinterval.VisitId,
                                    EndTime = schinterval.EndTime,
                                    Date = schinterval.Date,
                                    StartTimeDetail = "",
                                }).ToList();

                return Ok(_results);
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetScheduleDate(Guid id)
        {
            try
            {
                var _schedulingTime = _unitOfWork.DoctorSchedulings.GetAll().Where(x=>x.HospitalDoctorMapId==id).ToList();
                var _docScheduling = (from sch in _schedulingTime
                                      orderby sch.Date
                                      select new
                                      {
                                          sch.SchedulingId,
                                          sch.Date,
                                          StartTime = DateTime.Parse(sch.StartTime.ToString()).ToString("hh:mm tt", CultureInfo.GetCultureInfo("en-US")),
                                          EndTime = DateTime.Parse(sch.EndTime.ToString()).ToString("hh:mm tt", CultureInfo.GetCultureInfo("en-US")),
                                          StartHours = sch.StartTime.Hours,
                                          StartMinutes = sch.StartTime.Minutes,
                                          EndHours = sch.EndTime.Hours,
                                          EndMinutes = sch.EndTime.Minutes,
                                          //StartMeridian = sch.StartTime.TotalMinutes <= 720 ? "A.M" : "P.M",
                                          //EndMeridian = sch.EndTime.TotalMinutes <= 720 ? "A.M" : "P.M",
                                          //DoctorAvailable = sch.StartTime + " " + " - " + " " + sch.EndTime,


                                      }).ToList();



                return Ok(_docScheduling);
            }
            catch
            {
                throw;
            }
        }



        [HttpGet("{visitdate}/{id}/{id1}/{hosId}")]
        public  IActionResult GetVisitTime(DateTime visitdate, Guid id, Guid id1, Guid hosId)
        {
            
            var hospitalDoctorMapId = _unitOfWork.HospitalDoctorMaps.GetAll().Where(x => x.DoctorId == id && x.HospitalId == hosId && x.DepartmentId == id1).Select(s=>s.HospitalDoctorMapId).FirstOrDefault();
            var schedullinginterval = _unitOfWork.ScheduleInterval.GetAll().Where(x => x.Date.Year == visitdate.Year && x.Date.Month == visitdate.Month && x.Date.Day == visitdate.Day &&x.HospitalDoctorMapId==hospitalDoctorMapId).ToList();
            var schedullingList = _unitOfWork.DoctorSchedulings.GetAll().Where(x => x.Date.Year == visitdate.Year && x.Date.Month == visitdate.Month && x.Date.Day == visitdate.Day &&x.HospitalDoctorMapId==hospitalDoctorMapId).ToList();
            var hosDocMap = _unitOfWork.HospitalDoctorMaps.GetAll().ToList();
            var variable = (from hosdoc in hosDocMap
                            // join sch in schedullinginterval on hosdoc.HospitalDoctorMapId equals sch.HospitalDoctorMapId
                            join schlist in schedullingList on hosdoc.HospitalDoctorMapId equals schlist.HospitalDoctorMapId
                            where hosdoc.HospitalDoctorMapId == hospitalDoctorMapId
                            select new
                            {
                                Date = schlist.Date,
                                StartTime = DateTime.Parse(schlist.StartTime.ToString()).ToString("hh:mm tt", CultureInfo.GetCultureInfo("en-US")),
                                EndTime = DateTime.Parse(schlist.EndTime.ToString()).ToString("hh:mm tt", CultureInfo.GetCultureInfo("en-US")),
                                SchedulingId = schlist.SchedulingId,
                                HospitalDoctorMapId = schlist.HospitalDoctorMapId
                            }).ToList();

            return Ok(variable);
        }



        [HttpPost]
        [Produces(typeof(SchedulingViewModel))]
        public IActionResult PostScheduling([FromBody]SchedulingViewModel _schedulingVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {

                List<DoctorScheduling> schedulingList = new List<DoctorScheduling>();
                if (_schedulingVM.NewScheduleList.Count > 0)
                {
                    foreach (var item in _schedulingVM.NewScheduleList)
                    {


                        TimeSpan starttime = new TimeSpan(_schedulingVM.NewScheduleList[index].StartHours, _schedulingVM.NewScheduleList[index].StartMinutes, _schedulingVM.NewScheduleList[index].StartSeconds);
                        TimeSpan endtime = new TimeSpan(_schedulingVM.NewScheduleList[index].EndHours, _schedulingVM.NewScheduleList[index].EndMinutes, _schedulingVM.NewScheduleList[index].EndSeconds);
                        DoctorScheduling scheduling = new DoctorScheduling();
                        scheduling.SchedulingId = Guid.NewGuid();
                        scheduling.Date = _schedulingVM.StartDate;
                        scheduling.StartTime = starttime;
                        scheduling.EndTime = endtime;
                        scheduling.HospitalDoctorMapId = _schedulingVM.HospitalDoctorMapId;
                        schedulingList.Add(scheduling);
                        index++;

                    }

                }
                
                foreach (var item in schedulingList)
                {
                    DoctorScheduling newScheduling = new DoctorScheduling();
                    newScheduling = _mapper.Map<DoctorScheduling, DoctorScheduling>(item);
                    _unitOfWork.DoctorSchedulings.Add(newScheduling);
                    _unitOfWork.SaveChanges();
                }

                return Ok(StatusCode(StatusCodes.Status201Created));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }

        }


        [HttpPost]
        [Produces(typeof(SchedulingViewModel))]
        public IActionResult AddScheduling([FromBody]SchedulingViewModel _schedulingVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {  
                var schVM = new List<ScheduleIntervalViewModel>();
                var _schedule = _mapper.Map<List<ScheduleIntervalViewModel>, List<ScheduleIntervalViewModel>>(_schedulingVM.ScheduleInterval, schVM);
                List<DoctorScheduling> schedulingList = new List<DoctorScheduling>();
                int DayInterval = 1;
                if (_schedulingVM.EndDate != null)
                {
                    while (_schedulingVM.StartDate <= _schedulingVM.EndDate)
                    {
                        foreach (var items in _schedulingVM.NewScheduleList)
                        {
                            TimeSpan starttime = new TimeSpan(_schedulingVM.NewScheduleList[index].StartHours, _schedulingVM.NewScheduleList[index].StartMinutes, _schedulingVM.NewScheduleList[index].StartSeconds);
                            TimeSpan endtime = new TimeSpan(_schedulingVM.NewScheduleList[index].EndHours, _schedulingVM.NewScheduleList[index].EndMinutes, _schedulingVM.NewScheduleList[index].EndSeconds);
                            DoctorScheduling schedulings = new DoctorScheduling();
                            schedulings.SchedulingId = Guid.NewGuid();
                            schedulings.Date = _schedulingVM.StartDate;
                            schedulings.StartTime = starttime;
                            schedulings.EndTime = endtime;
                            schedulings.HospitalDoctorMapId = _schedulingVM.HospitalDoctorMapId;
                            schedulings.AccomodatedPatient = _schedulingVM.NewScheduleList[index].AccomodatedPatient;
                            if (_schedule.Count > 0)
                            {
                                foreach (var item in _schedule)
                                {
                                    ScheduleInterval _SchdInterval = new ScheduleInterval();
                                    _SchdInterval.ScheduleIntervalId = Guid.NewGuid();
                                    _SchdInterval.StartTime = DateTime.Parse(item.StartTime).ToShortTimeString();
                                    _SchdInterval.EndTime = DateTime.Parse(item.EndTime).ToShortTimeString();
                                    _SchdInterval.VisitId = null;
                                    _SchdInterval.Date = item.Date;
                                    _SchdInterval.HospitalDoctorMapId = item.HospitalDoctorMapId;
                                    _SchdInterval.IsActive = item.IsActive;
                                    _SchdInterval.IsBooked = item.IsBooked;
                                    var splitstring = _SchdInterval.StartTime.Remove(_SchdInterval.StartTime.Length - 2);
                                    var position = _SchdInterval.StartTime.IndexOf(":");
                                    var min = _SchdInterval.StartTime.Substring(position+1, 2);
                                    var intparse = int.Parse(min);
                                    int  minInstring = Convert.ToInt32(intparse);
                                    var result = _SchdInterval.StartTime.Substring(_SchdInterval.StartTime.Length - 2);
                                    var firstwochar = _SchdInterval.StartTime.Substring(0,2);
                                    TimeSpan ts = TimeSpan.Parse(splitstring);
                                    if (result == "PM") {
                                        if (firstwochar == "12")
                                        {
                                            TimeSpan time1 =  new TimeSpan(12, minInstring, 0);
                                            ts = time1;
                                        }
                                        else
                                        {
                                            TimeSpan time1 = TimeSpan.FromHours(12);
                                            ts = ts.Add(time1);
                                        }
                                       
                                    }else
                                    {
                                        if (firstwochar == "12")
                                        {
                                            TimeSpan time1 = new TimeSpan(0, minInstring, 0);
                                            ts = time1;
                                        }
                                        
                                    }
                                    if (item.Date == _schedulingVM.StartDate && ((ts >= starttime) && (ts <= endtime)))
                                    {
                                        _SchdInterval.SchedulingId = schedulings.SchedulingId;
                                        _unitOfWork.ScheduleInterval.Add(_SchdInterval);
                                    }
                                }

                            }
                            schedulingList.Add(schedulings);
                            //_unitOfWork.DoctorSchedulings.Add(schedulings);
                           // _unitOfWork.SaveChanges();
                            index++;
                        }

                        _schedulingVM.StartDate = _schedulingVM.StartDate.AddDays(DayInterval);
                        index = 0;
                    }
                    foreach (var data in schedulingList)
                    {
                        DoctorScheduling newScheduling = new DoctorScheduling();
                        newScheduling = _mapper.Map<DoctorScheduling, DoctorScheduling>(data);
                        _unitOfWork.DoctorSchedulings.Add(newScheduling);
                        _unitOfWork.SaveChanges();
                    }
                }
                else
                {
                    foreach (var items in _schedulingVM.NewScheduleList)
                    {
                        TimeSpan starttime = new TimeSpan(_schedulingVM.StartHours, _schedulingVM.StartMinutes, _schedulingVM.StartSeconds);
                        TimeSpan endtime = new TimeSpan(_schedulingVM.EndHours, _schedulingVM.EndMinutes, _schedulingVM.EndSeconds);
                        DoctorScheduling schedulings = new DoctorScheduling();
                        schedulings.SchedulingId = Guid.NewGuid();
                        schedulings.Date = _schedulingVM.StartDate;
                        schedulings.StartTime = starttime;
                        schedulings.EndTime = endtime;
                        schedulings.HospitalDoctorMapId = _schedulingVM.HospitalDoctorMapId;
                        schedulings.AccomodatedPatient = _schedulingVM.NewScheduleList[index].AccomodatedPatient;
                        if (_schedule.Count > 0)
                        {
                            foreach (var item in _schedule)
                            {
                                ScheduleInterval _SchdInterval = new ScheduleInterval();
                                _SchdInterval.ScheduleIntervalId = Guid.NewGuid();
                                _SchdInterval.StartTime = DateTime.Parse(item.StartTime).ToShortTimeString();
                                _SchdInterval.EndTime = DateTime.Parse(item.EndTime).ToShortTimeString();
                                _SchdInterval.VisitId = null;
                                _SchdInterval.Date = item.Date;
                                _SchdInterval.HospitalDoctorMapId = item.HospitalDoctorMapId;
                                _SchdInterval.IsActive = item.IsActive;
                                _SchdInterval.IsBooked = item.IsBooked;
                                var splitstring = _SchdInterval.StartTime.Remove(_SchdInterval.StartTime.Length - 2);
                                var result = _SchdInterval.StartTime.Substring(_SchdInterval.StartTime.Length - 2);
                                TimeSpan ts = TimeSpan.Parse(splitstring);
                                if (result == "PM")
                                {
                                    TimeSpan time1 = TimeSpan.FromHours(12);
                                    ts = ts.Add(time1);
                                }
                                if (item.Date == _schedulingVM.StartDate && ((ts >= starttime) && (ts <= endtime)))
                                {
                                    _SchdInterval.SchedulingId = schedulings.SchedulingId;
                                    _unitOfWork.ScheduleInterval.Add(_SchdInterval);
                                    _unitOfWork.SaveChanges();
                                }

                            }

                        }
                        _unitOfWork.DoctorSchedulings.Add(schedulings);
                        _unitOfWork.SaveChanges();
                        index++;
                    }
                }
               

                return Ok(StatusCode(StatusCodes.Status200OK));
               
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }

        }

        [HttpPost("{visitId}/{schIntId}/{schId}/{pastschId}/{pastbookingTime}")]
        [Produces(typeof(VisitViewModel))]
        public IActionResult PostReschedule([FromBody]VisitViewModel _visitVM, Guid visitId, Guid schIntId, Guid schId, string pastschId, string pastbookingTime)
        {
           
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var visitdata = _mapper.Map<Visit>(_visitVM);
                var _schedulingIntervalbyId = _unitOfWork.ScheduleInterval.GetAll().ToList();
                var _vist = _unitOfWork.Visits.GetAll();

                ScheduleInterval schInt = (from i in _schedulingIntervalbyId
                                           where i.ScheduleIntervalId == schIntId
                                           select i).First();
                schInt.IsBooked = true;
                schInt.VisitId = visitId;
              
                _unitOfWork.ScheduleInterval.Update(schInt);
                Visit visit = (from v in _vist
                               where v.VisitId == visitId
                               select v).First();

                visit.VisitId = visitId;
                visit.Status = "initiated";
                visit.BookingTime = _visitVM.BookingTime;
                visit.SchedulingId = schId;
                visit.VisitDate = _visitVM.VisitDate;
                _unitOfWork.Visits.Update(visit);
                var _schedulingIntervalList = _unitOfWork.ScheduleInterval.GetAll().ToList();
                if (pastbookingTime != "null" )
                {
                    var index = pastbookingTime.IndexOf("-");
                    // var temp = visitdata.BookingTime.Substring(visitdata.BookingTime.Length - index);
                    var temp1 = pastbookingTime.Remove(index - 1);
                    ScheduleInterval schIntervalId = (from i in _schedulingIntervalbyId
                                                      where i.SchedulingId == Guid.Parse(pastschId) && i.StartTime == temp1
                                                      select i).First();
                    schIntervalId.IsBooked = false;
                    schIntervalId.VisitId = null;
                    _unitOfWork.ScheduleInterval.Update(schIntervalId);
                    //return Ok();
                }
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (Exception ex)
            {
                throw ex;
               
            }
        }





      



        [HttpPut("{id}")]
        [Produces(typeof(SchedulingViewModel))]
        public ActionResult UpdateScheduling([FromBody] SchedulingViewModel _scheduleVM)
        {
            try
            {
                TimeSpan starttime = new TimeSpan(_scheduleVM.StartHours, _scheduleVM.StartMinutes, _scheduleVM.StartSeconds);
                TimeSpan endtime = new TimeSpan(_scheduleVM.EndHours, _scheduleVM.EndMinutes, _scheduleVM.EndSeconds);
                _scheduleVM.StartTime = starttime;
                _scheduleVM.EndTime = endtime;
                _scheduleVM.StartDate = _scheduleVM.StartDate.AddDays(1);
                var schedule = _mapper.Map<DoctorScheduling>(_scheduleVM);
                schedule.Date = _scheduleVM.StartDate;
                _unitOfWork.DoctorSchedulings.UpdateProperties(schedule, p => p.Date, p => p.StartTime, p => p.EndTime);
   
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        [HttpDelete("{id}")]
        [Produces(typeof(SchedulingViewModel))]
        public void DeleteSchedule(Guid id)
        {
            var _ScheduleToDelete = _unitOfWork.DoctorSchedulings.GetGuid(id);

            try
            {
                _unitOfWork.DoctorSchedulings.Remove(_ScheduleToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
    }
}
