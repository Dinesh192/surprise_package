﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telemedicine.DalLayer;
using TestTele.ViewModel;
using AutoMapper;
using Telemedicine.ServerModel;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using System.Net.Http.Headers;
using Microsoft.Extensions.Hosting.Internal;
using System.Net.Mail;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;

namespace TestTele.Controllers
{
    [Route("api/[controller]/[action]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class HospitalController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private IWebHostEnvironment _hostingEnvironment;
        private readonly ILogger<HospitalController> _logger;
        private readonly IAuthorizationService _authorizationService;
        private readonly UserManager<ApplicationUser> _userManager;
        public HospitalController(IUnitOfWork unitOfWork, IMapper mapper, IWebHostEnvironment hostingEnvironment, IAuthorizationService authorizationService,
                                  UserManager<ApplicationUser> userManager, ILogger<HospitalController> logger)
        {
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
            _hostingEnvironment = hostingEnvironment;
            _authorizationService = authorizationService;
            this._logger = logger;
            _userManager = userManager;
        }


        [HttpGet]
        [Produces(typeof(List<HospitalViewModel>))]
        public async Task<ActionResult> HospitalDetails()
        {
            //  List<Hospital> hospitalList = _unitOfWork.Hospitals.GetAll().ToList();
            // return Ok(hospitalList);

            var hospitalName = User.FindFirst("HospitalIdentifier").Value;
            //if(hospitalName == "NA")
            //{
            //    var patientId = User.FindFirst("PatientIdentifier").Value;
            //    var hospitalIdentifier = _unitOfWork.Patients.GetAll().Where(x => x.PatientId.ToString() == patientId).Select(s => s.HospitalIdentifier).FirstOrDefault();
            //    var contactNumber = _unitOfWork.Patients.GetAll().Where(x => x.PatientId.ToString() == patientId).Select(s => s.ContactNumber).FirstOrDefault();
            //    var identityUser = await _userManager.FindByNameAsync(contactNumber);
            //    var identity = new ClaimsIdentity(User.Identity);
            //    List<Claim> userClaims = (await _userManager.GetClaimsAsync(identityUser)).ToList();
            //    await _userManager.RemoveClaimsAsync(identityUser, userClaims);
            //    var adminClaim = new Claim[]
            //                      {
            //                        new Claim(Constants.UserType, "Patient"),
            //                        new Claim(Constants.PatientIdentifier, patientId),
            //                         new Claim(Constants.Hospital, hospitalIdentifier)
            //                      };
            //    await _userManager.AddClaimsAsync(identityUser, adminClaim);
            //    hospitalName = hospitalIdentifier;
            //}
            var hospitalList = await _unitOfWork.Hospitals.GetAllAsync();
            if (!string.IsNullOrEmpty(hospitalName) && hospitalName != "NA")
            {
                hospitalList = _unitOfWork.Hospitals.GetAll().Where(x => x.HospitalIdentifier == hospitalName);
            }
            else
            {
                //hospitalList = _unitOfWork.Hospitals.GetAll().Where(x => string.IsNullOrEmpty(x.HospitalIdentifier));
                hospitalList = _unitOfWork.Hospitals.GetAll(); 
            }
            var hospitalsVM = _mapper.Map<List<Hospital>,List<HospitalViewModel>>(hospitalList.ToList());
            if(hospitalsVM.Count() > 0)
            {
                return Ok(hospitalsVM);
            }
            else
            {
                return Ok();
            }
            
        }

        [HttpGet("{id}")]
        public IActionResult GetHospital(Guid id)
        {
            try
            {

                var _getHospital = _unitOfWork.Hospitals.GetGuid(id);
                var hospital = _mapper.Map<Hospital, HospitalViewModel>(_getHospital);
                return Ok(hospital);
            }
            catch
            {
                throw;
            }
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> GetDocHospital(Guid id)
        {
            var hosdocmap =  _unitOfWork.HospitalDoctorMaps.GetAll().Where(x=>x.IsActive ==  true).ToList();
            var hosp =await _unitOfWork.Hospitals.GetAllAsync(); 
            var doc = _unitOfWork.Doctors.GetAll().Where(x => x.DoctorId == id).ToList();
            var hosdocVM = _mapper.Map<List<HospitalDoctorMap>, List<HospitalDoctorMapViewModel>>(hosdocmap.ToList());
            var dochosList = (from hosdoc in hosdocmap
                             join hos in hosp on hosdoc.HospitalId equals hos.HospitalId
                             join docdetails in doc on hosdoc.DoctorId equals  docdetails.DoctorId
                              where hosdoc.DoctorId == id
                              select new
                              {
                                  hos.HospitalName,
                                  hosdoc.DepartmentId,
                                  hosdoc.HospitalId,
                                  hosdoc.HospitalDoctorMapId,
                                  DoctorName = docdetails.FirstName + " " + (String.IsNullOrEmpty(docdetails.MiddleName) ? " " : docdetails.MiddleName) + " " + docdetails.LastName,
                                  docdetails.FilePath,
                                  docdetails.PhoneNumber,
                                  docdetails.EnablePhNo,
                                  docdetails.MailingAddress,
                                  hosdoc.IsActive,
                                  Qualification = _unitOfWork.Qualification.GetAll().Where(x => x.DoctorId == id).ToList(),
                              }).ToList().GroupBy(a => a.HospitalId).Select(g => new
                              {
                                  HospitalName = g.Select(a => a.HospitalName).FirstOrDefault(),
                                  HospitalId = g.Select(a => a.HospitalId).FirstOrDefault(),
                                  DepartmentId = g.Select(a => a.DepartmentId).ToList(),
                                  DoctorName = g.Select(a => a.DoctorName).FirstOrDefault(),
                                  FilePath = g.Select(a => a.FilePath).FirstOrDefault(),
                                  MailingAddress = g.Select(a => a.MailingAddress).FirstOrDefault(),
                                  PhoneNumber = g.Select(a => a.PhoneNumber).FirstOrDefault(),
                                  EnablePhNo = g.Select(a => a.EnablePhNo).FirstOrDefault(),
                                  Qualification = g.Select(a => a.Qualification).FirstOrDefault(),
                                  IsActive = g.Select(a=>a.IsActive).FirstOrDefault()
                              });

            return Ok(dochosList);
        }
        [HttpPost]
        [Produces(typeof(HospitalViewModel))]
        public IActionResult AddHospital([FromBody]HospitalViewModel _hospitalVM)
        {
            //Check done to prevent Insecure Direct Object References(IDOR) vulnerability
            //if (!(await _authorizationService.AuthorizeAsync(User, _doctorVM.DoctorId, policyName: "IsDoctorSame")).Succeeded)
            //{
            //    return Forbid();
            //}
            var hospitalName = User.FindFirst("HospitalIdentifier").Value;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var hospital = _mapper.Map<Hospital>(_hospitalVM);
                if (_hospitalVM.ImagePath != null || _hospitalVM.HospitalImage != null)
                {
                    hospital.ImagePath = "../Upload/" + _hospitalVM.ImagePath.Substring(_hospitalVM.ImagePath.LastIndexOf('\\') + 1);
                    //doctor.FileName = _doctorVM.FilePath.Substring(_doctorVM.FilePath.LastIndexOf('\\') + 1);
                    //doctor.FileExtention = _doctorVM.FilePath.Substring(_doctorVM.FilePath.LastIndexOf('.') + 1);
                }
                hospital.HospitalIdentifier = hospitalName;
                 _unitOfWork.Hospitals.Add(hospital);
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status201Created));
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }
        [HttpDelete("{id}")]
        [Produces(typeof(HospitalViewModel))]
        public void DeleteHospital(Guid id)
        {
            var _hospitalToDelete = _unitOfWork.Hospitals.GetGuid(id);

            try
            {
                _unitOfWork.Hospitals.Remove(_hospitalToDelete);
                _unitOfWork.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }
        [HttpPut("{id}")]
        [Produces(typeof(HospitalViewModel))]
        public ActionResult UpdateHospital([FromBody] HospitalViewModel _hospitalVM)
        {
            try
            {
                var hospital = _mapper.Map<Hospital>(_hospitalVM);
                var hospitalName = User.FindFirst("HospitalIdentifier").Value;
                hospital.HospitalIdentifier = hospitalName;
                if (_hospitalVM.ImagePath != null && _hospitalVM.HospitalName != null)
                {
                    hospital.ImagePath = "../Upload/" + _hospitalVM.ImagePath.Substring(_hospitalVM.ImagePath.LastIndexOf('\\') + 1);
                    
                }
                _unitOfWork.Hospitals.UpdateProperties(hospital, p => p.HospitalName, p => p.HospitalCode, p => p.Location, p => p.ImagePath, p => p.HospitalImage, p => p.IsActive, p => p.PaymentEnable );
                _unitOfWork.SaveChanges();
                return Ok(StatusCode(StatusCodes.Status200OK));
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
        }


    }
}
