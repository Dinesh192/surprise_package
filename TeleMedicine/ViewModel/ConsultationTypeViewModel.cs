﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestTele.ViewModel
{
    public class ConsultationTypeViewModel
    {
        public Guid ConsultationTypeId { get; set; }
        public string Description { get; set; }
        public decimal Charge { get; set; }
    }
}
