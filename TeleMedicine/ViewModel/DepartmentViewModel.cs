﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestTele.ViewModel
{
    public class DepartmentViewModel
    {
        public Guid  DepartmentId { get; set; }

        public string DepartmentName { get; set; }
        public string DepartmentShortName { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }
  
}