﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestTele.ViewModel
{
    public class QualificationViewModel
    {
        public string QualificationId { get; set; }
        public string Designation { get; set; }
        public string Education { get; set; }
        public string PastAffiliation { get; set; }
        public string Experience { get; set; }
        public string Membership { get; set; }
        public string DoctorId { get; set; }
    }
}
