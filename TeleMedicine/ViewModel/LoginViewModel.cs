﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TestTele.ViewModel
{
    public class LoginViewModel
    {
        [Required]
        [StringLength(10, MinimumLength = 10, ErrorMessage = "Not enough digits in Phone Number")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Phone Number must be numeric")]
        public string PhoneNumber { get; set; }
        //public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
        public bool RememberMe { get; set; }
    }

    public class UserViewModel
    {
        [Required]
        public string UserId { get; set; }
        [Required]
        [StringLength(6, MinimumLength = 6, ErrorMessage = "Password must be atleast 6 characters")]
        public string Password { get; set; }
    }
}
