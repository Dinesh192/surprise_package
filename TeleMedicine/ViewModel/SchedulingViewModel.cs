﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TestTele.ViewModel
{
    public class SchedulingViewModel
    {
        public Guid SchedulingId { get; set; }
        [Column(TypeName = "date")]
        public DateTime StartDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? EndDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime Date { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
        public int StartHours { get; set; }
        public int StartMinutes { get; set; }
        public int StartSeconds { get; set; }
        public int EndHours { get; set; }
        public int EndMinutes { get; set; }
        public int EndSeconds { get; set; }
        public Guid HospitalDoctorMapId { get; set; }
        public virtual List<SchedulingListViewModel> NewScheduleList { get; set; }
        public virtual List<ScheduleIntervalViewModel> ScheduleInterval { get; set; }
    }
    public class SchedulingListViewModel
    {
        public int StartHours { get; set; }
        public int StartMinutes { get; set; }
        public int StartSeconds { get; set; }
        public int EndHours { get; set; }
        public int EndMinutes { get; set; }
        public int EndSeconds { get; set; }
        public int AccomodatedPatient { get; set; }
    }

        public class VisitDateViewModel
        {
            [Column(TypeName = "date")]
            public DateTime VisitDate { get; set; }
            [Column(TypeName = "date")]
            public DateTime Date { get; set; }
            public Guid? DepartmentId { get; set; }
            public Guid? HospitalId { get; set; }
            public Guid DoctorId { get; set; }
            public string StartTime { get; set; }
            public string EndTime { get; set; }
            public int StartHours { get; set; }
            public int StartMinutes { get; set; }
            public int StartSeconds { get; set; }
            public int EndHours { get; set; }
            public int EndMinutes { get; set; }
            public int EndSeconds { get; set; }
            public string StartMeridian { get; set; }
            public string EndMeridian { get; set; }
        }
        public class ScheduleIntervalViewModel
        {
            public Guid ScheduleIntervalId { get; set; }
            public string StartTime { get; set; }
            public string EndTime { get; set; }
            public DateTime Date { get; set; }
            public bool IsBooked { get; set; }
            public bool IsActive { get; set; }
            public Guid HospitalDoctorMapId { get; set; }
            public Guid? VisitId { get; set; }
            public string SchedulingId { get; set; }

        }
    }

