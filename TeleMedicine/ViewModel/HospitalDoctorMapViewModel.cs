﻿using Microsoft.AspNetCore.Authentication;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Telemedicine.ServerModel;

namespace TestTele.ViewModel
{
    public class HospitalDoctorMapViewModel
    {
        public string HospitalDoctorMapId { get; set; }
        public string DoctorId { get; set; }

        public string HospitalId { get; set; }
        public string DepartmentId { get; set; }
        public string HospitalName { get; set; }
        public string DepartmentName { get; set; }
        public string DoctorName { get; set; }

        public decimal Charge { get; set; }
        public string ConsultationType { get; set; }
        public string HospitalIdentifier { get; set; }


    }
  
}
