﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telemedicine.ServerModel;

namespace TestTele.ViewModel
{
    public class PaymentTransactionViewModel
    {
        public Guid TransactionId { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public Guid ConsultationTypeId { get; set; }
        public ConsultationTypeViewModel ConsultationType { get; set; }
        public DateTime TransactionDate { get; set; }
        public decimal Amount { get; set; }
        public Guid VisitId { get; set; }
        public virtual VisitViewModel Visit { get; set; }
    }
}
