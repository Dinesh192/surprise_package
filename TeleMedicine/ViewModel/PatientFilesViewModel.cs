﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestTele.ViewModel
{
    public class PatientFilesViewModel
    {
        public Guid PatientFileId { get; set; }
        public Guid PatientId { get; set; }
        public Guid ROWGUID { get; set; }
        public string FileType { get; set; }
        public string Title { get; set; }
        public DateTime UploadedOn { get; set; }
        public int UploadedBy { get; set; }
        public string Description { get; set; }
        public byte[] FileBinaryData { get; set; }
        public int FileNo { get; set; }
        public string FileName { get; set; }
        public string FileExtention { get; set; }

        public bool? IsActive { get; set; }

        public Guid VisitId { get; set; }
        public string FileBase64String { get; set; }

        public bool? HasFile { get; set; }
    }
}
