﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TestTele.ViewModel
{
    public class PatientFilesUploadViewModel
    {
        public string PatientFileId { get; set; }
        public string PatientId { get; set; }
        public string VisitId { get; set; }
        public string FileType { get; set; }
        public byte[] FileBinaryData { get; set; }
        public int FileNo { get; set; }
        public string FileName { get; set; }
        public string FileExtention { get; set; }
        public string Title { get; set; }
        public string Comment { get; set; }
        public string FilePath { get; set; }
        public string FileKind { get; set; }

        [NotMapped]
        public string FileBase64String { get; set; }
    }
}
