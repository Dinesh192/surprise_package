﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestTele.ViewModel
{
    public class EsewaTransactionViewModel
    {
        public Guid TransactionId { get; set; } // A visitid taken as a reference transactionid for payment
        public virtual PaymentTransactionViewModel Transaction { get; set; }
        public string ReferenceId { get; set; }  // A unique payment reference code from eSewa generated on SUCCESSFUL transaction
    }
}
