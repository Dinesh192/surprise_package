﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestTele.ViewModel
{
    public class VisitViewModel
    {
        public Guid VisitId { get; set; }

        public DateTime VisitDate { get; set; }
        public Guid ProviderId { get; set; }
        public DateTime? VisitStartTime { get; set; }
        public DateTime? VisitEndTime { get; set; }
        public string BookingTime { get; set; }
        public string TreatmentAdvice { get; set; }
        public string Medication { get; set; }
        public string FollowUp  { get; set; }
        public Guid ConsultationId { get; set; }

        public Guid PatientId { get; set; }
        public string VisitType { get; set; }
        public bool? IsActive { get; set; }
        public string Status { get; set; }
        public string PaymentStatus { get; set; }
        public Guid HospitalId { get; set; }
        public Guid HospitalDoctorMapId { get; set; }
        public Guid DepartmentId { get; set; }
        public Guid SchedulingId { get; set; }
        public ProblemViewModel Problem { get; set; }
        public  PatientFilesUploadViewModel PatientFilesUpload { get; set; }
        public List<PatientFilesUploadViewModel> PatientFiles { get; set; }
        public  string PatFile { get; set; }
        public DateTime LastUpdateTime { get; set; }
        public bool IsConversationCompleted { get; set; }

    }
  
}