﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TestTele.ViewModel
{
    public class PatientViewModel
    {
        public Guid PatientId { get; set; }
        public int RegistrationNumber { get; set; }
        [Required]
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string UserName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string MailingAddress { get; set; }
        public string Address { get; set; }
        public string IdentityUserId { get; set; }
        public bool? IsActive { get; set; }
        [Required]
        public string Password { get; set; }
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        [Required]
        [StringLength(10, MinimumLength = 10, ErrorMessage = "Not enough digits in Phone Number")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Phone Number must be numeric")]
        public string PhoneNumber { get; set; }
        public string ContactNumber { get; set; }
        public Guid ConsultationId { get; set; }
        public virtual List<VisitViewModel> VisitDetails { get; set; }
        public virtual ProblemViewModel  Problems { get; set; }
        public virtual PatientFilesViewModel PatientFiles { get; set; }
        public string AdminId { get; set; }
        public string HospitalIdentifier { get; set; }
    }
}
