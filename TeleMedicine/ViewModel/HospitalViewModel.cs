﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestTele.ViewModel
{
    public class HospitalViewModel
    {

        public Guid HospitalId { get; set; }
        public string HospitalName { get; set; }
        public string HospitalCode { get; set; }
        public string HospitalImage { get; set; }
        public string ImagePath { get; set; }
        public string HospitalLogo { get; set; }
        public string Location { get; set; }
        public bool IsActive { get; set; }
        public bool PaymentEnable { get; set; }
        public string HospitalIdentifier { get; set; }

    }
}
