﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Telemedicine.ServerModel;

namespace TestTele.ViewModel
{
    public class DoctorViewModel
    {
        public Guid DoctorId { get; set; }
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        [Required]
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        [Required]
        public string LastName { get; set; } 
        public string NMC { get; set; }
        public string LongSignature { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileExtention { get; set; }
        public string MailingAddress { get; set; }
        public string Gender { get; set; }
        [Required]
        [StringLength(10, MinimumLength = 10, ErrorMessage = "Not enough digits in Phone Number")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Phone Number must be numeric")]
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
     //   public string Department { get; set; }
        public int? HospitalId { get; set; }
        public Guid? DepartmentId { get; set; }
        public virtual Department Department { get; set; }
        public bool? IsActive { get; set; }
        public string IdentityUserId { get; set; }
        public string AdminId { get; set; }
        public virtual ApplicationUser IdentityUser { get; set; }
        public string HospitalDoctorMap { get; set; }
        public string Qualification { get; set; }
        public bool EnablePhNo { get; set; }
        public string HospitalIdentifier { get; set; }
        public List<HospitalDoctorMapViewModel> HospitalDepartmentMap { get; set; }
        public List<QualificationViewModel> DoctorQualification { get; set; }
        public Guid? DoctorRoomName { get; set; }

    }
}
