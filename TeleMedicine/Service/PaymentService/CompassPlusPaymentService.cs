﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Xml.Linq;
using Telemedicine.DalLayer;
using Telemedicine.ServerModel;

namespace TestTele.Service.PaymentService
{
    public class CompassPlusPaymentService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IWebHostEnvironment _environment;
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        public CompassPlusPaymentService(IUnitOfWork unitOfWork,
                                         IWebHostEnvironment environment,
                                         IConfiguration configuration,
                                         ILoggerFactory loggerFactory)
        {
            _unitOfWork = unitOfWork;
            _environment = environment;
            _configuration = configuration;
            _logger = loggerFactory.CreateLogger("TestTele.Service.PaymentService.CompassPlusPaymentService");
        }
        public void UpdateNabilPaymentStatus(string visitId, NabilPaymentStatus status)
        {
            var nabilTransaction = LatestNotCompletedNabilTransactionForVisitId(visitId);
            nabilTransaction.PaymentStatus = status;
            _unitOfWork.SaveChanges();
        }

        public void SaveGetOrderStatusRequest(string visitId, string orderStatusRequestXML)
        {
            var nabilTransaction = LatestNotCompletedNabilTransactionForVisitId(visitId);
            nabilTransaction.GetOrderStatusRequest = orderStatusRequestXML;
            _unitOfWork.SaveChanges();
        }

        public void SaveGetOrderStatusResponse(string visitId, string orderStatus)
        {
            var nabilTransaction = LatestNotCompletedNabilTransactionForVisitId(visitId);
            nabilTransaction.GetOrderStatusResponse = orderStatus;
            _unitOfWork.SaveChanges();
        }

        public XDocument GetOrderStatusRequestData(string orderId, string sessionId)
        {
            string orderStatusPath = Path.Combine(_environment.ContentRootPath, @"Service\PaymentService\GetOrderStatus.xml");
            var XMLOrderStatus = XDocument.Load(orderStatusPath);
            XMLOrderStatus.Descendants("Merchant").First().SetValue(_configuration["Payment:NabilMerchantCode"]);
            XMLOrderStatus.Descendants("OrderID").First().SetValue(orderId);
            XMLOrderStatus.Descendants("SessionID").First().SetValue(sessionId);
            return XMLOrderStatus;
        }

        public void SaveResponseXMLOut(string visitId, string nabilResponseXML)
        {
            var nabilTransaction = LatestNotCompletedNabilTransactionForVisitId(visitId);
            nabilTransaction.ResponseXMLOut = nabilResponseXML;
            _unitOfWork.SaveChanges();
        }

        public void SaveCreateOrderResponse(string visitId, string createOrderResponse)
        {
            var nabilTransaction = LatestNotCompletedNabilTransactionForVisitId(visitId);
            nabilTransaction.CreateOrderResponse = createOrderResponse;
            _unitOfWork.SaveChanges();
        }
        public void CompleteTransactionNabil(string visitId)
        {
            var mainTransaction = _unitOfWork.PaymentTransactions.GetSingleOrDefault(t => t.VisitId == Guid.Parse(visitId) && t.PaymentStatus != PaymentStatus.Complete);
            mainTransaction.PaymentStatus = PaymentStatus.Complete;
            var nabilTransaction = LatestNotCompletedNabilTransactionForVisitId(visitId);
            nabilTransaction.PaymentStatus = NabilPaymentStatus.Approved;
            var visit = _unitOfWork.Visits.GetSingleOrDefault(v => v.VisitId == mainTransaction.VisitId);
            visit.PaymentStatus = "paid";
            //save all in one Transaction
            _unitOfWork.SaveChanges();
        }

        public string GetSesssionID(string visitId)
        {
            var nabilTransaction = LatestNotCompletedNabilTransactionForVisitId(visitId);
            return nabilTransaction.SessionId;
        }
        public void SaveOrderIdAndSessionId(string visitId, string orderId, string sessionId)
        {
            var nabilTransaction = LatestNotCompletedNabilTransactionForVisitId(visitId);
            nabilTransaction.OrderId = orderId;
            nabilTransaction.SessionId = sessionId;
            _unitOfWork.SaveChanges();
        }
        public void SaveCreateOrderRequest(string createOrderRequestXML, string visitId, decimal charge)
        {
            using (var scope = new TransactionScope())
            {
                var mainTransaction = _unitOfWork.PaymentTransactions
                                                 .GetSingleOrDefault(t => t.VisitId == Guid.Parse(visitId) && t.PaymentStatus == PaymentStatus.Pending);
                // check if Payment has been intitiated before and is declined(pending)
                if (mainTransaction == null)
                {
                    mainTransaction = new PaymentTransaction
                    {
                        VisitId = Guid.Parse(visitId),
                        Amount = charge,
                        PaymentMethod = PaymentMethod.NabilCard,
                        TransactionDateTime = DateTime.Now,
                        PaymentStatus = PaymentStatus.Pending
                    };
                    _unitOfWork.PaymentTransactions.Add(mainTransaction);
                    _unitOfWork.SaveChanges();
                    var nabilTransaction = new NabilTransaction
                    {
                        TransactionId = mainTransaction.TransactionId,
                        CreateOrderRequest = createOrderRequestXML,
                        TransactionDateTime = DateTime.Now,
                        PaymentStatus = NabilPaymentStatus.Pending
                    };
                    _unitOfWork.NabilTransactions.Add(nabilTransaction);
                    _unitOfWork.SaveChanges();
                    scope.Complete();
                }
                else
                {
                    //Payment for this Visit has been initiated before but declined/canceled in Nabil and pending in Main Transaction
                    var nabilTransaction = new NabilTransaction
                    {
                        TransactionId = mainTransaction.TransactionId,
                        CreateOrderRequest = createOrderRequestXML,
                        TransactionDateTime = DateTime.Now,
                        PaymentStatus = NabilPaymentStatus.Pending
                    };
                    _unitOfWork.NabilTransactions.Add(nabilTransaction);
                    _unitOfWork.SaveChanges();
                    scope.Complete();
                }

            }

        }
        private NabilTransaction LatestNotCompletedNabilTransactionForVisitId(string visitId)
        {
            var nabilTransaction = (from main in _unitOfWork.PaymentTransactions.Find(t => t.VisitId == Guid.Parse(visitId) && t.PaymentStatus != PaymentStatus.Complete)
                                    join nabil in _unitOfWork.NabilTransactions.Find(t => t.PaymentStatus != NabilPaymentStatus.Approved)
                                    on main.TransactionId equals nabil.TransactionId
                                    orderby nabil.TransactionDateTime descending
                                    select nabil).FirstOrDefault();

            return nabilTransaction;
            //var mainTransaction = _unitOfWork.PaymentTransactions.GetSingleOrDefault(t => t.VisitId == Guid.Parse(visitId) && t.PaymentStatus != PaymentStatus.Complete);
            //return _unitOfWork.NabilTransactions.Find(t => t.TransactionId == mainTransaction.TransactionId && )
            //                                    .OrderByDescending(t => t.TransactionDateTime)
            //                                    .FirstOrDefault();
        }
        public async Task<XDocument> ReadBodyAsync(Stream body)
        {
            XDocument responseXML;
            using (var reader = new StreamReader(body))
            {
                {
                    //ContentType is application/x-www-form-urlencoded from Nabil
                    var formUrlEncoded = await reader.ReadToEndAsync();
                    _logger.LogInformation(formUrlEncoded);
                    var formUrlDecoded = WebUtility.UrlDecode(formUrlEncoded);
                    var formBody = QueryHelpers.ParseQuery(formUrlDecoded);
                    //response is in a variable named xmlmsg
                    var messageXML = formBody.FirstOrDefault(x => x.Key == "xmlmsg").Value;
                    responseXML = XDocument.Parse(messageXML);
                }
                return responseXML;
            }
        }
        public string Decrypt(string encryptedMessage)
        {
            // Convert encrypted message and password to bytes
            string hexKey = _configuration["Payment:NabilKey"];
            byte[] encryptedMessageBytes = HexStringToByteArray(encryptedMessage);
            byte[] keyBytes = HexStringToByteArray(hexKey);

            // Set encryption settings -- Use password for both key and init. vector
            DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
            provider.Key = keyBytes;
            provider.Padding = PaddingMode.None;
            provider.Mode = CipherMode.ECB;
            ICryptoTransform transform = provider.CreateDecryptor();
            CryptoStreamMode mode = CryptoStreamMode.Write;

            // Set up streams and decrypt
            MemoryStream memStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memStream, transform, mode);
            cryptoStream.Write(encryptedMessageBytes, 0, encryptedMessageBytes.Length);
            cryptoStream.FlushFinalBlock();

            // Read decrypted message from memory stream
            byte[] decryptedMessageBytes = new byte[memStream.Length];
            memStream.Position = 0;
            memStream.Read(decryptedMessageBytes, 0, decryptedMessageBytes.Length);

            // Encode decrypted binary data to ASCII string
            string messageASCII = Encoding.ASCII.GetString(decryptedMessageBytes);

            return messageASCII;
        }

        public byte[] HexStringToByteArray(string hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        public async Task<XDocument> GetOrderStatusNabilAsync(XDocument orderStatusRequestXML)
        {
            HttpClient httpClient = GetHttpClient();

            string nabilPaymentGatewayUrl = _configuration["Payment:NabilCompassPlusUrl"];
            var orderStatusHttpRequest = new HttpRequestMessage(HttpMethod.Post, nabilPaymentGatewayUrl)
            {
                Content = new StringContent(orderStatusRequestXML.ToString(), Encoding.UTF8, "application/xml")
            };
            var orderStatusResponse = await httpClient.SendAsync(orderStatusHttpRequest);
            XDocument orderStatus = null;
            if (orderStatusResponse.IsSuccessStatusCode)
            {
                var content = await orderStatusResponse.Content.ReadAsStringAsync();
                orderStatus = XDocument.Parse(content);
            }
            return orderStatus;
        }

        public HttpClient GetHttpClient()
        {
            var clientCertificatePath = Path.Combine(_environment.ContentRootPath, @"Service\PaymentService\nabilcertificate.pfx");
            var clientCertificate = new X509Certificate2(clientCertificatePath,"Imark@123");
            var handler = new HttpClientHandler();
            handler.ClientCertificates.Add(clientCertificate);
            handler.ServerCertificateCustomValidationCallback += (o, c, ch, er) => true;
            return new HttpClient(handler);
        }

        public async Task<XDocument> CreateOrderRequestAsync(XDocument createOrderRequestXML)
        {
            HttpClient httpClient = GetHttpClient();

            string nabilPaymentGatewayUrl = _configuration["Payment:NabilCompassPlusUrl"];
            var createOrderRequest = new HttpRequestMessage(HttpMethod.Post, nabilPaymentGatewayUrl)
            {
                Content = new StringContent(createOrderRequestXML.ToString(), Encoding.UTF8, "application/xml")
            };
            var purchaseRequestResult = await httpClient.SendAsync(createOrderRequest);
            XDocument createOrderResponseXML = null;
            if (purchaseRequestResult.IsSuccessStatusCode)
            {
                string content = await purchaseRequestResult.Content.ReadAsStringAsync();
                createOrderResponseXML = XDocument.Parse(content);
            }
            else
            {
                _logger.LogInformation($"Purchase Request Unsuccessful with {purchaseRequestResult.StatusCode} from Compass Plus PG");
                //return StatusCode((int)purchaseRequestResult.StatusCode, $"Purchase Request Unsuccessful with {purchaseRequestResult.StatusCode} from Compass Plus PG");
            }
            return createOrderResponseXML;
        }
        public XDocument GetOrderRequestData(string visitId, string charge)
        {
            string XMLRequestFormatPath = Path.Combine(_environment.ContentRootPath, @"Service\PaymentService\RequestFormat.xml");
            var XMLRequestFormat = XDocument.Load(XMLRequestFormatPath);
            var orderElement = XMLRequestFormat.Descendants("Request").First().Descendants("Order").First();
            orderElement.Element("Merchant").SetValue(_configuration["Payment:NabilMerchantCode"]);
            orderElement.Element("Amount").SetValue(charge);
            orderElement.Element("Description").SetValue(visitId);
            return XMLRequestFormat;
        }
        public string GetPGErrorMessage(string responseStatusPG)
        {
            // these error messages are from the Compass Plus PG Documentation
            switch (responseStatusPG)
            {
                case "30":
                    return "Invalid Message Format (no mandatory parameters etc.)";
                case "10":
                    return "Internet shop has no access to the Create Order operation (or the Internet shop is not registered)";
                case "54":
                    return "Invalid Operation";
                case "72":
                    return "Empty POS Driver Response";
                case "96":
                    return "System Error";
                case "97":
                    return "POS Driver Connection Error";
                default:
                    return "Error Not in Compass Plus Documentation";
            }
        }
        public decimal ChargeForVisit(string visitId)
        {
            return _unitOfWork.Visits.FindByIncluding(v => v.VisitId == Guid.Parse(visitId), v => v.HospitalDoctorMap)
                                     .Select(v => v.HospitalDoctorMap.Charge)
                                     .FirstOrDefault();
        }
    }
}
