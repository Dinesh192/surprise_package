﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telemedicine.ServerModel;

namespace TestTele.Service.PaymentService
{
    public interface IPaymentProvider
    {
        List<PaymentTransaction> GetPaymentTransactions();
        Task<bool> IsTransactionValid(PaymentParameters paymentParameters);
        void SaveTransaction(PaymentParameters paymentParameters);
    }
}
