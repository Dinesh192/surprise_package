﻿using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Telemedicine.DalLayer;
using Telemedicine.ServerModel;

namespace TestTele.Service.PaymentService
{
    public class PaymentService
    {
        private readonly IPaymentProvider _paymentProvider;
        public PaymentService(IPaymentProvider paymentProvider)
        {
            _paymentProvider = paymentProvider;
        }

        public async Task<bool> IsTransactionValid(PaymentParameters parameters)
        {
            return await _paymentProvider.IsTransactionValid(parameters);
        }

        public void SaveTransaction(PaymentParameters paymentParameters)
        {
            _paymentProvider.SaveTransaction(paymentParameters);
        }
    }
}
