﻿using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Telemedicine.DalLayer;
using Telemedicine.ServerModel;

namespace TestTele.Service.PaymentService
{
    public class EsewaPaymentProvider : IPaymentProvider
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration _configuration;
        public EsewaPaymentProvider(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _configuration = configuration;
        }

        

        public List<PaymentTransaction> GetPaymentTransactions()
        {
            var txn = _unitOfWork.PaymentTransactions.GetAll().ToList();
            return txn;
        }
        public async Task<bool> IsTransactionValid(PaymentParameters parameters)
        {
            //vist ko charge
            decimal amountInOurSystem = AmountChargedForVisit(parameters.VisitId);
               
            var queryParams = new Dictionary<string, string>()
            {
                {"amt", amountInOurSystem.ToString() }, // amount of our product/service saved in our system
                {"scd", _configuration["Payment:EsewaMerchantId"]},                // merchant code provided by Esewa
                {"pid", parameters.VisitId},            // A unique ID of product or item or ticket etc generated by merchant for payment
                {"rid", parameters.ProviderReferenceId} // A unique payment reference code from eSewa generated on SUCCESSFUL transaction
            };

            string verificationUrl = _configuration["Payment:EsewaPaymentVerificationUrl"];
            string verificationUrlWithQueryParams = QueryHelpers.AddQueryString(verificationUrl, queryParams);
            HttpClient httpClient = new HttpClient();
            var result = await httpClient.GetAsync(verificationUrlWithQueryParams);
            if (result.IsSuccessStatusCode)
            {
                string content = await result.Content.ReadAsStringAsync();
                if (content.Contains("Success", StringComparison.InvariantCultureIgnoreCase))
                {
                    return true;
                }
                else if (content.Contains("failure", StringComparison.InvariantCultureIgnoreCase))
                {
                    return false;
                }
            }

            return false;
        }

        public void SaveTransaction(PaymentParameters parameters)
        {
            PaymentTransaction paymentTransaction = new PaymentTransaction
            {
                Amount = Convert.ToDecimal(parameters.Amount),
                PaymentMethod = PaymentMethod.Esewa,
                PaymentStatus = PaymentStatus.Complete,
                TransactionDateTime = DateTime.Now,
                VisitId = Guid.Parse(parameters.VisitId)
            };
            _unitOfWork.PaymentTransactions.Add(paymentTransaction);

            EsewaTransaction esewaTransaction = new EsewaTransaction
            {
                ReferenceId = parameters.ProviderReferenceId,
                TransactionId = paymentTransaction.TransactionId
            };
            _unitOfWork.EsewaTransactions.Add(esewaTransaction);
            Visit visit = _unitOfWork.Visits.GetSingleOrDefault(v => v.VisitId == Guid.Parse(parameters.VisitId));
            visit.PaymentStatus = "paid";
            _unitOfWork.Visits.UpdateProperties(visit, v => v.PaymentStatus);
            //save all in one transaction
            _unitOfWork.SaveChanges();
        }

        private decimal AmountChargedForVisit(string visitId)
        {
            return _unitOfWork.Visits.FindByIncluding(v => v.VisitId == Guid.Parse(visitId), v => v.HospitalDoctorMap)
                                     .Select(v => v.HospitalDoctorMap.Charge)
                                     .FirstOrDefault();
        }
    }
}
