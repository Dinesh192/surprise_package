﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestTele.Service.PaymentService
{
    public class PaymentParameters
    {
        public string Amount { get; set; }
        public string ProviderReferenceId { get; set; }
        public string VisitId { get; set; }
    }
}
