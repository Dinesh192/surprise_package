﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Telemedicine.DalLayer.Repositories.Interface;
using Telemedicine.DALLayer;
using Telemedicine.ServerModel;

namespace TestTele.Service.NotificationService
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class NotificationHub : Hub<INotificationHub>
    {
        public class ConnectedUser
        {
            public Guid UserGuid { get; set; }
            public string UserSignalRId { get; set; }
            //public Guid ReceiverGuid { get; set; }
            //public string ReceiverSignalRId { get; set; }
        }

        private static List<ConnectedUser> _connectedUsers = new List<ConnectedUser>();
        public override Task OnConnectedAsync()
        {
            ConnectedUser newConnectedUser = new ConnectedUser
            {
                UserGuid = Guid.Parse(Context.User.FindFirst(ClaimTypes.Name).Value),
                UserSignalRId = Context.ConnectionId
            };
            _connectedUsers.Add(newConnectedUser);
            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            var userToRemove = _connectedUsers.FirstOrDefault(x => x.UserGuid == Guid.Parse(Context.User.FindFirst(ClaimTypes.Name).Value));
            _connectedUsers.Remove(userToRemove);
            return base.OnDisconnectedAsync(exception);
        }

        public async Task SendChatMessage(ChatMessage chatMessage)
        {
            var messageRecipientSignalRId = _connectedUsers.FirstOrDefault(x => x.UserGuid == Guid.Parse(chatMessage.ReceiverId)).UserSignalRId;

            var chatMsgJsonStr = JsonConvert.SerializeObject(chatMessage);
            await Clients.Client(messageRecipientSignalRId).ChatMessage(chatMsgJsonStr);
        }



        //    public async Task SendChatMessage1(ChatMessage chatMessage)
        //    {
        //        var userClaims = Context.User.Claims;
        //        var connId = "";
        //        ChatMessage chatMsg = new ChatMessage();
        //        if (chatMessage.IsDoctor)
        //        {
        //            connId = Context.ConnectionId;
        //            chatMsg.IsDoctor = false;
        //            chatMsg.Name = chatMessage.Name;
        //            chatMsg.Id = chatMessage.Id;
        //        }
        //        else
        //        {
        //            connId = Context.ConnectionId;
        //            chatMsg.IsDoctor = true;
        //            chatMsg.Name = chatMessage.Name;
        //            chatMsg.Id = chatMessage.Id;
        //        }
        //        chatMsg.Message = chatMessage.Message;

        //        var chatMsgJsonStr = JsonConvert.SerializeObject(chatMsg);
        //        await this.Clients.All.ChatMessage(chatMsgJsonStr);
        //    }

        //}


        public class Message
        {
            public string Type { get; set; }
            public string Payload { get; set; }
        }
    }
}

       
    


