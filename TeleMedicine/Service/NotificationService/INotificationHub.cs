﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestTele.Service.NotificationService
{
    public interface INotificationHub
    {
        Task ChatMessage(string message);

    }
}
