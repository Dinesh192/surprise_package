﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telemedicine.ServerModel;
using Telemedicine.ServerModel.Interfaces;
using TestTele.ViewModel;

namespace TestTele.AutoMapper
{
    public class DomainProfile : Profile
    {
        public DomainProfile()
        {
            CreateMap<Patient, PatientViewModel>().ForMember(des => des.PhoneNumber,opt => opt.MapFrom(src => src.ContactNumber));
            CreateMap<PatientViewModel, Patient>().ForMember(des => des.ContactNumber, opt => opt.MapFrom(src => src.PhoneNumber));
            CreateMap<Hospital, HospitalViewModel>().ReverseMap(); 
            CreateMap< VisitViewModel, Visit>();
            CreateMap<VisitViewModel, Visit>().ReverseMap();
            CreateMap<ProblemViewModel, Problem >(); 
            CreateMap<Problem ,ProblemViewModel >();
            CreateMap<PatientFilesViewModel, PatientFilesModel>();
            CreateMap<PatientFilesViewModel, PatientFilesModel>().ReverseMap();
            CreateMap<HospitalDoctorMapViewModel, HospitalDoctorMap>();
            CreateMap<HospitalDoctorMapViewModel, HospitalDoctorMap>().ReverseMap();
            CreateMap< DoctorViewModel , Doctor>().ReverseMap();
            CreateMap<PatientViewModel, Patient>().ReverseMap();
            CreateMap<DepartmentViewModel, Department>();
            CreateMap<DepartmentViewModel, Department>().ReverseMap();
            CreateMap<PatientFilesUploadViewModel, PatientFilesUpload>();
            CreateMap<PatientFilesUpload, PatientFilesUploadViewModel>().ReverseMap();
            CreateMap<SchedulingViewModel, DoctorScheduling>();
            CreateMap<DoctorScheduling, SchedulingViewModel>();
            CreateMap<ScheduleInterval, ScheduleIntervalViewModel>();
            CreateMap<ScheduleInterval, ScheduleIntervalViewModel>().ReverseMap();
            CreateMap<Qualification, QualificationViewModel>().ReverseMap();


        }
    }
}
