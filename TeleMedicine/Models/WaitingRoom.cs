﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestTele.Models
{
    public class WaitingRoom
    {
        public List<Patients> Patients { get; set; }
        public WaitingRoom()
        {
            this.Patients = new List<Patients>();
        }
    }
}
