﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Telemedicine.ServerModel;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace Telemedicine.ServerModel
{
    public class DoctorScheduling
    {
        [Key]
        public Guid SchedulingId { get; set; }
        [Column(TypeName = "date")]
        public DateTime Date { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
        public int AccomodatedPatient { get; set; }
        public Guid HospitalDoctorMapId { get; set; }
        public virtual HospitalDoctorMap HospitalDoctorMap { get; set; }
    }
   
}
