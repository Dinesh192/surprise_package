﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Telemedicine.ServerModel;

namespace Telemedicine.ServerModel
{
    public class HospitalDoctorMap : AuditableEntity
    {
        public Guid HospitalDoctorMapId { get; set; }
        public Guid DoctorId { get; set; }
        public Guid? HospitalId { get; set; }
        public virtual Hospital Hospital { get; set; }
        public Guid? DepartmentId { get; set; }
        public virtual Department Department { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Charge { get; set;  }
        public string ConsultationType { get; set; }
        public bool IsActive { get; set; }
    }
}
