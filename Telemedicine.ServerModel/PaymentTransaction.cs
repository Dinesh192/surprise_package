﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Telemedicine.ServerModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Telemedicine.ServerModel
{
    public class PaymentTransaction
    {
        [Key]
        public Guid TransactionId { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public DateTime TransactionDateTime { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Amount { get; set; }
        public Guid VisitId { get; set; }
        public virtual Visit Visit { get; set; }
        public PaymentStatus PaymentStatus { get; set; }
    }

}
