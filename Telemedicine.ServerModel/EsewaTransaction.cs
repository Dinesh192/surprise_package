﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Telemedicine.ServerModel
{
    public class EsewaTransaction
    {
        public Guid EsewaTransactionId { get; set; }
        public Guid TransactionId { get; set; }
        public virtual PaymentTransaction Transaction { get; set; }
        public string ReferenceId { get; set; }  // A unique payment reference code from eSewa generated on SUCCESSFUL transaction
    }
}
