﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Telemedicine.ServerModel;

namespace Telemedicine.ServerModel
{
     public class PatientFilesUpload : AuditableEntity
    {
        [Key]
        public Guid PatientFileId { get; set; }
        public Guid PatientId { get; set; }
        public Guid VisitId { get; set; }
        public string FileType { get; set; }
        public byte[] FileBinaryData { get; set; }
        public int FileNo { get; set; }
        public string FileName { get; set; }
        public string FileExtention { get; set; }
        public string FileKind { get; set; }
        //public virtual Visit VisitDetails { get; set; }
        public string Title { get; set; }
        public string Comment { get; set; }
        public string FilePath { get; set; }
     

        [NotMapped]
        public string FileBase64String { get; set; }
        public virtual ICollection<Visit> Visit { get; set; }
        public PatientFilesUpload()
        {
            Visit = new List<Visit>();
           
        }

    }
}
