﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
namespace Telemedicine.ServerModel
{
    public class Visit : AuditableEntity
    {
        [Key]
        public Guid VisitId { get; set; }
        public DateTime VisitDate { get; set; }
        public Guid ProviderId { get; set; }
        public DateTime? VisitStartTime { get; set; }
        public DateTime? VisitEndTime { get; set; }
        public string BookingTime { get; set; }
        public bool? IsActive { get; set; }
        public string Status { get; set; }  
        public Guid PatientId { get; set; }
        public  string TreatmentAdvice { get; set; }
        public string Medication { get; set; }
        public string FollowUp { get; set; }

        public DateTime LastUpdateTime { get; set; }
        public string VisitType { get; set; }
        public string PaymentStatus { get; set; }
        public virtual Patient Patient { get; set; }
        public Guid HospitalId { get; set; }
        public virtual Hospital Hospital{ get; set; }
        public Guid DepartmentId { get; set; }
        public Guid? SchedulingId { get; set; }
        public Guid HospitalDoctorMapId { get; set; }
        public virtual HospitalDoctorMap HospitalDoctorMap { get; set; }
        public Guid ConsultationId { get; set; }
        public virtual Consultation Consultation { get; set; }
        public bool IsConversationCompleted { get; set; }

    }
}
