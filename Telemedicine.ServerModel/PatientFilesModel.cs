﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Telemedicine.ServerModel;

namespace Telemedicine.ServerModel.Interfaces
{
    public class PatientFilesModel : AuditableEntity
    {
        [Key]
        public Guid PatientFileId { get; set; }
        public Guid PatientId { get; set; }
        public Guid VisitId { get; set; }
        public string FileType { get; set; }
        public string Title { get; set; }
        public DateTime UploadedOn { get; set; }
        public int UploadedBy { get; set; }
        public string Description { get; set; }
        public byte[] FileBinaryData { get; set; }
        public int FileNo { get; set; }
        public string FileName { get; set; }
        public string FileExtention { get; set; }

        public bool? IsActive { get; set; }

        
        public string FileBase64String { get; set; }

        public bool? HasFile { get; set; }

    }
}
