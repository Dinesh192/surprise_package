﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Telemedicine.ServerModel
{
    public enum PaymentMethod
    {
        Esewa = 1,
        Khalti = 2,
        NabilCard = 3
    }
    public enum PaymentStatus
    {
        Pending = 0,
        Complete = 1
    }

    public enum NabilPaymentStatus
    {
        Pending = 0,
        Approved = 1, // complete
        Declined = 2,
        Cancel = 3
    }
}
