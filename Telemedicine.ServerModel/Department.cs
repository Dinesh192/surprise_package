﻿using System;
using System.Collections.Generic;
using System.Text;
using Telemedicine.ServerModel;

namespace Telemedicine.ServerModel
{
   public class Department: AuditableEntity
    {
        public Guid DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string DepartmentShortName { get; set; }
        public string Description { get; set; }

        public bool IsActive { get; set; }
    }

}
