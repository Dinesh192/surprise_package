﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Telemedicine.ServerModel;

namespace Telemedicine.ServerModel
{
    public class Doctor : AuditableEntity
    {
        public Guid DoctorId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string NMC { get; set; }
        public string LongSignature { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string FileExtention { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string MailingAddress { get; set; }
        public string Gender { get; set; }
        public string PhoneNumber { get; set; }
        public bool EnablePhNo { get; set; }
        public string Address { get; set; }
       // public string Location { get; set; }
        //public string Department { get; set; }
        public int? HospitalId { get; set; }
        public Guid? DepartmentId { get; set; }
        public virtual Department Department { get; set; }
        public bool? IsActive { get; set; }
        public string IdentityUserId { get; set; }
        [NotMapped]
        public string SignalRConnectionId { get; set; }
        public virtual ApplicationUser IdentityUser { get; set; }
        public string HospitalIdentifier { get; set; }
        public virtual List<Qualification> Qualifications { get; set; }
        public Guid? DoctorRoomName { get; set; }
    }
}
