﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Telemedicine.ServerModel;

namespace Telemedicine.ServerModel
{
    public class ScheduleInterval 
    {
        [Key]
        public Guid ScheduleIntervalId { get; set; }
        public string StartTime { get; set; }
        public bool IsActive { get; set; }
        public DateTime Date { get; set; }
        public string EndTime { get; set; }
        public bool IsBooked { get; set; }
        public Guid? VisitId { get; set; }
        public virtual Visit VisitDetails { get; set; }
        public Guid SchedulingId { get; set; }
        public Guid HospitalDoctorMapId { get; set; }
    }

}

