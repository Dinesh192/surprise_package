﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
namespace Telemedicine.ServerModel
{
    public class Consultation
    {
        private Guid? _hospitalId;
        private Guid? _doctorId;
        public Guid ConsultationId { get; set; }
        public ConsultationType Type { get; set; }
        public Guid? DoctorId
        {
            get { return _doctorId; }
            set
            {
                _doctorId = value;
                if (_doctorId == null)
                {
                    Type = ConsultationType.HospitalGeneric;
                }
                else
                {
                    if (_hospitalId != null)
                        Type = ConsultationType.HospitalDoctorSpecific; // doctor pani cha hospital pani cha 
                    else
                        Type = ConsultationType.DoctorSelf;
                }
            }
        }
        public virtual Doctor Doctor { get; set; }
        public Guid? HospitalId
        {
            get { return _hospitalId; }
            set
            {
                _hospitalId = value;
                if (_hospitalId == null)
                {
                    Type = ConsultationType.DoctorSelf; // hospital chaina
                }
                else
                {
                    if (_doctorId != null)
                        Type = ConsultationType.HospitalDoctorSpecific; // hospital ni cha doctor ni cha 
                    else
                        Type = ConsultationType.HospitalGeneric; // hosital cha doctor chaina
                }
            }
        }
        public virtual Hospital Hospital { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Charge { get; set; }
        public string Description { get; set; }
    }

    public enum ConsultationType
    {
        DoctorSelf = 1,
        HospitalDoctorSpecific = 2,
        HospitalGeneric = 3
    }
}
