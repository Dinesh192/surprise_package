﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Telemedicine.ServerModel
{
    public class User : AuditableEntity
    {
        [Key]
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        
        public string UserType { get; set; }
        public string Department { get; set; }

       
    }
    public class ChatMessage
    {
        [NotMapped]
        public string SenderId { get; set; }
        [NotMapped]
        public string ReceiverId { get; set; }
        [NotMapped]
        public string Name { get; set; }
        [NotMapped]
        public string Message { get; set; }
    }

}
