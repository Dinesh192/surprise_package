﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Telemedicine.ServerModel
{
    public class Problem : AuditableEntity
    {
        [Key]
        public Guid ProblemId { get; set; }     
        public bool Fever { get; set; }
        public bool Cough { get; set; }
        public bool BreathingDifficulty { get; set; }
        public bool Tiredness { get; set; }
        public bool SoreThroat { get; set; }
        public bool Bodyache { get; set; }
        public bool ChestPain { get; set; }
        public bool Diarrhea { get; set; }
        public string AnyOtherSymptoms { get; set; }
        public bool HeartDisease { get; set; }
        public bool HighBloodPressure { get; set; }
        public bool Diabetes { get; set; }
        public bool Copd { get; set; }
        public bool Transplant { get; set; }
        public bool RecentTravel { get; set; }
        public bool Cancer { get; set; }
        public bool Exposure { get; set; }

        public string OtherPertientInformation { get; set; }
      
        public Guid VisitId { get; set; }
     
        public virtual  Visit VisitDetails { get; set; }
        //public Guid PatientId { get; set; }
       
        // virtual Patient Patient { get; set; }

    }
}
