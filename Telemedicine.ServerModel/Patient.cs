﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Telemedicine.ServerModel;

namespace Telemedicine.ServerModel
{
    public class Patient : AuditableEntity
    {
        [Key]
        public Guid PatientId { get; set; }
        public int RegistrationNumber { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string ShortName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string MailingAddress { get; set; }
        public string ContactNumber { get; set; }
        public string Address { get; set; }
        public bool? IsActive { get; set; }
        public string IdentityUserId { get; set; }

        [NotMapped]
        public string SignalRConnectionId { get; set; }
        public string HospitalIdentifier { get; set; }
        public virtual ApplicationUser IdentityUser { get; set; }
        public virtual List<Visit> VisitDetails { get; set; }
       

    }
}
