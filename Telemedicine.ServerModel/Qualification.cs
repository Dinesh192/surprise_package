﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata;
using System.Runtime;
using System.Text;

namespace Telemedicine.ServerModel
{
    public class Qualification : AuditableEntity
    {
        public Guid QualificationId { get; set; }
        public string Designation { get; set; }
        public string Education { get; set; }
        public string PastAffiliation { get; set; }
        public string Experience { get; set; }
        public string Membership { get; set; }
        public Guid DoctorId { get; set; }

    }
}
