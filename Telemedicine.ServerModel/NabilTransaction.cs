﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Telemedicine.ServerModel
{
    public class NabilTransaction
    {
        public Guid NabilTransactionId { get; set; }
        public Guid TransactionId { get; set; }
        public virtual PaymentTransaction Transaction { get; set; }
        public NabilPaymentStatus PaymentStatus { get; set; }
        public DateTime TransactionDateTime { get; set; }
        public string OrderId { get; set; }
        public string SessionId { get; set; }
        public string CreateOrderRequest { get; set; }
        public string CreateOrderResponse { get; set; }
        public string ResponseXMLOut { get; set; }
        public string GetOrderStatusRequest { get; set; }
        public string GetOrderStatusResponse { get; set; }
    }
}
