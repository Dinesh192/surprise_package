﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Telemedicine.DalLayer.Migrations
{
    public partial class _0920enable_phno : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "EnablePhNo",
                table: "Doctors",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EnablePhNo",
                table: "Doctors");
        }
    }
}
