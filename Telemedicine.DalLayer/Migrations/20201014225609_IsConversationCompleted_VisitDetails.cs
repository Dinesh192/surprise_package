﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Telemedicine.DalLayer.Migrations
{
    public partial class IsConversationCompleted_VisitDetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsConversationCompleted",
                table: "VisitDetails",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsConversationCompleted",
                table: "VisitDetails");
        }
    }
}
