﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Telemedicine.DalLayer.Migrations
{
    public partial class _10th_Oct : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "HospitalIdentifier",
                table: "Hospitals",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "DoctorRoomName",
                table: "Doctors",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HospitalIdentifier",
                table: "Hospitals");

            migrationBuilder.DropColumn(
                name: "DoctorRoomName",
                table: "Doctors");
        }
    }
}
