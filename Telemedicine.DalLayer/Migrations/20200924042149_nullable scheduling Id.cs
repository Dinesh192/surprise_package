﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Telemedicine.DalLayer.Migrations
{
    public partial class nullableschedulingId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Location",
                table: "Doctors");

            migrationBuilder.AlterColumn<Guid>(
                name: "SchedulingId",
                table: "VisitDetails",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<Guid>(
                name: "SchedulingId",
                table: "VisitDetails",
                type: "uniqueidentifier",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Location",
                table: "Doctors",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
