﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Telemedicine.DalLayer.Migrations
{
    public partial class HospitalIdentifier_added : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "HospitalIdentifier",
                table: "Patients",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "EnablePhNo",
                table: "Doctors",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "HospitalIdentifier",
                table: "Doctors",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HospitalIdentifier",
                table: "Patients");

            migrationBuilder.DropColumn(
                name: "EnablePhNo",
                table: "Doctors");

            migrationBuilder.DropColumn(
                name: "HospitalIdentifier",
                table: "Doctors");
        }
    }
}
