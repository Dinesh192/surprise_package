﻿
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Telemedicine.ServerModel;
using Telemedicine.ServerModel.Interfaces;
//using TestTele.Models;

namespace Telemedicine.DALLayer
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options,
                                    IHttpContextAccessor httpContextAccessor) : base(options)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public DbSet<Patient> Patients { get; set; }
        public DbSet<Doctor> Doctors { get; set; }
        public DbSet<Problem> Problems { get; set; }
        //public DbSet<User> UserRegistration { get; set; }
        public DbSet<Visit> VisitDetails { get; set; }
        public DbSet<Hospital> Hospitals { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<PaymentTransaction> Transactions { get; set; }
        public DbSet<EsewaTransaction> EsewaTransactions { get; set; }
        public DbSet<NabilTransaction> NabilTransactions { get; set; }
        public DbSet<Consultation> Consultations { get; set; }
        public DbSet<PatientFilesUpload> PatientFiles { get; set; }
        public DbSet<HospitalDoctorMap> HospitalDoctorMaps { get; set; }
        public DbSet<DoctorScheduling> DoctorSchedulings { get; set; }
        public DbSet<Qualification> Qualifications { get; set; }
        public DbSet<ScheduleInterval> ScheduleIntervals { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Patient>()
                .HasMany(x => x.VisitDetails)
                .WithOne(y => y.Patient)
                .HasForeignKey(z => z.PatientId);


            modelBuilder.Entity<Patient>()
                   .HasOne(p => p.IdentityUser);

            modelBuilder.Entity<Doctor>()
                .HasOne(d => d.IdentityUser);

            modelBuilder.Entity<Doctor>()
                .HasOne(x => x.Department);

            modelBuilder.Entity<PaymentTransaction>()
                .HasOne(t => t.Visit);



            modelBuilder.Entity<EsewaTransaction>()
               .HasOne(t => t.Transaction);

            modelBuilder.Entity<NabilTransaction>()
                .HasOne(t => t.Transaction)
                .WithMany();



            //modelBuilder.Entity<Visit>()
            //    .HasOne<Problem>(a => a.Problem)

            //    .HasForeignKey<Visit>(b => b.VisitId);
            //  modelBuilder.Entity<Visit>()
            // .HasOne(x => x.Problem)
            //.WithMany()
            //.HasForeignKey(y => y.ProblemId);

            base.OnModelCreating(modelBuilder);
        }




        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            SaveAuditableEntities();
            return base.SaveChangesAsync(cancellationToken);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            SaveAuditableEntities();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }
        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            SaveAuditableEntities();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }
        public override int SaveChanges()
        {
            SaveAuditableEntities();
            return base.SaveChanges();
        }

        private void SaveAuditableEntities()
        {
            ClaimsPrincipal user = _httpContextAccessor?.HttpContext?.User;
            var modifiedEntities = ChangeTracker.Entries().Where(e => e.Entity is AuditableEntity).ToList();

            foreach (var entity in modifiedEntities)
            {
                if (entity.State == EntityState.Added)
                {
                    //entity.Properties.First(p => p.Metadata.Name == "CreatedBy").CurrentValue ??= user?.Claims.First(c => c.Type == ClaimTypes.Name).Value;
                    entity.Properties.First(p => p.Metadata.Name == "CreatedDate").CurrentValue = DateTime.Now;
                }

                //if (entity.State == EntityState.Modified)
                //{
                //    entity.Properties.First(p => p.Metadata.Name == "UpdatedBy").CurrentValue = user?.Claims.First(c => c.Type == ClaimTypes.Name).Value;
                //    entity.Properties.First(p => p.Metadata.Name == "UpdatedDate").CurrentValue = DateTime.Now;
                //}
            }
        }
    }
}
