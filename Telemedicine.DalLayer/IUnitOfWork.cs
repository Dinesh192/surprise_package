﻿using System;
using System.Collections.Generic;
using System.Text;
using Telemedicine.DalLayer.Repositories.Interface;

namespace Telemedicine.DalLayer
{
    public interface IUnitOfWork
    {
        IPatientRepository Patients { get; }
        IProblemRepository Problems { get; }
        IVisitRepository Visits { get; }
        IUserRepository Users { get; }
        IDoctorRepository Doctors { get; }
        IHospitalRepository Hospitals { get; }
        IDepartmentRepository Departments { get; }
        IPaymentTransactionRepository PaymentTransactions { get; }
        IEsewaTransactionRepository EsewaTransactions { get; }
        INabilTransactionRepository NabilTransactions { get; }
        IConsultationRepository Consultations { get; }
        IPatientFilesUploadRepository PatientFilesUpload { get; }
        IHospitalDoctorMapRepository HospitalDoctorMaps { get; }
        IDoctorSchedulingRepository DoctorSchedulings { get; }
        IQualificationRepository Qualification { get; }
        IScheduleIntervalRepository ScheduleInterval { get; }
        int SaveChanges();
    }
}