﻿using System;
using System.Collections.Generic;
using System.Text;
using Telemedicine.DalLayer.Repositories;
using Telemedicine.DalLayer.Repositories.Interface;
using Telemedicine.DALLayer;

namespace Telemedicine.DalLayer
{
    public class UnitOfWork : IUnitOfWork
    {
        readonly ApplicationDbContext _context;
        IPatientRepository _patients;
        IProblemRepository _problems;
        IVisitRepository _visits;
        IUserRepository _users;
        IDoctorRepository _doctors;
        IHospitalRepository _hospitals;
        IDepartmentRepository _departments;
        IPaymentTransactionRepository _paymentTransactions;
        IConsultationRepository _consultationRepository;
        IEsewaTransactionRepository _esewaTransactions;
        INabilTransactionRepository _nabilTransactions;
        IPatientFilesUploadRepository _patientfilesupload;
        IHospitalDoctorMapRepository _hospitalDoctors;
        IDoctorSchedulingRepository _doctorSchedulings;
        IQualificationRepository _qualifications;
        IScheduleIntervalRepository _scheduleinterval;

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
        }
        public IPatientRepository Patients
        {
            get
            {
                if (_patients == null)
                    _patients = new PatientRepository(_context);

                return _patients;
            }
        }
        public IProblemRepository Problems
        {
            get
            {
                if (_problems == null)
                    _problems = new ProblemRepository(_context);

                return _problems;
            }
        }

        public IVisitRepository Visits
        {
            get
            {
                if (_visits == null)
                    _visits = new VisitRepository(_context);

                return _visits;
            }
        }
        public IUserRepository Users
        {
            get
            {
                if (_users == null)
                    _users = new UserRepository(_context);

                return _users;
            }
        }
        public  IDoctorRepository Doctors
        {
            get
            {
                if (_doctors == null)
                    _doctors = new DoctorRepository(_context);

                return _doctors;
            }
        }
        public IHospitalRepository Hospitals
        {
            get
            {
                if (_hospitals == null)
                    _hospitals = new HospitalRepository(_context);

                return _hospitals;
            }
        }
        public IPatientFilesUploadRepository PatientFilesUpload
        {
            get
            {
                if (_patientfilesupload == null)
                    _patientfilesupload = new PatientFilesUploadRepository(_context);

                return _patientfilesupload;
            }
        }
        public IDepartmentRepository Departments
        {
            get
            {
                if (_departments == null)
                    _departments = new DepartmentRepository(_context);

                return _departments;
            }
        }
        public IPaymentTransactionRepository PaymentTransactions
        {
            get
            {
                if (_paymentTransactions == null)
                    _paymentTransactions = new PaymentTransactionRepository(_context);

                return _paymentTransactions;
            }
        }
        public IEsewaTransactionRepository EsewaTransactions
        {
            get
            {
                if (_esewaTransactions == null)
                    _esewaTransactions = new EsewaTransactionRepository(_context);

                return _esewaTransactions;
            }
        }
        public INabilTransactionRepository NabilTransactions
        {
            get
            {
                if (_nabilTransactions == null)
                    _nabilTransactions = new NabilTransactionRepository(_context);

                return _nabilTransactions;
            }
        }
        public IConsultationRepository Consultations
        {
            get
            {
                if (_consultationRepository == null)
                    _consultationRepository = new ConsultationRepository(_context);

                return _consultationRepository;
            }
        }

        
        public IHospitalDoctorMapRepository HospitalDoctorMaps
        {
            get
            {
                if (_hospitalDoctors == null)
                    _hospitalDoctors = new HospitalDoctorMapRepository(_context);

                return _hospitalDoctors;
            }
        }
        public IDoctorSchedulingRepository DoctorSchedulings
        {
            get
            {
                if (_doctorSchedulings == null)
                    _doctorSchedulings = new DoctorSchedulingRepository(_context);

                return _doctorSchedulings;
            }
        }
        public IQualificationRepository Qualification
        {
            get
            {
                if (_qualifications == null)
                    _qualifications = new QualificationRepository(_context);

                return _qualifications;
            }
        }
        public IScheduleIntervalRepository ScheduleInterval
        {
            get
            {
                if (_scheduleinterval == null)
                    _scheduleinterval = new ScheduleIntervalRepository(_context);

                return _scheduleinterval;
            }
        }
        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

    }
}
