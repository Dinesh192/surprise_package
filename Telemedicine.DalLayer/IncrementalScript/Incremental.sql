﻿GO
ALTER TABLE [VisitDetails] ALTER COLUMN [SchedulingId] uniqueidentifier NULL;
GO
ALTER TABLE [Doctors] ADD [EnablePhNo] bit NOT NULL DEFAULT CAST(0 AS bit);
GO
ALTER TABLE [Patients] ADD [HospitalIdentifier] nvarchar(max) NULL;
GO
ALTER TABLE [Doctors] ADD [HospitalIdentifier] nvarchar(max) NULL;
GO
ALTER TABLE [Hospitals] ADD [HospitalIdentifier] nvarchar(max) NULL;
GO
--Start for MEDICITI Hospital Database
GO
update Patients set HospitalIdentifier = 'mediciti'
update Doctors set HospitalIdentifier = 'mediciti'
update Hospitals set HospitalIdentifier = 'mediciti'
GO
insert into AspNetUserClaims
(UserId, ClaimType, ClaimValue)
select Id,'HospitalIdentifier','mediciti' from AspNetUsers as users
where not exists (
    select *
    from AspNetUserClaims
    where UserId = users.Id and ClaimType = 'HospitalIdentifier' and ClaimValue='mediciti'
)
GO
--End for MEDICITI Hospital Database

GO

ALTER TABLE [Doctors] ADD [DoctorRoomName] uniqueidentifier NULL;

GO

--Date: 10/14/2020 by Deepak
ALTER TABLE [VisitDetails] ADD [IsConversationCompleted] bit NOT NULL DEFAULT CAST(0 AS bit);

GO

---Date:20th_Nov_2020

alter table HospitalDoctorMaps
drop column  Isactive 
go