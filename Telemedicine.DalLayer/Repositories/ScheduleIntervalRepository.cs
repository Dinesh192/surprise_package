﻿using System;
using System.Collections.Generic;
using System.Text;
using Telemedicine.DalLayer.Repositories.Interface;
using Telemedicine.DALLayer;
using Telemedicine.ServerModel;

namespace Telemedicine.DalLayer.Repositories
{
    public class ScheduleIntervalRepository : Repository<ScheduleInterval>, IScheduleIntervalRepository
    {
        public ScheduleIntervalRepository(ApplicationDbContext context) : base(context)
        {

        }
    }
}