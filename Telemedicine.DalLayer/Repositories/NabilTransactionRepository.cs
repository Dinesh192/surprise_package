﻿using System;
using System.Collections.Generic;
using System.Text;
using Telemedicine.DalLayer.Repositories.Interface;
using Telemedicine.DALLayer;
using Telemedicine.ServerModel;

namespace Telemedicine.DalLayer.Repositories
{
    public class NabilTransactionRepository : Repository<NabilTransaction>, INabilTransactionRepository
    {
        public NabilTransactionRepository(ApplicationDbContext context) : base(context)
        {

        }
    }
}
