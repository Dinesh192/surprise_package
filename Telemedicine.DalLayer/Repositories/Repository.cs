﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;
using Telemedicine.DalLayer.Interfaces;

namespace Telemedicine.DalLayer.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly DbContext _context;
        protected readonly DbSet<TEntity> _entities;

        public Repository(DbContext context)
        {
            _context = context;
            _entities = context.Set<TEntity>();
        }

        public virtual async Task<TEntity> AddAsync(TEntity entity)
        {
            await _entities.AddAsync(entity);

            return entity;
        }
        public virtual async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await _entities.ToListAsync();
        }
        public virtual async Task<IEnumerable<TEntity>> GetAllAsync(int id)
        {
            return await _entities.ToListAsync();
        }
        public virtual void Add(TEntity entity)
        {
            _context.Add(entity);
        }
        public virtual void AddRange(IEnumerable<TEntity> entities)
        {
            _entities.AddRange(entities);
        }


        public virtual void Update(TEntity entity)
        {
            _entities.Update(entity);
        }

        public virtual void UpdateProperties(TEntity entity, params Expression<Func<TEntity, object>>[] updatedProperties)
        {
            _entities.Attach(entity);
            if (updatedProperties.Any())
            {
                foreach (var property in updatedProperties)
                {
                    _context.Entry(entity).Property(property).IsModified = true;
                }
            }
        }


        public virtual void UpdateRange(IEnumerable<TEntity> entities)
        {
            _entities.UpdateRange(entities);
        }



        public virtual void Remove(TEntity entity)
        {
            _entities.Remove(entity);
        }

        public virtual void RemoveRange(IEnumerable<TEntity> entities)
        {
            _entities.RemoveRange(entities);
        }


        public virtual int Count()
        {
            return _entities.Count();
        }


        public virtual IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return _entities.Where(predicate);
        }

        public virtual TEntity GetSingleOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return _entities.SingleOrDefault(predicate);
        }

        public virtual TEntity Get(int id)
        {
            return _entities.Find(id);
        }
        public virtual TEntity GetGuid(Guid id)
        {
            return _entities.Find(id);

        }
        public virtual async Task<TEntity> GetGuidAsync(Guid id)
        {
            return await _entities.FindAsync(id);
        }
        public virtual IQueryable<TEntity> GetAll()
        {
            return _entities;
        }

        public IEnumerable<TEntity> GetAllIncluding(params Expression<Func<TEntity,object>>[] includeProperties)
        {
            return GetAllIncludingPrivate(includeProperties).ToList();
        }

        public IEnumerable<TEntity> FindByIncluding(Expression<Func<TEntity,bool>> predicate, params Expression<Func<TEntity,object>>[] includeProperties)
        {
            IQueryable<TEntity> query = GetAllIncludingPrivate(includeProperties);
            IEnumerable<TEntity> results = query.Where(predicate).ToList();
            return results;
        }


        private IQueryable<TEntity> GetAllIncludingPrivate(params Expression<Func<TEntity,object>>[] includeProperties)
        {
            IQueryable<TEntity> queryable = _entities;
            return includeProperties.Aggregate(queryable,(current,includeProperty) => current.Include(includeProperty));
        }
    }
}
