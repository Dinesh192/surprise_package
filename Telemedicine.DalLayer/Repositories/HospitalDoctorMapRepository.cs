﻿using System;
using System.Collections.Generic;
using System.Text;
using Telemedicine.DalLayer.Repositories.Interface;
using Telemedicine.DALLayer;
using Telemedicine.ServerModel;

namespace Telemedicine.DalLayer.Repositories
{
    public class HospitalDoctorMapRepository : Repository<HospitalDoctorMap>, IHospitalDoctorMapRepository
    {
        public HospitalDoctorMapRepository(ApplicationDbContext context) : base(context)
        {

        }
    }
}
