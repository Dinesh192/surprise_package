﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Telemedicine.DalLayer.Interfaces;
using Telemedicine.DALLayer;
using Telemedicine.ServerModel;

namespace Telemedicine.DalLayer.Repositories.Interface
{
    public interface IQualificationRepository : IRepository<Qualification>
    {
        
    }
}
