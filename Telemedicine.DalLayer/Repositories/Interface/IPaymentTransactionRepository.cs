﻿using System;
using System.Collections.Generic;
using System.Text;
using Telemedicine.DalLayer.Interfaces;
using Telemedicine.ServerModel;

namespace Telemedicine.DalLayer.Repositories.Interface
{
    public interface IPaymentTransactionRepository: IRepository<PaymentTransaction>
    {
    }
}
