﻿using System;
using System.Collections.Generic;
using System.Text;
using Telemedicine.DALLayer;
using Telemedicine.ServerModel;

namespace Telemedicine.DalLayer.Repositories.Interface
{
    public class ConsultationRepository : Repository<Consultation>, IConsultationRepository
    {
        public ConsultationRepository(ApplicationDbContext context) : base(context)
        {


        }
    }
}
