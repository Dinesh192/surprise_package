﻿using System;
using System.Collections.Generic;
using System.Text;
using Telemedicine.DalLayer.Repositories.Interface;
using Telemedicine.DALLayer;
using Telemedicine.ServerModel.Interfaces;
using Telemedicine.ServerModel;

namespace Telemedicine.DalLayer.Repositories
{
    public class PatientFilesRepository : Repository<PatientFilesModel>, IPatientFilesRepository
    {
        public PatientFilesRepository(ApplicationDbContext context) : base(context)
        {


        }
    }
}
