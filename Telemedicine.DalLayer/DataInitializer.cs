﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Telemedicine.ServerModel;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Telemedicine.DalLayer
{
    public static class DataInitializer
    {
        public async static Task Initialize(Telemedicine.DALLayer.ApplicationDbContext context, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            context.Database.EnsureCreated();

            if (!(await roleManager.RoleExistsAsync("Admin")))
            {
                await roleManager.CreateAsync(new IdentityRole("Admin"));
            }
            if ((await userManager.FindByNameAsync("1234567890")) == null)
            {

                var adminUser = new ApplicationUser
                {
                    UserName = "1234567890",
                    Email = "admin@telemedicine.com",
                    EmailConfirmed = true,
                };
                var result = await userManager.CreateAsync(adminUser, "Imark@123");
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(adminUser, "Admin");
                }
            }
            if ((await userManager.FindByNameAsync("2020000002")) == null)
            {

                var adminUser = new ApplicationUser
                {
                    UserName = "2020000002",
                    Email = "mikc@telemedicine.com",
                    EmailConfirmed = true,
                };
                var result = await userManager.CreateAsync(adminUser, "Imark@123");
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(adminUser, "Admin");
                }
            }
            if ((await userManager.FindByNameAsync("2020000003")) == null)
            {

                var adminUser = new ApplicationUser
                {
                    UserName = "2020000003",
                    Email = "danphetelehealth@telemedicine.com",
                    EmailConfirmed = true,
                };
                var result = await userManager.CreateAsync(adminUser, "Imark@123");
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(adminUser, "Admin");
                }
            }

            if (context.Hospitals.Any() || context.Hospitals.Any())
            {
                return;   // DB has been seeded
            }
            IList<Profile> list = new List<Profile>();

            var hospitals = new Hospital[]
        {
            new Hospital{ HospitalName="Danphe Tele Health",HospitalCode="DTH",ImagePath="./assets/img/DanpheTeleHealth.jpg",HospitalLogo="./assets/img/DanpheTeleHealth.png",Location="Dilli Bazar",IsActive=true,PaymentEnable= true },
            //new Hospital{ HospitalName="Maharajgunj Teaching Hospital",HospitalCode="TUTH",ImagePath="./assets/img/Teaching.jpg",HospitalLogo="./assets/img/TeachingLogo.png",Location="Teaching Hosspital",IsActive=true,PaymentEnable= false },
            //new Hospital{ HospitalName="HAMS Hospital",HospitalCode="HAMS",ImagePath="./assets/img/HAMS.jpg",HospitalLogo="./assets/img/HAMSLogo.jpg",Location="Mandhikhatar",IsActive=true,PaymentEnable= true },
            //new Hospital{ HospitalName="Grande Hospital",HospitalCode="Grande",ImagePath="./assets/img/Grande.jpg",HospitalLogo="./assets/img/GrandeLogo.png",Location="Tokha",IsActive=true,PaymentEnable= true },
            //new Hospital{ HospitalName="Matrika Eye Center",HospitalCode="MEC",ImagePath="./assets/img/Matrika.jpg",HospitalLogo="./assets/img/MatrikaLogo.png",Location="Baneshwor",IsActive=true,PaymentEnable= true },

           };
            foreach (Hospital hsptl in hospitals)
            {
                context.Hospitals.Add(hsptl);
            }
            var departments = new Department[]
            {
                  new Department{DepartmentName ="Cardiology",DepartmentShortName="CDG",Description="Related to Heart",IsActive=true},
                  new Department{DepartmentName ="Infectious Disease",DepartmentShortName="GNC",Description="Infectious Diseases",IsActive=true},
                  new Department{DepartmentName ="Hematology",DepartmentShortName="HPT",Description="Related to Liver",IsActive=true},
                  new Department{DepartmentName ="Nephrology",DepartmentShortName="NPR",Description="Related to Kidney",IsActive=true},
                  new Department{DepartmentName ="Internal Medicine",DepartmentShortName="OPD",Description="General",IsActive=true},
                  new Department{DepartmentName ="ENT",DepartmentShortName="ENT",Description="Related to Ear, Nose & throat",IsActive=true},
                  new Department{DepartmentName ="Gynaecology",DepartmentShortName="GNC",Description="Related to Women",IsActive=true},
                  new Department{DepartmentName ="Psychiatry",DepartmentShortName="PSY",Description="Related to Mental Health",IsActive=true}
            };
            foreach (Department dept in departments)
            {
                context.Departments.Add(dept);
            }
            context.SaveChanges();

            var consultations = new Consultation[]
            {
                 new Consultation
                 {
                     Description = "500 wala consultation",
                     Charge = 500.00M,
                     HospitalId = context.Hospitals.FirstOrDefault().HospitalId,
                 }
            };

            foreach (Consultation item in consultations)
            {
                context.Consultations.Add(item);
            }

            context.SaveChanges();

        }
    }
}
